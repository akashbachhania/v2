<link rel="stylesheet" href="<?php echo base_url()?>application/views/assets/style/visitors.css">

<?php
if( !isset($visitors) 
|| !isset($selected_letter) 
|| !isset($selected_user) 
|| !isset($status) 
){
    //die('Missing data');
}
?>
<style type="">
#div_members_all{
    /*width: 100%;*/
}

#gridSystem{
    width: 100%;
}

.container{
    width: 1000px;
}

.dataTable tbody tr td:nth-child(2) a{
    width: 100px;
}
</style>
<div class="row">
    <div id="div_members_all" class="col-md-12">
    <?php
        $headers = array('', 'Visit date', 'First Name','Last Name', 'Address','Phone','Mail','Age','Notes', 'Action' );
        $letters = array('ALL', 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
        
        foreach( $letters as &$letter ){
            $class='';
            if( $selected_letter == $letter ){
                $class='class="selected_letter"';
            }
            $letter = '<span><a '.$class.' href="'.site_url('giving/index/'.$letter).'">'.$letter.'</a></span>';
        }
        $letters = '<div id="div_letters">'.implode('',$letters).'</div>';
        
        $this->table->set_heading( $headers );
        
        $visitors_table = $this->table->generate();
        $this->table->clear();
    ?>

     <form  id="form_batch_action" method="post">
        <div id="div_givings ">
        <div class="panel-heading datatable-heading">
            <h4 class="section-title">Giving</h4>     
        </div>
<!--             <legend>Giving</legend> -->
            <b>Select Date:</b>
            <input type="text" data-datepicker="datepicker" class="input-small" name="giving_date" id="giving_date" value="<?php echo $date_giving;?>" />
            <b>View:</b>
            <select name="view_stand" id="view_stand" class="input">
                <option value="1" <?php echo (isset($_GET['view']) and $_GET['view'] == 1)? 'selected' : ''; ?>>Head of Households</option>
                <option value="2" <?php echo (isset($_GET['view']) and $_GET['view'] == 2)? 'selected' : ''; ?>>All Members</option>
            </select>
            <a class="btn btn-info" onclick="showGivings();" style="margin-bottom:10px;" >Show Givings</a>
            <input class="btn span2 btn-info giving_btn_save" type="button" value="Save" name="action" id="btn_save" style="float:right;"/>
            <?php 
            echo $letters;
           // echo $visitors_table; 
            ?>
            <table cellspacing="0" width="100%" class="table table-bordered table-striped dataTable">
                <thead>
                    <tr>
                        <th><input id="select_visitors" type="checkbox"></th>
                        <!-- <th>Member Since</th> -->
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Tithing</th>
                        <th>Notes</th>
                    </tr>
                </thead>
            </table>

        </div>
          <div class="horizontal_center" style="width:100%;">
              <br><b>Report Range</b>
                From:
                <input type="text" data-datepicker="datepicker" class="input-small" name="start_date" id="start_date" value="<?php echo $start_date;?>" />
                To:  
                <input type="text" data-datepicker="datepicker" class="input-small" name="end_date" id="end_date" value="<?php echo $end_date;?>" />
                <div class="horizontal_center" style="float: right; width: 55%;">
                    <input class="btn span2 btn-info" type="submit" value="Email Report" data-batch-action="<?php echo site_url('giving/email_report')?>" name="action" id="btn_email" disabled=""/>
                    <!-- <input class="btn span2 btn-info" type="submit" value="Print Report" data-batch-action="<?php //echo site_url('giving/giving_print')?>" name="action" id="btn_print"/> -->    
                    <input class="btn span2 btn-info" type="button" value="Print Report" id="p_report" onclick="givingprint()">
                    <input class="btn span2 btn-info" type="button" value="Save" name="action" id="btn_save"/>
                    <input class="btn span2 btn-info" type="hidden" value="<?php  echo $selected_letter; ?>" name="first_letter"/>        
                </div>  
                
                <input class="btn span2 btn-info" type="hidden" value="<?php  echo $selected_letter; ?>" name="first_letter"/>        
            </div> 
     </form>
    </div>
</div>
<input type="hidden" name="selected_letter" id="selected_letter" value="<?php echo $selected_letter?>">

<script>
$(document).ready(function(){
    $("#btn_save,.giving_btn_save").click(function(){
        var url = '<?php echo site_url("giving/index/".$selected_letter); ?>';
        var arrayFund = [];
        $(".object_fund").each(function(index, el) {
            arrayFund.push({amount: $(this).find('.input').val(), 'giving': $(this).find('.selection').val(), visitor: $(this).data('id'), edit: $(this).data('is')});               
        });
       
        var data={
                    giving_date: $("#giving_date").val(),
                    fund: arrayFund,
                    view_type:'<?php echo $view_type;?>',
                    action:'Save'
                };
        
        $.post(url, data, function(resp, textStatus, xhr) {
            location.reload();
        },'json');
    });


});


function add_more_fund(id){
    el  ='<span class="object_fund" data-id="'+id+'" data-is="false"><div><input type="text" name="giving['+id+'][]" class="input" value=""></div>';
    option_content = $("#selected_user_"+ id).html();
    el  +='<div><select class="selection">';
    el  +=option_content;
    el  +='/<select></div></span>';
    $("#add_more_container_"+id).append(el);

}

    $('#div_givings table').dataTable( {
                "aoColumns": [
                    // { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": false },
                    <?php //if(isset($givings) and is_array($givings) and count($givings) > 0) {?>
                        <?php //foreach($givings as $give) {?>
                           //{ "bSortable": false },
                        <?php //}?>
                    <?php //}?>
                    { "bSortable": false },
                    ],
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": '<?php echo site_url('giving/ajax_loading/'.$selected_letter);?>?giving_date=<?php echo $date_giving;?>&view_type=<?php echo $view_type;?>',
                "fnDrawCallback":afterLoadTable,
                "sPaginationType": "bootstrap",
                "sDom": "<<l><f>r>t<'row'<'span2'i><p>>",
                /*"sDom": "<<'span1'lf>t<'row'<'span2'><p>>",*/
                "aaSorting": [],
                "autoWidth":false,
                "oLanguage": {
                    "sInfo": "_TOTAL_ Members (_START_ to _END_)",
                    "sSearch": "Search:",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                         "sNext": "Next",
                         "sPrevious": "Prev",
                    }                         
                },                     
            }
    );  
    function showGivings() {
        var giving_date = $('#giving_date').val();
        var view = $('#view_stand').val();
        document.location.href = '<?php echo site_url('giving/index/'.$selected_letter);?>?giving_date='+giving_date+'&view='+view;
    }
        // Listen for click on toggle checkbox
    $('#select_visitors').click(function(event) {   
        if(this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function() {
                this.checked = true;                        
            });
        } else {
            $(':checkbox').each(function() {
                this.checked = false;                        
            });
        }
    });
    $(document).ready(function(){
        $('#DataTables_Table_0_wrapper > div.row > div:nth-child(1)').addClass('span4');
        $('#DataTables_Table_0_wrapper > div.row > div:nth-child(1)').removeClass('span2');
    });

    function givingprint(action) {
        
        $.ajax({
            url: "<?php echo base_url() ?>index.php/giving/giving_print",
            type: 'POST',
            data: 'start_date=' + $("#start_date").val() +'&end_date='+$("#end_date").val(),
            success: function(data) {
                //console.log(data);                     
                window.open(data);
            }
        });        
    }
</script>

<style type="text/css">
    .object_fund{
        display: block;
    }

    .object_fund div{
        float: left;
        width: 100%;
        display: inline-block;
    }

</style>