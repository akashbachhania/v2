<?php     
$error = 'error';

switch( $status ){
    case Visitors::STATUS_SAVED:
        $msg = "New visitors has been created!";
        $class = 'alert alert-success';
    break;
    case Visitors::STATUS_UPDATE:
        $msg = "Visitor has been updated";
        $class = 'alert alert-info'; 
    break;
    case Visitors::STATUS_ERROR:
        if( $visitor->errors ){
            $msg = "Plese fill in mandatory fields";
        } else {
            $msg = "There was an error while trying to save visitor";
        }
        
        $class = 'alert alert-error'; 
    break;
}

$referrers;
$member_children = array();
$children = array();
$member_referer = '';
$member_spouse  = '';
$spouse  = '';
$referer = '';
$hidden_referer = 'hidden';
$hidden_spouse  = 'hidden';
 
if( isset( $visitor->_relations )){
    foreach( $visitor->_relations as $relation ){
         if( $relation->relation_type == Relations::RELATION_CHILD ){
             $member_children[] = $relation->related_person_id;
             $children[] = $relation->related_person_name;
         } else if( $relation->relation_type == Relations::RELATION_SPOUSE ){
             $member_spouse = $relation->related_person_id;
             $spouse = $relation->related_person_name;
             $hidden_spouse = '';
         } else if( $relation->relation_type == Relations::RELATION_REFERER ){
             $member_referer = $relation->related_person_id;
             $referer = $relation->related_person_name;
             $hidden_referer = '';
         }
    }
}


$visitor_year = $visitor_day = $visitor_month = '-' ;
if(preg_match('!(?P<year>\d\d\d\d)-(?P<month>\d\d)-(?P<day>\d\d)!', $visitor->birth, $match)){
    $visitor_month = $match['month'];
    $visitor_year  = $match['year'];
    $visitor_day   = $match['day'];
}

  

?>
<!DOCTYPE html>
<html>
    <head>
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Visitor Plus</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/bootstrap.css' ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/extensions.css' ?>"/>
        <?php if( isset( $responsive_css_enabled )) {?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/bootstrap-responsive.css' ?>"/>
        <?php }?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/docs.css' ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/prettify.css' ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/datepicker.css' ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/jquery-ui-1.8.21.custom.css' ?>"/> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/js/htmlarea/jHtmlArea.css' ?>"/> 
        <script src="<?php echo base_url() . 'application/views/assets/js/jquery.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-datepicker.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/jquery.dataTables.js' ?>" type="text/javascript"/></script>        
        <script src="<?php echo base_url() . 'application/views/assets/js/custom.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-tooltip.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-popover.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-modal.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-collapse.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-dropdown.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/estilos.js' ?>" type="text/javascript"/></script>        
        <script src="<?php echo base_url() . 'application/views/assets/js/popup.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/jquery.formatCurrency.all.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/galleria/galleria-1.2.8.js' ?>" type="text/javascript"/></script>
        
        <script src="<?php echo base_url() . 'application/views/assets/js/htmlarea/jHtmlArea-0.7.5.js' ?>" type="text/javascript"/></script>
        
  
        
          
        <script src="<?php echo base_url() . 'application/views/assets/js/default.js' ?>" type="text/javascript"/></script>                   
        <script src="<?php echo base_url() . 'application/views/assets/js/editor/jquery.wysiwyg.js' ?>" type="text/javascript"/></script> 
        <script src="<?php echo base_url() . 'application/views/assets/js/editor/controls/default.js' ?>" type="text/javascript"/></script> 

        <script>
            $(document).ready(
                function(){
                    self.print();
                }
            )
        </script>
    </head>
    <body>


         
        
                         
        <div class="<?php echo $class ?> clearfix">
            <section id="gridSystem"> 
            
            
<div class="row">
    <div id="div_new_visitor" class="span8">
        <?php if( $status ){ ?>
            <div id="div_visitor_added" class="<?php echo $class ?>">
                    <a class="close" data-dismiss="alert">�</a>
                    <?php echo $msg ?>
            </div>
        <?php } ?>    
        <form class="form-horizontal" id="form_new_visitor" action="<?php echo site_url('visitors/new_visitor')?>" method="post" onsubmit="remove_notes(this)">
            <fieldset>
                <legend><?php echo $visitor->id ? $visitor->first_name .' '. $visitor->last_name: 'New visitor'?></legend>
                
                <div class="control-group">
                    <label class="control-label" for="input01">Visit date:</label>
                    <div class="controls">
                        <input type="text" data-datepicker="datepicker" name="datetime" class="input-xlarge" id="datetime" value="<?php echo isset($visitor->_visits) && isset($visitor->_visits[0]->datetime) ? $visitor->get_us_date( $visitor->_visits[0]->datetime) : ''?>">
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="input01">First:</label>
                    <div class="controls controls-row">
                        <input type="text" class="input-medium <?php echo isset($visitor->errors['first_name'])? $error:''?>" value="<?php echo $visitor->first_name?>"   name="first_name" id="first_name">
                        Last:
                        <input type="text" class="input-medium <?php echo isset($visitor->errors['last_name'])? $error:''?>" value="<?php echo $visitor->last_name?>"   name="last_name" id="last_name">
                    </div>
                </div>
                        
                <div class="control-group">
                    <label class="control-label" for="input01">Address:</label>
                    <div class="controls">
                        <input type="text" class="input-xlarge <?php echo isset($visitor->errors['address'])? $error:''?>" value="<?php echo $visitor->address?>"   name="address" id="address">
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="input01">City:</label>
                    <div class="controls">
                        <input type="text" class="input-small <?php echo isset($visitor->errors['city'])? $error:''?>" value="<?php echo $visitor->city?>" name="city"   id="city">
                        State:
                        <?php echo form_dropdown('state', $states, $visitor->state, 'id="state" class="input-small"') ?>
                        Zip:
                        <input type="text" class="input-small <?php echo isset($visitor->errors['zip'])? $error:''?>" value="<?php echo $visitor->zip?>"   name="zip"    id="zip">
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="input01">Phone:</label>
                    <div class="controls">
                        <input type="text" size="3"  maxlength="3" class="input-mini  <?php echo isset($visitor->errors['phone_1'])? $error:''?>" value="<?php echo substr($visitor->phone_1,0,3)?>" name="phone_1"   id="phone_1">
                        -
                        <input type="text" size="3"  maxlength="3" class="input-mini  <?php echo isset($visitor->errors['phone_1'])? $error:''?>" value="<?php echo substr($visitor->phone_1,3,3)?>" name="phone_2"   id="phone_2">
                        -
                        <input type="text" size="4"  maxlength="4" class="input-mini  <?php echo isset($visitor->errors['phone_1'])? $error:''?>" value="<?php echo substr($visitor->phone_1,6,4)?>" name="phone_3"   id="phone_3">
                        Gender:
                        <?php echo form_dropdown('gender', array('' => '-Select gender-', Visitor::GENDER_FEMALE => 'Female',Visitor::GENDER_MALE => 'Male'), $visitor->gender , 'id="gender" class="input-mini '.(isset($visitor->errors['gender'])? $error:'').' "') ?>                                        

                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="input01">Email:</label>
                    <div class="controls">
                        <input type="text" class="input-xlarge <?php echo isset($visitor->errors['email'])? $error:''?>" value="<?php echo $visitor->email?>" name="email"   id="email">
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="input01">Month:</label>
                    <div class="controls">
                        <?php echo form_dropdown('month', $months, $visitor_month, 'class="birth" id="select_month" '); ?>
                        <label class="inline_label birth">Day:</label>
                        <?php echo form_dropdown('day', $days, $visitor_day, 'class="birth" id="select_day" '); ?>
                        <label class="inline_label birth">Year:</label>
                        <?php echo form_dropdown('year', $years, $visitor_year, 'class="birth" id="select_year" '); ?>
                        
                        <?php /*echo form_dropdown('day', $days, parse($visitor->birth), 'id="day" class="input-small '.(isset($visitor->errors["birth"])? $error:"").'" ' )  
                          echo form_dropdown('month', $months, parse($visitor->birth), 'id="month" class="input-small'.(isset($visitor->errors["birth"])? $error:"").'" ')  
                          echo form_dropdown('year', $years, parse($visitor->birth), 'id="year" class="input-small'.(isset($visitor->errors["birth"])? $error:"").'" ')*/?>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="input01">Married:</label>
                    <div class="controls">
                        <?php echo form_dropdown('married', array('' => '-', Visitor::MARRIED_SINGLE => 'No',Visitor::MARRIED_YES => 'Yes'), $visitor->married , 'id="select_married" class="input-mini"') ?>                
                        <label class="inline_label spouse <?php echo $hidden_spouse ?>">Spouse:</label>
                        <input type="text" name="spouse_related_person_name" class="spouse <?php echo $hidden_spouse ?>" id="spouse" value="<?php echo $spouse?>">
                         
                        <?php // echo form_dropdown('spouse', $members, $member_spouse, 'id="select_spouse" class="'.$hidden_spouse.' input-small form_tooltip" data-placement="top" rel="tooltip"  data-original-title="Select refering member"')?>
                        <input type="hidden" name="spouse_person_id" id="spouse_person_id"/>        
                        <input type="hidden" name="spouse_relation_type" id="spouse_person_id" value="<?php echo Relations::RELATION_SPOUSE?>"/>        
                        <input type="hidden" name="spouse_related_person_id" id="spouse_related_person_id"/>        
                    </div> 
                </div>
                
                <div id="div_control_children" class="control-group">
                    <label class="control-label" for="input01">Children</label>
                    <div class="controls">
                        <?php echo form_dropdown('children', array('0'=>'No','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'), count($member_children), 'id="select_children" class="input-mini"')?>            
                    </div> 
                </div>
                
                <?php
                if( $children ){
                    foreach( $children as $index=>$child ){
                     echo   '<div class="children control-group">
                            <label class="control-label" for="input01">Child#'.($index+1).'</label>
                            <div class="controls">
                                <input type="text" class="input-normal" name="child_'.$index.'_related_person_name" id="child_name_'.$index.'" value="'.$child.'">                                                      
                                <input type="hidden" class="input-medium" name="child_'.$index.'_related_person_id" id="child_'.$index.'related_person_id">
                                <input type="hidden" class="input-medium" name="child_'.$index.'_person_id" id="child_'.$index.'person_id" value="'.$visitor->id.'">              
                                <input type="hidden" class="input-medium" name="child_'.$index.'_relation_type" id="child_'.$index.'relation_type" value="child">                  
                            </div>
                        </div>';                
                    }
                }
                ?>
                               
                <div class="control-group">
                    <label class="control-label" for="input01">Bible study:</label>
                    <div class="controls">
                        <?php echo form_dropdown('bible_study', array('' => '-', Visitor::STUDY_YES => 'Yes',Visitor::STUDY_NO => 'No'), $visitor->bible_study , 'id="bible_study" class="input-mini"') ?>
                        <label id="label_primary_lang" class="inline_label">Primary language:</label>
                        <?php echo form_dropdown('primary_language', $languages, $visitor->primary_language, ' id="primary_language" class="input-small add_new_type"  data-type="'.Type::TYPE_LANGUAGE.'"  onclick="add_new_type(this)"') ?>                                 
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="input01">Reffered by:</label>
                    <div class="controls">
                        <?php echo form_dropdown('refered_by', $referrers, $visitor->refered_by, ' class="input-mini add_new_type" id="refered_by" data-type="'.Type::TYPE_REFERER.'" onclick="add_new_type(this)"')?>
                        <label class="inline_label label_referer <?php echo $hidden_referer ?>">Name:</label>
                        <input type="text" name="refered_by_related_person_name" class="input-name <?php echo $hidden_referer ?>" id="referer" value="<?php echo $referer ?>">               
                        <input type="hidden" name="refered_by_person_id" id="refered_by_person_id"/>        
                        <input type="hidden" name="refered_by_relation_type" id="refered_by_relation_type" value="<?php echo Relations::RELATION_REFERER?>"/>        
                        <input type="hidden" name="refered_by_related_person_id" id="refered_by_related_person_id"/>                                  
                    </div>
                </div>
                
                <?php 
                    if( isset($visitor->_assignment_log)){
                        foreach($visitor->_assignment_log as $index=>$assignment ){
                            $this->load->view(
                                'visitors/component_assignment',
                                array(
                                    'assignment_types' => $assignment_types,
                                    'members' => $members,
                                    'assignment' => $assignment,
                                    'index' => $index
                                )
                            );
                        }    
                    }                                                                                 
                ?>
                
                <div class="new_assignment control-group">
                    <label class="control-label" for="input01">New assignment</label>
                    <div class="controls">
                        <?php echo form_dropdown('new_assignment_id', $assignment_types, '', ' id="new_assignment_id" class="input-mini add_new_type" data-type="'.Type::TYPE_ASSIGNMENT.'"  onclick="add_new_type(this)" ')?>
                        <label  class="inline_label label_assign">Date:</label>                                                                      
                        <input type="text" data-datepicker="datepicker" name="new_assignment_datetime"  id="new_assignment_date" class="input-small assignment_date" id="new_assignment_datetime" value="<?php echo gmdate('Y-m-d')?>">
                        <label class="inline_label label_referer">Done by:</label>                                                      
                        <?php echo form_dropdown('new_assignment_doneby_person_id',$done_by_members, '', ' id="new_assignment_doneby_person_id"  class="input-mini  add_new_type" data-type="'.Type::TYPE_DONE_BY.'"  onclick="add_new_type(this)" ')?>
                    </div>
                </div>
                                               

                <!-- notes -->
                <?php ?>
                    <div id="div_fourth">
                         <div class="">
                            <div id="visitor_notes">
                                <table id="vifeed_table">
                                    <?php if( $visitor->_note && is_array($visitor->_note)) { ?>
                                    <thead>
                                        <tr>
                                            <th>Time</th>
                                            <th>Modified By</th>
                                            <th>Note</th>
                                        </tr>
                                    </thead>
                                    <?php } ?>
                                    <tbody id="">
                                    <?php 
                                    if( $visitor->_note && is_array($visitor->_note)) {
                                        foreach( $visitor->_note as $note ){
                                            if( $note->note ){
                                                echo '<tr>'.
                                                        '<td>'.$note->change_time.'</td>'.
                                                        '<td>'.$note->_person[0]->name.'</td>'.
                                                        '<td><textarea name="new_note['.$note->id.'_'.$note->person_id.']" class="note_textarea" onchange="change_note(this)" data-clean="true" onclick="slide_down(this)">'.$note->note.'</textarea></td>'.                                        
                                                     '</tr>';                                                 
                                            }
                                        }
                                        
                                    }
                                     
                                    ?>                 
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                 
                <!-- end notes -->                
                <div class="control-group">

                </div>
                <input type="hidden" name="id" id="id" value="<?php  echo $visitor->id?>"/>                                                                                                         
                <input type="hidden" name="save" value="save"/>
                <?php if(isset($done_by_added) && $done_by_added == true){?>                                                                                                         
                    <input type="hidden" name="done_by_added" id="done_by_added" value="done_by_added"/>                                                                                                         
                <?php } ?>
            </fieldset>                
        </form>
    </div>
 
              </section>
              <div class="push"></div>
            </div>
            

    </body>
</html>
<?php
function parse( $date ){
return $date;    
}
?>
