<div class="assignments form_row">
    <label class="field_name align_right" for="input01">Assignment#<?php echo $index ?></label>
    <div class="field">
        <?php echo form_dropdown('assignment_'.$index.'_type_id', $assignment_types, $assignment->type_id, ' id="'.Type::TYPE_ASSIGNMENT.'_'.$index.'" class="span3 add_new_type" data-type="'.Type::TYPE_ASSIGNMENT.'" onclick="add_new_type(this)" ') ?>
        <label  class="inline_label label_assign">Date:</label>
        <input type="text" data-datepicker="datepicker" name="assignment_<?php echo $index?>_datetime" class="input-small assignment_date" value="<?php echo $assignment->datetime?>" data-date-format="yyyy/mm/dd">
        <label class="inline_label label_referer">Done by:</label>                                                      
        <?php echo form_dropdown('assignment_'.$index.'_doneby_person_id', $done_by_members, $assignment->doneby_person_id, ' id="'.Type::TYPE_DONE_BY.'_'.$index.'" class="input-mini add_new_type" data-type="'.Type::TYPE_DONE_BY.'"  onclick="add_new_type(this)"')?>
        <label class="inline_label label_referer" style="width:20px;"><i data-assignment-id="<?php echo $assignment->id ? $assignment->id : 0 ?>" class="span3 remove_assignment" style="display:none;"></i></label>
        <input type="hidden" name="assignment_<?php echo $index?>_id" value="<?php echo $assignment->id ?>">
    </div>
</div>
