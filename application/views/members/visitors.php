<link rel="stylesheet" href="<?php echo base_url()?>application/views/assets/style/visitors.css">

<?php
if( !isset($visitors) 
|| !isset($selected_letter) 
|| !isset($selected_user) 
|| !isset($status) 
){
    //die('Missing data');
}
?>
<style type="">
#div_visitors_all{
    width: 100%;
}

#gridSystem{
    width: 100%;
}

.container{
    width: 1400px;
}

.dataTable tbody tr td:nth-child(2) a{
    width: 100px;
}
</style>

<div class="row col-md-12">
<div class="col-md-1"></div>

    <div id="div_visitors_all" class="span12">
    <?php
        $headers = array('', 'Visit date', 'First Name','Last Name', 'Address','Phone','Mail','Age','Notes', 'Action' );
        $letters = array('ALL', 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
        
        foreach( $letters as &$letter ){
            $class='';
            if( $selected_letter == $letter ){
                $class='class="selected_letter"';
            }
            $letter = '<span><a '.$class.' href="'.site_url('members/index/'.$letter).'">'.$letter.'</a></span>';
        }
        $letters = '<div id="div_letters">'.implode('',$letters).'</div>';
        
        $this->table->set_heading( $headers );
        
        $visitors_table = $this->table->generate();
        $this->table->clear();
   
     ?>
     <input type="hidden"  id="sort" value="<?php echo $sort_descending ? 'true' : 'false' ?>" />
     <form  id="form_batch_action" method="post">
        <div id="div_members">
        <div class="panel-heading datatable-heading" id="data-table">
            <h4 class="section-title">Members</h4>
        </div>
<!--             <legend>Members</legend>
 -->            <?php 
            echo $letters;
           // echo $visitors_table; 
            ?>
            <table cellspacing="0" width="100%" class="table table-bordered table-striped dataTable" id="tableSortable">
                <thead>
                    <tr>
                        <th></th>
                        <!-- <th>Member Since</th> -->
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Mail</th>
                        <th>Age</th>
                        <th>Notes</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>

        <div class="col-md-12 myclass">
        <div class="btn span2 btn-info"  value="" name="quick_add_member" id="btn_quick_add_member">Quick Add</div>
            <input class="btn span2 btn-info" type="submit" value="Email" data-batch-action="<?php echo site_url('email/index/batch_email')?>" name="action" id="btn_email"/>    
            <input class="btn span2 btn-info" type="submit" value="Postcard" data-batch-action="<?php echo site_url('paper/new_order_step_1/batch_paper')?>" name="action" id="btn_postcard"/>
            <input class="btn span2 btn-info" type="submit" value="Fwd contacts" data-batch-action="<?php echo site_url('email/index/batch_forward')?>" name="action" id="btn_forward"/>
            <input class="btn span2 btn-info" type="submit" value="Delete contacts" data-batch-action="<?php echo site_url('members/batch_delete')?>" name="action" id="btn_forward" onclick="delete_contacts(this)"/>
            <input class="btn span2 btn-info" type="submit" value="Import contacts" data-batch-action="<?php echo site_url('members/batch_import')?>" name="action" id="btn_import" />
            <input class="btn span2 btn-info" type="hidden" value="<?php  echo $selected_letter; ?>" name="first_letter"/>        
        </div>    
     </form>
    </div>
</div>
<input type="hidden" name="selected_letter" id="selected_letter" value="<?php echo $selected_letter?>">

<style>
    .datepickerDiv .datepicker{
        top: 75px!important;
        left: 0px!important;
    }
</style>

<!-- Modal -->
<div class="modal fade" id="member_modal_quick_add" tabindex="-1" role="dialog" aria-labelledby="memberquickadd" style="display:none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="memberquickadd">Quick Add Member</h4>
      </div>
        <div class="modal-body">
            <fieldset>
                <div class="form-group row">
                    <label class="control-label col-md-12" for="input01">Attend date:</label>
                    <div class="controls col-md-12">
                        <input type="text" name="datetime" class="input-xlarge form-control" id="qam_datetime" value="">
                    </div>
                </div>
                
                <div class="form-group row">
                    <div class="col-md-6">
                        <label class="control-label col-md-12" for="input01">First (*):</label>
                        <div class="controls col-md-12 controls-row">
                            <input type="text" class="input-medium form-control" value="" name="first_name" id="qam_first_name">
                        </div>                                            
                    </div>

                    <div class="col-md-6">
                        <label class="control-label col-md-12" for="input01">Last (*):</label>
                        <div class="controls col-md-12 controls-row">
                            <input type="text" class="input-medium form-control" value="" name="last_name" id="qam_last_name">
                        </div>                                                
                    </div>

                </div>

                <div class="form-group row">
                    <label class="control-label col-md-12" for="input01">Address:</label>
                    <div class="controls col-md-12">
                        <input type="text" class="input-xlarge form-control" value="" name="address" id="qam_address">
                    </div>
                </div>
                
                <div class="form-group row">
                    
                    <div class="col-md-4">
                        <label class="control-label" for="input01">City:</label>
                        <div class="controls">
                            <input type="text" class="form-control" value="" name="city" id="qam_city">
                        </div>    
                    </div>

                    <div class="col-md-4">
                        <label class="control-label" for="input01">State:</label>
                        <div class="controls">
                            <?php echo form_dropdown('state', $states, $visitor->state, 'id="qam_state" class="input-small form-control"') ?>                       
                        </div>    
                    </div>

                    <div class="col-md-4">
                        <label class="control-label" for="input01">Zip:</label>
                        <div class="controls">
                            <input type="text" class="form-control" value="" name="zip" id="qam_zip">
                        </div>                                            
                    </div>

                    
                </div>

                <div class="col-md-6">
                    <label class="control-label" for="input01">Phone:</label>
                    <div class="controls">
                        <input type="text" size="3" maxlength="3" class="input-mini  " value="" name="phone_1" id="qam_phone_1">
                        -
                        <input type="text" size="3" maxlength="3" class="input-mini  " value="" name="phone_2" id="qam_phone_2">
                        -
                        <input type="text" size="4" maxlength="4" class="input-mini  " value="" name="phone_3" id="qam_phone_3">
                    </div>
                </div>


                <div class="col-md-6">
                    <label class="control-label" for="input01">Gender:</label>
                    <div class="controls">
                        <?php echo form_dropdown('gender', array('' => '-Select gender-', Visitor::GENDER_FEMALE => 'Female',Visitor::GENDER_MALE => 'Male'), $visitor->gender , 'id="qam_gender" class="form-control'.(isset($visitor->errors['gender'])? $error:'').' "') ?>
                    </div>
                </div>
                <br>


                <div class="col-md-12">
                    <label class="control-label col-md-12" for="input01">Email (*):</label>
                    <div class="controls col-md-12">
                        <input type="text" class="input-xlarge form-control" value="" name="email" id="qam_email">
                    </div>
                </div>
                
                <div class="col-md-6">
                    <label class="control-label" for="input01">Head of Household:</label>
                    <div class="controls">
                        <?php echo form_dropdown('head_household', array(Visitor::MARRIED_SINGLE => 'No',Visitor::MARRIED_YES => 'Yes'), $visitor->head_household , 'id="qam_head_household" class="col-md-3 form-control"') ?>
                    </div> 
                </div>
                
                <div class="col-md-6">
                    <label class="control-label" for="input01">Member Status:</label>
                    <div class="controls">
                        <?php echo form_dropdown('active', array(1 => 'Active',0 => 'Inactive'), $visitor->active , 'id="qam_active" class="form-control col-md-3"') ?>               
                    </div> 
                </div>
                 <div class="col-md-4">
                    <label class="control-label" for="input01">Month:</label>
                    <div class="controls">
                        <?php echo form_dropdown('month', $months, $visitor_month, 'class="birth form-control" id="qam_select_month" '); ?>
                    </div> 
                </div>
                <div class="col-md-4">
                        <label class="inline_label birth">Day:</label>
                    <div class="controls">
                        <?php echo form_dropdown('day', $days, $visitor_day, 'class="birth form-control" id="qam_select_day" '); ?>
                    </div> 
                </div>
                <div class="col-md-4">
                        <label class="inline_label birth">Year:</label>
                    <div class="controls">
                        <?php echo form_dropdown('year', $years, $visitor_year, 'class="birth form-control" id="qam_select_year" '); ?>                                               
                    </div> 
                </div>
                
            </fieldset>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btn_save_member_quick_add" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#DataTables_Table_0_wrapper > div:nth-child(3) > div:nth-child(1)').removeClass('span2');
        $('#DataTables_Table_0_wrapper > div:nth-child(3) > div:nth-child(1)').addClass('span4');
    });

    </script>