<?php    
$visit_fields = array(
    '_visitor::first_name',
    '_visitor::last_name',
    '_visitor::phone_1',
    '_visitor::city',
    '_visitor::zip',
    '_visitor::state',
    'datetime'
);
$visit_overrides = array(
    '_visitor::first_name' => array('name' => 'First Name'),
    '_visitor::last_name'  => array('name' => 'Last Name'),
    '_visitor::phone_1'    => array('name' => 'Phone'),
    '_visitor::city'       => array('name' => 'City'),
    '_visitor::zip'        => array('name' => 'Zip'),
    '_visitor::state'      => array('name' => 'State'),
    'datetime'             => array('name' => 'Visit'),
    '__href' => array(
        'href' => site_url('visitors/show_visitor/<_visitor::id>'),
        'id' => '<id>',
    )                       
);
foreach( $visits as $index=>$visit ){
    if( $visit->_visitor == null){
        unset($visits[$index]);
    }
}
$path = str_replace('index.php','',site_url()).'application/views/assets/img/';
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default panel-block">
            <div class="list-group">
                <div class="list-group-item">
                    <!-- <div id="div_top" class="row center m_20t">
                        <div class="col-md-12 col-md-push-2">                  
                            
                            <span class="div_dash col-md-2">
                            
                                <a data-placement="top" rel="tooltip" data-original-title="Visitors"  class="a_dash pull-left" href="<?php echo site_url('visitors')?>">
                            
                                    <img class='dashboard_icon' width="115px" height="115px" src="<?php echo $path?>family-icon.png"/>
                            
                                </a>
                            
                            </span>
                            
                            <span class="div_dash col-md-2">

                                <a data-placement="top" rel="tooltip" data-original-title="Reports"   class="a_dash pull-left" href="<?php echo site_url('reports')?>">

                                    <img class='dashboard_icon' width="115px" height="115px" src="<?php echo $path?>keynote-on-icon.png"/>

                                </a>

                            </span>
                            
                            <span class="div_dash col-md-2">

                                <a data-placement="top" rel="tooltip" data-original-title="Email"     class="a_dash pull-left" href="<?php echo site_url('email')?>">

                                    <img class='dashboard_icon' width="115px" height="115px" src="<?php echo $path?>address-book-icon.png"/>

                                </a>
                            </span>
                            
                            <span class="div_dash col-md-2">

                                <a data-placement="top" rel="tooltip" data-original-title="Postcards" class="a_dash pull-left" href="<?php echo site_url('paper')?>">

                                    <img class='dashboard_icon' width="115px" height="115px" src="<?php echo $path?>sign-up-icon.png"/>

                                </a>

                            </span>
                        </div>
                    </div> -->

                    <div id="div_second_members"  class="row m_20t">    
                        <form id="form_total_visits" method="post" action="<?php echo site_url('dashboard/index') ?>">
                            <div class="col-md-3">
                                <div id="div_visitors_total">
                                        <div class="form form-inline" id="div_form_total_visits">
                                            <input name="set_date" type="submit" class="btn btn-info" value="Set"/>
                                            <input name="start_date" class="input-small" type="text" id="start_date" data-datepicker="datepicker" value="<?php echo $start_date?>"/>-<input  class="input-small" id="end_date" name="end_date" type="text" data-datepicker="datepicker" value="<?php echo $end_date?>"/>
                                            <input name="check_feed" class="input-small" type="hidden" id="check_feed" value="<?php echo strtotime($end_date) < strtotime(gmdate('Y-m-d')) ? 'once' : 'true'?>"/>
                                        </div> 
                                        <?php                        
                                            $this->table->clear();
                                            $dates = '';
                                            $this->table->set_heading('Members info', $dates );
                                            //$this->table->add_row(array('Total Member', $total_member));
                                            $this->table->add_row(array('Total Attendence', $total_M));                        
                                            $this->table->add_row(array('Male', $male_M.'%'));
                                            $this->table->add_row(array('Female', $female_M.'%'));                    
                                            echo $this->table->generate();
                                        ?>
                                </div>    
                            </div>
                            <div class="col-md-9 charts">
                                <div id="members_per_age" class="col-md-6" style="margin-left: 0px;">
                                </div>   
                                <div id="attendance_per_date" class="col-md-6" style="margin-left: 0px;">
                                </div>  
                            </div>
                        </form>        
                    </div>

                    <div id="div_second"  class="row m_20t">    
                        <form id="form_total_visits" method="post" action="<?php echo site_url('dashboard/index') ?>">
                            <div class="col-md-3">
                                <div id="div_visitors_total">
                                    
                                        <div class="form form-inline" id="div_form_total_visits">
                                            <input name="set_date" type="submit" class="btn btn-info" value="Set"/>
                                            <input name="start_date" class="input-small" type="text" id="start_date" data-datepicker="datepicker" value="<?php echo $start_date?>"/>-<input  class="input-small" id="end_date" name="end_date" type="text" data-datepicker="datepicker" value="<?php echo $end_date?>"/>
                                            <input name="check_feed" class="input-small" type="hidden" id="check_feed" value="<?php echo strtotime($end_date) < strtotime(gmdate('Y-m-d')) ? 'once' : 'true'?>"/>
                                        </div> 
                                        <?php                        
                                            $this->table->clear();
                                            $dates = '';
                                            $this->table->set_heading('Visitor info', $dates );
                                            $this->table->add_row(array('Total visitors', $total));
                                            $this->table->add_row(array('Male', $male.'%'));
                                            $this->table->add_row(array('Female', $female.'%'));                    
                                            echo $this->table->generate();
                                        ?>
                                </div>    
                            </div>
                            <div class="col-md-9 charts">
                                <div id="visitors_per_age" class="col-md-6" style="margin-left: 0px;">
                                </div>   
                                <div id="visits_per_date" class="col-md-6" style="margin-left: 0px;">
                                </div>  
                            </div>
                            
                        </form>        
                    </div>

                    <div id="div_third" class="row table_div">
                         <div class="col-md-12">
                            <div id="div_visitors_table">
                            <?php     
                                echo $this->output_helper->generate_table( $visits, $visit_fields, $this->table, $visit_overrides);
                            ?>
                            </div>
                        </div>
                    </div>

                    <div id="div_fourth" class="row table_div">
                        <div class="col-md-12">
                            <div id="div_visitors_feed">
                                <legend>Recent Activity Feed</legend>
                                <table id="feed_table">
                                    <thead>
                                        <tr>
                                            <th>Time</th>
                                            <th>Visitor Name</th>
                                            <th>Activity Type</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody id="feed_body">
                                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .select2-container {
        display: none!important;
    }
    .datepicker{
    display: block;
    left: 76px!important;
    top: 32px!important;

    }
</style>