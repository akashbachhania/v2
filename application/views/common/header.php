<?php
    if( isset( $fluid_layout ) && $fluid_layout == true ){
        $class = 'container-fluid';
    } else {
        $class = 'container';
    }
?>

<!DOCTYPE html>
<html>
    <head>
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Visitor Plus</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/bootstrap.css' ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/extensions.css' ?>"/>
        <?php if( isset( $responsive_css_enabled )) {?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/bootstrap-responsive.css' ?>"/>
        <?php }?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/docs.css' ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/prettify.css' ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/datepicker.css' ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/jquery-ui-1.8.21.custom.css' ?>"/> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/js/htmlarea/jHtmlArea.css' ?>"/> 
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css"/> 

        <script src="<?php echo base_url() . 'application/views/assets/js/jquery.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-datepicker.js' ?>" type="text/javascript"/></script>

        <script src="<?php echo base_url() . 'application/views/assets/js/jquery.dataTables.js' ?>" type="text/javascript"/></script>        
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" type="text/javascript"/></script>        
        <script src="<?php echo base_url() . 'application/views/assets/js/custom.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-tooltip.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-popover.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-modal.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-collapse.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-dropdown.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/estilos.js' ?>" type="text/javascript"/></script>        
        <script src="<?php echo base_url() . 'application/views/assets/js/popup.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/jquery.formatCurrency.all.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/galleria/galleria-1.2.8.js' ?>" type="text/javascript"/></script>
        
        <script src="<?php echo base_url() . 'application/views/assets/js/htmlarea/jHtmlArea-0.7.5.js' ?>" type="text/javascript"/></script>
        
         <?php  if( isset( $load_charts ) ) {?>
            <script src="https://www.google.com/jsapi" type="text/javascript"/></script>
            <script type="text/javascript">
                google.load('visualization', '1.0', {'packages':['corechart']});
                google.setOnLoadCallback(draw_chart);            
            </script>
            
        <?php } ?>        
        

        <!--<script src="<?php echo base_url() . 'application/views/assets/js/default.js' ?>" type="text/javascript"/></script>-->
        <script src="<?php echo base_url() . 'application/views/assets/js/editor/jquery.wysiwyg.js' ?>" type="text/javascript"/></script> 
        <script src="<?php echo base_url() . 'application/views/assets/js/editor/controls/default.js' ?>" type="text/javascript"/></script> 

         <?php  if( isset( $jquery_ui ) && $jquery_ui == true) {?>
            <script src="<?php echo base_url() . 'application/views/assets/js/jquery.ui.core.js' ?>" type="text/javascript"/></script>
            <script src="<?php echo base_url() . 'application/views/assets/js/jquery.ui.widget.js' ?>" type="text/javascript"/></script>
            <script src="<?php echo base_url() . 'application/views/assets/js/jquery-ui-1.8.21.custom.js' ?>" type="text/javascript"/></script>
            <script src="<?php echo base_url() . 'application/views/assets/js/jquery.ui.position.js' ?>" type="text/javascript"/></script>
            <script src="<?php echo base_url() . 'application/views/assets/js/jquery.ui.mouse.js' ?>" type="text/javascript"/></script>
            <script src="<?php echo base_url() . 'application/views/assets/js/jquery.ui.autocomplete.js' ?>" type="text/javascript"/></script>
            <script src="<?php echo base_url() . 'application/views/assets/js/jquery.multiselect.js' ?>" type="text/javascript"/></script> 
            
        <?php } ?>
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyChZoXfwuOEaMUWyD82YFD20K_aLgg7l18&sensor=false" type="text/javascript"/></script>
              
        
    </head>
    <body>
 <?php include_once("analyticstracking.php") ?>       
        <?php if( !isset( $dont_display_header )) {?> 
                <div id="div_logo"></div>
                <div id="div_main_menu_top" class="navbar">
                    <div id="div_header_navbar_top" class="navbar-inner">                  
                        <div id="inner_top" class="nav-collapse pull-right"> 
                            <ul id="ul_header_submenu_top" class="nav">
                                <li><a href="javascript:void(0)">Total Members: 
                                <?php   if(isset($user->company_id)){
                                            echo getTotalCompanyMembers($user->company_id);    
                                        }else{
                                            echo '0';
                                        }
                                ?>
                                </a></li>
                                 <li><a class="" href="#"><?php echo $user->name ?></a></li>
                                 <li><a class="" href="<?php echo site_url('dashboard/settings')?>" target="__blank">My Settings</a></li>
                                 <li><a class="" href="<?php echo site_url('login')?>">Sign Out</a></li> 
                                 <li><a class="" href="<?php echo site_url('dashboard')?>">Support</a></li>                                                               
                            </ul>
                        </div>
                    </div>                 
                </div>
                <div id="div_main_menu" class="navbar">
                    <div id="div_header_navbar" class="navbar-inner">
                        <div id="inner" class="nav-collapse pull-left"> 
                            <ul id="ul_header_submenu" class="nav">
                                <li><a class="a_visitors" id="history_back" onclick="window.history.go(-1);">Go Back</a></li>  
                                                            
                            </ul>
                        </div>                                          
                        <div id="inner" class="nav-collapse pull-right"> 
                            <ul id="ul_header_submenu" class="nav">
                                <?php if( in_array($user->person_type, array( Person::TYPE_USER )) ) {?>
                                    <li id="a_dashboard"><a href="<?php echo site_url('dashboard')?>">Dashboard</a></li>
                                    <li id="a_members" class="dropdown-submenu"><a  href="<?php echo site_url('members')?>">Members</a>
                                        <ul class="dropdown-menu pull-right" style="top: 46px;">
                                            <li><a href="<?php echo site_url('members/new_member')?>">Add Member</a></li>
                                            <li><a href="<?php echo site_url('members')?>"><i class=""></i>View Members</a></li>    
                                            <li><a href="<?php echo site_url('members/export')?>">Export Members</a></li>
                                        </ul>
                                    </li>
                                    <li id="a_visitors" class="dropdown-submenu"><a href="<?php echo site_url('visitors')?>">Visitors</a>                                    
                                        <ul class="dropdown-menu pull-right" style="top: 46px;">
                                            <li><a class="a_visitors" href="<?php echo site_url('visitors/new_visitor')?>"><i class=""></i>Add new visitor</a></li>
                                            <li><a class="a_visitors" href="<?php echo site_url('visitors')?>"><i class=""></i>View Visitors</a></li>    
                                            <li><a href="<?php echo site_url('visitors/export')?>">Export Visitors</a></li>
                                        </ul>
                                    </li>
                                    <li id="a_giving"><a href="<?php echo site_url('giving')?>">Giving</a></li>
                                    <li id="a_schedules"><a href="<?php echo site_url('schedule')?>">Schedule</a></li>
                                    <li id="a_reports"><a href="<?php echo site_url('reports')?>">Reports</a></li>
                                    <li id="a_email" class="dropdown-submenu">
                                        <a href="javascript:void(0)">Email</a>
                                        <ul class="dropdown-menu pull-right" style="top: 46px;">
                                            <li><a href="<?php echo site_url('email')?>">Message</a></li>
                                            <li><a href="<?php echo site_url('email/icontact')?>">Newsletter</a></li>
                                        </ul>
                                    </li>
                                    <li id="a_paper"><a   href="<?php echo site_url('paper/new_order_step_1')?>">Postcards</a></li>                                                                                                         
                                <?php } ?>
                                <?php if( in_array($user->person_type, array(Person::TYPE_ADMIN)) ) {?>
                                    <li id="a_admin"><a  href="<?php echo site_url('admin')?>">Admin</a></li>
                                    <li id="a_admin_orders"><a  href="<?php echo site_url('admin/orders')?>">Orders</a></li>
                                    <li id="a_admin_postcards"><a href="<?php echo site_url('admin/postcards')?>">Postcards</a></li>
                                    <!--<li>
                                         <form class="navbar-form pull-right short">
                                            <?php echo form_dropdown('admin_company_id', $companies, $user->company_id ? $user->company_id : '0', ' id="select_admin_view_companies" class="span2"'  ); ?>
                                        </form>                                   
                                    </li>-->
                                                        
                                <?php } ?>                
                            </ul>
                        </div>
                    </div>                 
                </div>

        <?php } 
        
        if( isset($visitor_menu )){
        ?>
        <div id="order_submenu" class="subnav">
            <ul class="nav nav-tabs"> 

            </ul>
        </div>
        <?php }  
        if( isset($postcard_menu )){
            $caption = $order_type == Order::ORDER_LETTER ? 'Letter' : 'Postcard';
            $controller = $order_type == Order::ORDER_LETTER ? 'envelope' : 'paper';
        ?>
        <div id="order_submenu" class="subnav">
            <ul class="nav nav-tabs">
                    <li><a class='abtn-primary' href="<?php echo site_url($controller.'/new_order_step_1')?>"><i class=""></i>New <?php echo $caption ?></a></li> 
                    <li><a class='abtn-primary' href="<?php echo site_url($controller)?>"><i class=""></i><?php echo $caption ?>s</a></li>                    
                    <li><a class='abtn-primary' href="<?php echo site_url($controller.'/paper_progress')?>"><i class=""></i><?php echo $caption ?>s in progress</a></li>
                    <li><a class='abtn-primary' href="<?php echo site_url($controller.'/paper_sent')?>"><i class=""></i><?php echo $caption ?>s sent</a></li>
            </ul>
        </div>
        <?php } ?>
        <?php   
        if( isset($admin_order_menu)){
             
        ?>

        <?php } ?>                         
        <div class="<?php echo $class ?> clearfix">
            <section id="gridSystem"> 
