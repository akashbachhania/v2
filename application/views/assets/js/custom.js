
/* Default class modification */
$.extend( $.fn.dataTableExt.oStdClasses, {
    "sWrapper": "dataTables_wrapper form-inline"
} );

/* API method to get paging information */
$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
{
    return {
        "iStart":         oSettings._iDisplayStart,                                       
        "iEnd":           oSettings.fnDisplayEnd(),
        "iLength":        oSettings._iDisplayLength,
        "iTotal":         oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
}

/* Bootstrap style pagination control */
$.extend( $.fn.dataTableExt.oPagination, {
    "bootstrap": {
        "fnInit": function( oSettings, nPaging, fnDraw ) {
            var oLang = oSettings.oLanguage.oPaginate;
            var fnClickHandler = function ( e ) {
                e.preventDefault();
                if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
                    fnDraw( oSettings );
                }
            };

            $(nPaging).addClass('pagination').append(
                '<ul>'+
                    '<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+
                    '<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+
                '</ul>'
            );
            var els = $('a', nPaging);
            $(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
            $(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
        },

        "fnUpdate": function ( oSettings, fnDraw ) {
            var iListLength = 5;
            var oPaging = oSettings.oInstance.fnPagingInfo();
            var an = oSettings.aanFeatures.p;
            var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);

            if ( oPaging.iTotalPages < iListLength) {
                iStart = 1;
                iEnd = oPaging.iTotalPages;
            }
            else if ( oPaging.iPage <= iHalf ) {
                iStart = 1;
                iEnd = iListLength;
            } else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
                iStart = oPaging.iTotalPages - iListLength + 1;
                iEnd = oPaging.iTotalPages;
            } else {
                iStart = oPaging.iPage - iHalf + 1;
                iEnd = iStart + iListLength - 1;
            }

            for ( i=0, iLen=an.length ; i<iLen ; i++ ) {
                // Remove the middle elements
                $('li:gt(0)', an[i]).filter(':not(:last)').remove();

                // Add the new list items and their event handlers
                for ( j=iStart ; j<=iEnd ; j++ ) {
                    sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
                    $('<li '+sClass+'><a href="#">'+j+'</a></li>')
                        .insertBefore( $('li:last', an[i])[0] )
                        .bind('click', function (e) {
                            e.preventDefault();
                            oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
                            fnDraw( oSettings );
                        } );
                }

                // Add / remove disabled classes from the static elements
                if ( oPaging.iPage === 0 ) {
                    $('li:first', an[i]).addClass('disabled');
                } else {
                    $('li:first', an[i]).removeClass('disabled');
                }

                if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
                    $('li:last', an[i]).addClass('disabled');
                } else {
                    $('li:last', an[i]).removeClass('disabled');
                }
            }
        }
    }
} );
    

var map;
var geocoder;

$(document).ready( 
    function(){

$('.active').removeClass('active');

$('#hxistory_back').click(
    function(){
        alert('asdfas');
        window.history.go(-1);
    }
)
        
if(String(window.location).match(/index.php\/dashboard/)){
    
   $('#a_dashboard').addClass('active');
   
   $('table').addClass('table table-striped ');
   
   $('#div_visitors_table table').dataTable( {
            /*"sPaginationType": "bootstrap",*/
            "sDom": "<<l><f>r>t<'row'<'span2'i><p>>",
            /*"sDom": "<<'span1'lf>t<'row'<'span2'><p>>",*/
            "oLanguage": {
                "sInfo": "_TOTAL_ visitors (_START_ to _END_)",
                "sSearch": "Search:",
                "sLengthMenu": "_MENU_",
                "oPaginate": {
                     "sNext": "Next",
                     "sPrevious": "Prev",
                }                         
            },                     
        }
    );
    
    $('.div_dash a').tooltip();
    
    $('#chk_settings').change(
        function(){
            $('.vr_credentials').toggle('slow');
        }
    );
    
    $('#btn_new_users').click(
        function(){
            if(!confirm('This will create new user account, are you sure you want to proceed?')){
                return false;
            }
        }
    )

    $('#password_repeat').change(
        function(){
            var repeat = $('#password')
            if( repeat.attr('value') !== $(this).attr('value')){
                $(this).parent().parent().addClass('error');
            } else{
                $(this).parent().parent().removeClass('error');
            }
            
        }
    );
    
    draw_chart();
    update_feed_table(false);
    setInterval(function(){
        update_feed_table(true)    
    },3000);
    
} else if(String(window.location).match(/index.php\/print/)){
    
    var contents = window.opener.document.getElementById("div_map");
    document.write(contents.innerHTML);
    window.print();  
    
                              
} else if(String(window.location).match(/index.php\/email/)){
       
    $('.dropdown-toggle').dropdown();
    
    $('#a_email').addClass('active'); 
    
    $('table').removeClass('table-striped');
    $('table').addClass('table-bordered');
    
    $('#a_vertical').click(
        function(){
            $('#div_export').modal();            
        }
    );
    $('#select_visitors').multiselect();
    
    $('[id^="a_popover_toggle_"]').click(
        function(){
            $(this).popover('toggle');
        }
    )
    /**
    * Email text editor
    */
    $("#body").htmlarea();
    $('.jHtmlArea,.ToolBar,iframe').removeAttr('style');
    $('.ToolBar').children().last().remove();
    $('.ToolBar').children().last().prev().remove();
    
    
    
    $("#a_vertical").tooltip();
    $('label.radio').tooltip();
    
    var visitor_mails =  get_emails();
    var church_mails  =  get_church_emails();

       
    $( "#to" )
        // don't navigate away from the field on tab when selecting an item
        .bind( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                    $( this ).data( "autocomplete" ).menu.active ) {
                event.preventDefault();
            }
        })
        .autocomplete({
            minLength: 0,
            source: function( request, response ) {
                // delegate back to autocomplete, but extract the last term
                response( $.ui.autocomplete.filter(
                    visitor_mails , extractLast( request.term ) ) );
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            select: function( event, ui ) {
                var terms = split( this.value );
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push( ui.item.value );
                // add placeholder to get the comma-and-space at the end
                terms.push( "" );
                this.value = terms.join( ", " );
                return false;
            }
        });
    

    $( "#from" )
        // don't navigate away from the field on tab when selecting an item
        .bind( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                    $( this ).data( "autocomplete" ).menu.active ) {
                event.preventDefault();
            }
        })
        .autocomplete({
            minLength: 0,
            source: function( request, response ) {
                // delegate back to autocomplete, but extract the last term
                response( $.ui.autocomplete.filter(
                    church_mails , extractLast( request.term ) ) );
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            select: function( event, ui ) {
                var terms = split( this.value );
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push( ui.item.value );
                // add placeholder to get the comma-and-space at the end
                terms.push( "" );
                this.value = terms.join( ", " );
                return false;
            }
        });
        
        $("#select_recipients").multiselect({
           noneSelectedText: 'Select recipients',
           header: "Recipients"
        })
            
        $('#select_recipients').bind('multiselectclick',
            function(event, ui){
                var count = $('#select_recipients').multiselect('getChecked').length;
                if( ui.checked ){
                   // $('#form_visitors').append('<input type="hidden" name="mail_to[]" value="'+ui.value+'">');
                } else {
                   // $('#form_visitors').find('[name="mail_to[]"][value="'+ui.value+'"]').remove();
                }
            }
        );
        
        $('#select_all').click(
            function(){
                if($(this).attr('data-state') == 'select'){
                   $(this).attr('data-state','unselect');
                   $(this).text('Unselect all');
                   $("#select_recipients").multiselect('checkAll');
                } else {
                   $(this).attr('data-state','select');
                   $(this).text('Select all');
                   $("#select_recipients").multiselect('uncheckAll');
                }
            }
        )
        
        $('#add_new_list').click(
            function(){
                var new_list_name = $('#new_list_name').val();
                var exists = false;
                $('#list_name').children().each(
                    function(){
                        if( $(this).text() == new_list_name ){
                            alert('List with that name already exists');
                            exists = true;
                        }                        
                    }
                )
                if( !exists){
                    $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/email/ajax_add_new_list", 
                        { list_name: new_list_name },
                        function(data) {
                            var ret_val  = jQuery.parseJSON( data );                                   
                            if( ret_val['status'] == 'success'){
                                 $('#list_name').children().removeAttr('selected');
                                 $('#list_name').append(
                                    '<option value="'+ret_val['id']+'" selected="selected">'+new_list_name+'</option>'
                                 );
                            } else {
                                append_status_div('#div_export','error', ret_val['status']);
                            }                                     
                        }
                    );
                }
            }
        )
             
        $('#btn_export_list').click(
            function(){
                var members = $('#select_visitors').multiselect('getChecked');
                var ids = '';
                members.each(
                    function(){
                        ids += '-'+$(this).val();
                    }
                )
                
                $('#div_loader').remove();
                $('#div_export .modal-body').append('<div id="div_loader" class="loading_template"><img src="'+get_server_url('application/views/assets/img/ajax-loader.gif')+'"></div>');    

                $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/email/ajax_add_list_members", 
                    { ids: ids, list_id: $('#list_name').val() },
                    function(data) {
                        var ret_val  = jQuery.parseJSON( data );
                        $('#div_loader').remove();                                   
                        if( ret_val['status'] == 'success'){
                            append_status_div('#div_export','success', 'List members added');                           
                        } else {
                            append_status_div('#div_export','error', ret_val['status']);
                        }                                     
                    }
                );                
            }
        )
        
    
} else if(String(window.location).match(/index.php\/reports/)){
   $('#div_status').hide();
   $('#a_reports').addClass('active');
   /* google.load('visualization', '1.0', {'packages':['corechart']});
    google.setOnLoadCallback(draw_chart); */
        
} else if(String(window.location).match(/index.php\/visitors/) || String(window.location).match(/index.php\/members/))
{
    if(String(window.location).match(/index.php\/visitors/)) { 
        $('#a_visitors').addClass('active');
    } else if(String(window.location).match(/index.php\/members/)) {
        $('#a_members').addClass('active');
    }
        
    
    $('#div_visitors_table table, #div_visitors_total table').addClass('table table table-bordered');
    
    $('.form_tooltip').tooltip();
     /**
    * Datepicker                                   
    */

    $('[data-datepicker="datepicker"]').change(
        function(){
            var value = $(this).attr('value');
            var id = $(this).attr('id');

            if( id == 'start_date' || id == 'end_date'){
                $('#form_total_visits').submit();                        
            }
            if($(this).hasClass('assignment_date')) {
                var value = $(this).attr('value');
                value = value.replace('/', '-');
                value = value.replace('/', '-');
                $(this).attr('value', value);
            } else {
                $(this).attr('value', value.substring(5,7)+'-'+value.substring(8,10)+'-'+value.substring(0,4));
            }
            
        }
    );
        
    $('table').addClass('table');
    
    $('.dropdown-toggle').dropdown();
    /**
    * Notes input
    */
    $('[id^="notes_"]').click(
        function(){
            $(this).popover('toggle');                
        }        
    );
    $('[id^="dates_"]').click(function(){
            var id = $(this).attr('id');
            $('[id^="dates_"]').each(
                function(){
                    if( $(this).attr('id') != id ){
                        $(this).popover('hide');    
                    }
                }
            )
            $(this).popover('toggle');
            $( $('#'+$(this).attr('id')+'_table') ).dataTable( {
                                "bFilter": false,
                                "iDisplayLength":5,
                                "bLengthChange": false,
                                "sPaginationType": "bootstrap",
                                "oLanguage": {
                                    "sInfo": "_TOTAL_ visits (_START_ to _END_)",
                                    "sSearch": "Search:",
                                    "sLengthMenu": "_MENU_",
                                    "oPaginate": {
                                         "sNext": "Next",
                                         "sPrevious": "Prev",
                                    }                         
                                },
                                                                      
           });
           
           $('.popover .dataTables_paginate ul').children().hide();
           $('.popover .dataTables_paginate ul .prev, .dataTables_paginate ul .next').show();
   
           var middle = $(this).offset().top + ($(this).css('height').replace('px','') /2);
           var middle_pop = $('.popover').css('height').replace('px','')/2;
           
           $('.popover').css('top', middle-middle_pop );             
        }
    );    
    /*All visitors, visit date log*/
    /*
    $('[id^="dates_"]').hover(
        function(){
            $(this).popover('toggle');
        },
        function(){
            $(this).popover('toggle');
        }
    );*/
    
    $('th:contains("Visit")').click(
        function(){
            var form = $( document.createElement('form') );                  
            form.attr('action', String(window.location).match(/(.*)index.php/).pop()+'index.php/visitors/index_sort/'+ $('#sort').val());
            form.attr('method','get');
            $('.container').append(form);
            form.submit();             
        }
    )
    var selected_letter = $('#selected_letter').val();
    var bottom_title = 'visitors';
    var urlToFetch = String(window.location).match(/(.*)index.php/).pop()+'index.php/visitors/ajax_loading/'+selected_letter;
    if(String(window.location).match(/index.php\/members/)) {
        bottom_title = 'members';
        urlToFetch = String(window.location).match(/(.*)index.php/).pop()+'index.php/members/ajax_loading/'+selected_letter;
    }
    
    $('#div_visitors table').dataTable( {
        "aoColumns": [
            { "bSortable": false },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false , sWidth: '1%'},
            { "bSortable": false , sWidth: '1%'},
            { "bSortable": false }
            ],
        "bProcessing": true,
        "bStateSave": true,
        "bServerSide": true,
        "sAjaxSource": urlToFetch,
        "fnDrawCallback": afterLoadTable,
        //"sPaginationType": "bootstrap",
        "sDom": "<<l><f>r>t<'row'<'span2'i><p>>",
        /*"sDom": "<<'span1'lf>t<'row'<'span2'><p>>",*/
        "aaSorting": [],
        "oLanguage": {
            "sInfo": "_TOTAL_ "+bottom_title+" (_START_ to _END_)",
            "sSearch": "Search:",
            "sLengthMenu": "_MENU_",
            "oPaginate": {
                 "sNext": "Next",
                 "sPrevious": "Prev",
            }                         
        },                     
    });  
    
    $('#div_members table').dataTable({ 
        "aoColumns": [
            { "bSortable": false },
            // { "bSortable": false },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false , sWidth: '1%'},
            { "bSortable": false },
            { "bSortable": false }
            ],
        "bProcessing": true,
        "bStateSave": true,
        "bServerSide": true,
        "sAjaxSource": urlToFetch,
        "fnDrawCallback": afterLoadTable,
        //"sPaginationType": "bootstrap",
        "sDom": "<<l><f>r>t<'row'<'span2'i><p>>",
        /*"sDom": "<<'span1'lf>t<'row'<'span2'><p>>",*/
        "aaSorting": [],
        "oLanguage": {
            "sInfo": "_TOTAL_ "+bottom_title+" (_START_ to _END_)",
            "sSearch": "Search:",
            "sLengthMenu": "_MENU_",
            "oPaginate": {
                 "sNext": "Next",
                 "sPrevious": "Prev",
            }                         
        },                     
    });  
    
    
    set_google_map();

    $('#select_married').change( 
        function(){
            if($(this).attr('value') == '1'){
                
                $('#spouse').removeClass('hidden').prev().removeClass('hidden');
                /*
                $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/visitors/ajax_get_members", 
                    { },
                    //Fill in Person form with data
                    function(data) {
                        var ret_val  = jQuery.parseJSON( data );                   
                        for( var key in ret_val ){
                            var select_spouse = $('#select_spouse');
                            select_spouse.removeClass('hidden');
                            select_spouse.append('<option selected="selected" value="'+key+'">'+ret_val[key]+'</option>') 
                             
                        }                               
                    });
                   */                        
            } else {
                $('#spouse').addClass('hidden').attr('value','').prev().addClass('hidden');                                  
                $('#select_spouse').addClass('hidden');                                  
            }
        }
    )
    $('#select_spouse').change(
        function(){
            $('#spouse').attr('value',$(this).find('[value="'+$(this).attr('value')+'"]').text());
            $('#spouse_related_person_id').attr('value', $(this).attr('value'));
        }
    )

    if($('#spouse').attr('value')){
        $('#spouse').removeClass('hidden');
    }
    if($('#referer').attr('value')){
        $('#referer').removeClass('hidden').prev().removeClass('hidden');
    }
    $('#address,#state,#zip,#city').change( function(){set_google_map()} );
    /*
    $('#select_children').change(
        function(){
            var value = $(this).attr('value');
            
            if( value.match(/\d/)){
  
               var value = parseInt( value );
               var div   = $('#div_control_children');
               $('.children').hide().remove();
                
                $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/visitors/ajax_get_members", 
                { },
                //Fill in Person form with data
                function(data) {
                    var ret_val  = jQuery.parseJSON( data );
                    var x = ' <option selected="selected" value="'+key+'">No</option>'; 
                    for( var key in ret_val ){     
                        x+='<option  value="'+key+'">'+ret_val[key]+'</option>';                                     
                    }
                    
                    // Create dropdowns and input boxes for all children
                    
                    for(var i=1; i<=value; i++) {         
                        var element = 
                        '<div class="children control-group">'+
                            '<label class="control-label" for="input01">Child #'+i+':</label>'+
                            '<div class="controls">'+
                                '<input type="text" class="input-normal" name="child_'+i+'_related_person_name" id="child_name_'+i+'">'+
                                '<input type="hidden" class="input-medium" name="child_'+i+'_related_person_id" id="child_'+i+'related_person_id">'+
                                '<input type="hidden" class="input-medium" name="child_'+i+'_person_id" id="child_'+i+'person_id">'+                                 
                                '<input type="hidden" class="input-medium" name="child_'+i+'_relation_type" id="child_'+i+'relation_type" value="child">'+
                            '</div>'+
                        '</div>';
                                                     
                        div.after( element );
                        
                        $('.form_tooltip').tooltip();
                        
                    }                            
                                                          
                });                  
            }
        }
    )
    */
   
   $('#select_children').change(
       function(){
            var value = $(this).attr('value');
            if( value.match(/\d/)){
               var value = parseInt( value );
               var div   = $('#div_control_children');

               $('.children').hide().remove();
               
               for(var i=1; i<=value; i++) {         
                        var element = 
                        '<div class="children form_row">'+
                            '<label class="field_name align_right">Child #'+i+':</label>'+
                            '<div class="field">'+
                                '<input type="text" class="input-normal" name="child_'+i+'_related_person_name" id="child_name_'+i+'">'+
                                '<input type="hidden" class="span4" name="child_'+i+'_related_person_id" id="child_'+i+'related_person_id">'+
                                '<input type="hidden" class="span4" name="child_'+i+'_person_id" id="child_'+i+'person_id">'+                                 
                                '<input type="hidden" class="span4" name="child_'+i+'_relation_type" id="child_'+i+'relation_type" value="child">'+
                            '</div>'+
                        '</div>';
                                                     
                        div.after( element );
                        
                        $('.form_tooltip').tooltip();
                        
                    }

                                    
            }
        }
    )

    $('#refered_by').change(
        function(){
            if($(this).attr('value') == 'member'){
                $('#referer').removeClass('hidden').prev().removeClass('hidden');   
                $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/visitors/ajax_get_members", 
                    { },
                    //Fill in Person form with data
                    function(data) {
                        var ret_val  = jQuery.parseJSON( data );                   
                        for( var key in ret_val ){
                            var select  = $('#select_referer');
                            select.removeClass('hidden');
                            select.append('<option selected="selected" value="'+key+'">'+ret_val[key]+'</option>') 
                        }                               
                    });                        
            } else {
                $('#select_referer').addClass('hidden');    
            }                   
        }
    )
    $('#select_referer').change(
        function(){
            if($(this).attr('value') == 'add_new'){
                window.open(String(window.location).match(/(.*)index.php/).pop()+"index.php/visitors/new_visitor/no_header","Add new visitor", "status=1,height=1024,width=768");
            } else {
                $('#referer').attr('value',$(this).find('[value="'+$(this).attr('value')+'"]').first().text());
                $('#refered_by_related_person_id').attr('value', $(this).attr('value'));                
            }            
        }
    ) /*
    $('#new_assignment_doneby_person_id').change(
        function(){
            if($(this).attr('value') == 'add_new'){
                window.open(String(window.location).match(/(.*)index.php/).pop()+"index.php/visitors/new_visitor/no_header/done_by_added","Add new visitor", "status=1,height=1024,width=768");
            } else {
                $('#new_assignment_doneby_person_id').attr('value',$(this).find('[value="'+$(this).attr('value')+'"]').first().text());                
            }            
        }
    )*/    
    
    $('#select_month').change(
        function(){
            if( $(this).val() == '-'){ 
                $('#select_year, #select_day').val('-');
            }
        }
    )
    
    
    $('#month, #day, #year').change(
        function(){                    
            $('#birth').attr('value', $('#year').attr('value')+'-'+$('#month').attr('value')+'-'+$('#day').attr('value'));
        }
    )
    /*
    $('.add_new_type').change(
        function(){
            if($(this).attr('value') == 'add_new'){
                var type = $(this).attr('data-type');
                var id   = $(this).attr('id');
                window.open(String(window.location).match(/(.*)index.php/).pop()+"index.php/visitors/add_new/"+type+"/"+id,"Add new", "status=1,height=720,width=475");
            }            
        }
    )
    */
    if( $('#div_item_added').length > 0){
        window.opener.add_type( $('#id').val(), $('#name').val(), $('#type').val());    
    }
    
    if( $('#div_assignment_item_added').length > 0){
        
        window.opener.add_doneby_type( $('#div_assignment_item_added').attr('data-id'), $('#name').val(), $('#id').val());    
    }    
      
    if( $('#div_visitor_added').length > 0 && window.opener ){
        if( $('#done_by_added').length > 0){
            window.opener.add_visitor( $('#id').attr('value'), $('#first_name').attr('value')+' '+$('#last_name').attr('value'), 'done_by');    
        } else {
            window.opener.add_visitor( $('#id').attr('value'), $('#first_name').attr('value')+' '+$('#last_name').attr('value'), 'referer');    
        }
            
    }
    
    $('#add_new_assignment').click(
        function(){
             var index = $('.assignments').length;
             var id = $('#new_assignment_id').val();
             var date = $('#new_assignment_date').val();
             var done = $('#new_assignment_doneby_person_id').val();
             
             $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/visitors/ajax_get_new_assignment", 
                    { index: index,
                      id: id,
                      date: date,
                      done: done
                    },
                    function(data) {                            
                        $('.new_assignment').before( data );
                        
                        $('.assignments').last().hover(
                            function(){
                                $(this).find('.icon-minus').show();
                            },
                            function(){
                                $(this).find('.icon-minus').hide();    
                            }                        
                        )
                        
                        $('.assignments').last().find('.remove_assignment').click(
                            function(){
                                var id = $(this).attr('data-assignment-id');
                                if( id != 0 ){
                                    $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/visitors/ajax_remove_assignment", 
                                        { 
                                          id: id
                                        },
                                        function(data) {                            
                                            
                                        }
                                    );                
                                }
                                $(this).parent().parent().parent().remove();
                            }                        
                        )
                    }
             ); 
        }
    )
    
    
    $('#add_new_ministr').click(
        function(){
             var index = $('.ministries').length;
             var id = $('#new_ministry_id').val();
             
             $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/members/ajax_get_new_ministr", 
                    { index: index,
                      id: id
                    },
                    function(data) {                            
                        $('.new_ministry').before( data );
                        
                        $('.ministries').last().hover(
                            function(){
                                $(this).find('.icon-minus').show();
                            },
                            function(){
                                $(this).find('.icon-minus').hide();    
                            }                        
                        )
                        
                        $('.ministries').last().find('.remove_ministr').click(
                            function(){
                                var id = $(this).attr('data-ministr-id');
                                if( id != 0 ){
                                    $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/members/ajax_remove_ministr", 
                                        { 
                                          id: id
                                        },
                                        function(data) {                            
                                            
                                        }
                                    );                
                                }
                                $(this).parent().parent().parent().remove();
                            }                        
                        )
                    }
             ); 
        }
    )

    $(' .assignments,.ministries').hover(
        function(){
            $(this).find('.icon-minus').show();
        },
        function(){
            $(this).find('.icon-minus').hide();    
        }
    )   
      
    
    $(' .remove_assignment').click(
        function(){
            var id = $(this).attr('data-assignment-id');
            if( id != 0 ){
                $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/visitors/ajax_remove_assignment", 
                    { 
                      id: id
                    },
                    function(data) {                            
                        
                    }
                );                
            }
            $(this).parent().parent().parent().remove();
        }
    )
    
    $(' .remove_ministr').click(
        function(){
            var id = $(this).attr('data-ministr-id');
            if( id != 0 ){
                $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/members/ajax_remove_ministr", 
                    { 
                      id: id
                    },
                    function(data) {                            
                        
                    }
                );                
            }
            $(this).parent().parent().parent().remove();
        }
    )
    
    $('[data-batch-action]').click(
        function(){
            var form = $('#form_batch_action')
            form.attr('action', $(this).attr('data-batch-action'));
            form.submit();            
        }
    )
      

                             
} else if(String(window.location).match(/index.php\/paper|envelope/)){
    
    if( String(window.location).match(/envelope/)){
        $('#a_letter').addClass('active');    
    } else {
        $('#a_paper').addClass('active');    
    }
    
    var selected_cover = '';
     
    if(String(window.location).match(/(paper|envelope)\/(new_order|final_step|finalize)/)){
        
        Galleria.loadTheme( String(window.location).match(/(.*)index.php/).pop()+'application/views/assets/js/galleria/galleria.classic.min.js');

        if(String(window.location).match(/(paper|envelope)\/new_order/)){
            
            Galleria.ready(function() {

                this.bind("thumbnail", function(e) {
                     
                    Galleria.log(this); // the gallery scope
                    Galleria.log(e) // the event object
                });

                this.bind("loadstart", function(e) {
                    if ( !e.cached ) {
                        Galleria.log(e.target + ' is not cached. Begin preload...');
                    } 
                    var id = $(this.getData().original).attr('id'); 
                    $('#front_side').attr('value', id);
                    $('#back_side').attr('value', id);
                });
            });
                    
            Galleria.run('#div_galleria', {
                transition: 'fade',
                imageCrop: false,
                height: 600
            }); 
                       
        } else if(String(window.location).match(/(paper|envelope)\/(final_step|finalize)/)){

            $('#a_change_back,#a_change_front').tooltip();
            $('#div_addresses').toggle();
            Galleria.ready(function() {

                this.bind("loadstart", function(e) {
                    if ( !e.cached ) {
                        Galleria.log(e.target + ' is not cached. Begin preload...');
                    } 
                    var id   = $(this.getData().original).attr('id'); 
                    var side = $(this.getData().original).attr('data-side'); 
                    
                    $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/paper/ajax_letter", 
                        { 
                            id: id, side: side
                        },
                        //Fill in Person form with data
                        function(data) {
                            var ret_val  = jQuery.parseJSON( data );                                   
                            if( ret_val['status'] == 'success'){                                 
                                    var src = $('#row_front :nth-child(1)').children().first().attr('src');
                                    $('#row_'+selected_cover+' :nth-child(1)').children().first().attr('src', src.replace(/\/\d+_thumb.+/, '/'+ret_val['path_thumb']));
                                    $('#row_'+selected_cover+' :nth-child(3)').text(ret_val['description']);
                                    $('#row_'+selected_cover+' :nth-child(4)').text('$'+ret_val['price']);
                                    $('#row_'+selected_cover+' :nth-child(6)').text('$'+Number(ret_val['price'])*Number($('#row_'+selected_cover+' :nth-child(5)').text()));                                                                      
                                    
                                    $('.amount').text('$'+ret_val['total']);
                                    $('#amount').val(ret_val['total']);
                            }
                        });
                    
                });
            });
            
            $('#select_recipients').bind('multiselectclick',
                function(event, ui){
                    var count = $('#select_recipients').multiselect('getChecked').length;
                    $('#row_front :nth-child(5)').text(count);
                    $('#row_back :nth-child(5)').text(count);
                    
                    $('#row_front :nth-child(6)').text('$'+Number($('#row_front :nth-child(4)').text().replace(/\$/,''))*Number(count));
                    $('#row_back :nth-child(6)').text('$'+Number($('#row_back :nth-child(4)').text().replace(/\$/,''))*Number(count));
                    
                    if( !ui.checked){
                        $('#span_'+ui.value).hide();
                        session_user(ui.value,'0');
                    } else {
                        if( $('#span_'+ui.value).length > 0 ){
                            $('#span_'+ui.value).show();
                            session_user(ui.value,'1');     
                        } else {
                            $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/paper/get_recipient", 
                                { 
                                    id: ui.value,
                                },
                                //Fill in Person form with data
                                function(data) {
                                    var ret_val  = jQuery.parseJSON( data );                                   
                                    if( ret_val['status'] == 'success'){                                 
                                          $('#div_addresses').append( ret_val['address']);
                                    }
                                });                           
                        }
                           
                    }
                    $('.amount').text('$'+(Number($('#row_front :nth-child(6)').text().replace(/\$/,'')) + Number($('#row_back :nth-child(6)').text().replace(/\$/,''))));
                }
            );            
            $('#a_change_front').click(
                function(){
                   
                    $('#letter_front_modal').modal('toggle');
                    selected_cover = 'front';
                    Galleria.run('#div_galleria_front', {
                        transition: 'fade',
                        imageCrop: false,
                        height: 400
                    });     
                }
            )
            $('#a_change_back').click(
                function(){
                    $('#letter_back_modal').modal('toggle');
                    selected_cover = 'back'; 
                    Galleria.run('#div_galleria_back', {
                        transition: 'fade',
                        imageCrop: false,
                        height: 400
                    });                        
                }
            )
            
            $('#a_show_addr').click(
                function(){
                    $('#div_addresses').toggle('slow');
                    if($(this).text() == 'Show Adresses'){
                        $(this).text('Hide Addresses');
                    } else {
                        $(this).text('Show Adresses');     
                    }
                }
            )
            
            $('#btn_charge').click(
                function(){
                    $('#charge_modal').modal('toggle');
                }
            )
            
            $('#btn_charge_modal').click(
                function(){                        
                    $('#div_order_charged').hide();
                    var cc_num  = $('#cc_num');
                    var cc_date = $('#cc_date');
                    var amount  = $('#amount');
                    var error   = false;
                    
                    if( cc_num.attr('value')=="" || isNaN(cc_num.attr('value'))){
                       cc_num.next().text('Invalid CC number value');
                       cc_num.parent().parent().addClass('error');
                       error = true;                            
                    }
                    if( cc_date.attr('value')=="" || isNaN(cc_date.attr('value'))){
                       cc_date.next().text('Invalid Expire date value');
                       cc_date.parent().parent().addClass('error');
                       error = true                            
                    }
                    if( amount.attr('value')=="" || isNaN(amount.attr('value'))){
                       amount.next().text('Invalid amount!');
                       amount.parent().parent().parent().addClass('error');
                       error = true                              
                    }
                    if( error ){
                        $('#div_order_charged span').text('Please correct the fields below');
                        $('#div_order_charged').addClass('alert alert-error').show();
                    } else {
                        if(confirm('Are you sure you want to charge this credit card with '+amount.attr('value')+'$?')){
                            $('#form_cc').submit();
                            $('#charge_modal').modal('toggle');
                        }
                    }
                }
            )
            $('[name="span_help"]').tooltip();
            
            $('.fill_in').hide();
            
            $('#select_fill_in').change(
                function(){
                    
                    var value = $(this).attr('value');
                    var prefix  = null;
                    var fill_in = false;
                    
                    var array = new Array('name','address','zip','city');
                    switch(value){
                        case '1':
                            for( key in array ){
                                $('#charge_modal .fill_in [name="'+array[key]+'"]').attr('value', '');
                            }
                            $('#charge_modal [name="state"]').find('[selected="selected"]').removeAttr('selected');                              
                        break;
                        case '2':
                            fill_in = true;
                            prefix = 'user';
                        break;
                        case '3':
                            fill_in = true;
                            prefix = 'company';                            
                        break;
                        case '5':
                            fill_in = true;
                            for( key in array ){
                                $('#charge_modal .fill_in [name="'+array[key]+'"]').attr('value', '');
                            }
                            $('#charge_modal [name="state"]').find('[selected="selected"]').removeAttr('selected');                                
                        break;                                                                                                                
                    }
                    
                    if( fill_in ){
                        $('.fill_in').show();
                    } else {
                        $('.fill_in').hide();
                    }

                    if( prefix){
                        $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/paper/get_details", 
                            { 
                                id: prefix,
                            },
                            //Fill in Person form with data
                            function(data) {
                                var ret_val  = jQuery.parseJSON( data );
                                
                                if( ret_val['status'] == 'success'){
                                    $('#charge_modal .fill_in input').attr('value','');
                                    for( key in ret_val['details'] ){                                            
                                        if( key == 'name'){
                                            var names = ret_val['details']['name'].split(' ');
                                            $('#charge_modal [name="last_name"]').attr('value','');
                                            for( index in names ){
                                                if( index == 0 ){
                                                    $('#charge_modal [name="first_name"]').attr('value',names[index]);
                                                } else{
                                                    $('#charge_modal [name="last_name"]').attr('value', $('#charge_modal [name="last_name"]').attr('value') + ' ' + names[index]); 
                                                }
                                            }                                                    
                                        } else if( key == 'state') {
                                            $('#charge_modal [name="state"]').find('[selected="selected"]').removeAttr('selected');                            
                                            $('#charge_modal [name="state"]').find('[value="'+ret_val['details']['state']+'"]').attr('selected','selected');                                                
                                        } else {
                                            $('#charge_modal [name="'+key+'"]').attr('value', ret_val['details'][key]);    
                                        }
                                    }
                                    $('#charge_modal [name="country"]').attr('value','US');                                                                                                                            
                                }                                     
                            });                             
                    } 
                    
                })
                
                $('#btn_cancel').click(
                    function(){
                        if( confirm('Are you sure you want to cancel your order?')){
                            window.location = String(window.location).match(/(.*)index.php/).pop()+"index.php/paper"
                        }
                    }
                )            
        }
        
    }
    
    $('#div_covers table thead tr').children().first().addClass('right'); 
    
    $("#select_recipients").multiselect({
       noneSelectedText: 'Select recipients',
       header: "Recipients"
    });
    
    $('#select_all').click(
        function(){
            if($(this).attr('data-state') == 'select'){
               $(this).attr('data-state','unselect');
               $(this).text('Unselect all');
               $("#select_recipients").multiselect('checkAll');
            } else {
               $(this).attr('data-state','select');
               $(this).text('Select all');
               $("#select_recipients").multiselect('uncheckAll');
            }
        }
    )    
               
    $('#div_print table').dataTable( {
                "bSort": false,
                "bFilter": false,
                "iDisplayLength":2,
                "sPaginationType": "bootstrap",
                "bLengthChange": false,
                "sDom": "<<l><f>r>t<'row'<'span2'><p>>",
                "oLanguage": {
                    "sInfo": "_TOTAL_ covers (_START_ to _END_)",
                    "sSearch": "Search:",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                         "sNext": "Next",
                         "sPrevious": "Prev",
                    }                         
                },                     
            }
    );
    $('#div_postcards_inner table').dataTable( {
/*                "sPaginationType": "bootstrap",*/
                "oLanguage": {
                    "sInfo": "_TOTAL_ postcards (_START_ to _END_)",
                    "sSearch": "Search:",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                         "sNext": "Next",
                         "sPrevious": "Prev",
                    }                         
                },                     
            }
    );    
    $('#div_postcards_inner table span').popover();
} else if(String(window.location).match(/index.php\/admin/)){
    
    
    if( String(window.location).match(/orders/)){
        $('#a_admin_orders').addClass('active');    
    } else if(String(window.location).match(/postcards|add_new_postcard/) ){
        $('#a_admin_postcards').addClass('active');    
    } else {
        $('#a_admin').addClass('active');         
    }
     
    if( String(window.location).match(/orders|postcards|add_new_postcard/)){
        $('#btn_add_new').click(
            function(){
                $('#div_add_new').modal();            
            }
        );
        
        var anOpen = []; 
        var oTable =  $('#div_postcards_inner table').dataTable( {
                        "bProcessing": true,
                        "iDisplayLength":5,
                        "bLengthChange": false,
                        "sPaginationType": "bootstrap",
                        "oLanguage": {
                            "sInfo": "_TOTAL_ postcards (_START_ to _END_)",
                            "sSearch": "Search:",
                            "sLengthMenu": "_MENU_",
                            "oPaginate": {
                                 "sNext": "Next",
                                 "sPrevious": "Prev",
                            }                         
                        },
                                                              
                    }
        );
            
        $( oTable.fnGetNodes()).each( function () {
           $(this).children().first().click(
                function(){
                   var nTr = this.parentNode;;
                   var i = $.inArray( nTr, anOpen );
                   var id = $(this).children().first().attr('id'); 
                   if ( i === -1 ) {
                      $('img', this).attr( 'src', String(window.location).match(/(.*)index.php/).pop()+'application/views/assets/img/remove-icon.png' );
                      
                      if( String(window.location).match(/admin\/orders/)){
                            fnFormatDetails(oTable, nTr, id,'details');    
                      } else if( String(window.location).match(/admin\/postcards|add_new_postcard/)){
                            fnFormatDetailsEdit(oTable, nTr, id,'details');
                      }
                      anOpen.push( nTr );
                      
                    }
                    else {
                      $('img', this).attr( 'src', String(window.location).match(/(.*)index.php/).pop()+'application/views/assets/img/add-icon.png' );
                      oTable.fnClose( nTr );
                      anOpen.splice( i, 1 );
                    }                
                }
           )

        } );
         
        function fnFormatDetails( oTable, nTr, id, css_class )
        {
            var sOut;
            $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/admin/get_recipients", 
                {
                    id: id
                },
                function(data) {
                    var addr  = jQuery.parseJSON( data );
                    var row = new Array();
                    var table = '';
                    var html_table = '';
                    
                    for( var key in addr ){
                          
                         address=  '<td>'+
                                    '<address>'+
                                        addr[key]['address']+
                                    '</address>'+
                                    '</td>'
                                    ;
                         
                         if( row.length > 3){
                             var temp_row = '<tr>';
                             for( var i=0; i<4; i++){
                                temp_row += row[i];
                             }
                             temp_row+='</tr>';
                             table += temp_row;
                             row = new Array();
                             temp_row = '';
                         } else {

                                row.push(address);     

                         }
                    }
                    if(row.length > 0){
                         var temp_row = '<tr>';
                         for( var i=0; i<4; i++){
                            if( row[i] != undefined){
                                temp_row += row[i];
                            }
                         }
                         temp_row+='</tr>';
                         table += temp_row;
                         row = new Array();
                         temp_row = '';                         
                    }
                    
                    var html_table =
                    '<legend>Recipient Addresses</legend>'+
                    '<table>'+
                        table +
                    '</table>';
                    sOut =
                            '<div class="innerDetails">'+
                              html_table+
                            '</div>';
                    oTable.fnOpen( nTr, sOut, css_class );                                                                                                    
                }
            );      
        
            return sOut;
        } 
      
         
        function fnFormatDetailsEdit( oTable, nTr, id, css_class )
        {
            var html =    
            '<form class="bs-docs-example form-horizontal"> '+
                '<legend>Edit Postcard</legend>'+
                '<div id="div_order_added_'+id+'" class="hidden">'+
                        '<a class="close" data-dismiss="alert">�</a>'+
                        '<span></span>'+
                '</div>'+    
                '<div class="control-group">'+
                    '<label for="inputEmail" class="control-label">Description</label>'+
                    '<div class="controls">'+
                        '<input type="text" placeholder="Description" value="'+$('#td_description_'+id).text()+'" id="description_'+id+'"/>'+
                    '</div>'+
               '</div>'+
               '<div class="control-group">'+ 
                    '<label for="inputEmail" class="control-label">Price</label>'+
                    '<div class="controls">'+
                        '<input type="text" placeholder="Price" value="'+$('#td_price_'+id).text()+'" id="price_'+id+'"/>'+
                    '</div>'+
               '</div>'+
               '<div class="control-group">'+ 
                    '<label for="inputEmail" class="control-label">Front side Postcard</label>'+
                    '<div class="controls">'+
                        '<label class="checkbox">'+
                            '<input id="type_front_side_postcard_'+id+'" value="1" type="checkbox" '+($('#td_type_'+id).attr('data-front-postcard')=='true' ? ' checked=="checked" ' : '')+'>'+
                        '</label>'+
                    '</div>'+
               '</div>'+
               '<div class="control-group">'+ 
                    '<label for="inputEmail" class="control-label">Back side Postcard</label>'+
                    '<div class="controls">'+
                        '<label class="checkbox">'+
                            '<input id="type_back_side_postcard_'+id+'" value="1" type="checkbox" '+($('#td_type_'+id).attr('data-back-postcard')=='true' ? ' checked=="checked" ' : '')+' >'+
                        '</label>'+
                    '</div>'+
               '</div>'+                                
               '<div class="control-group">'+ 
                    '<label for="inputEmail" class="control-label">Status</label>'+
                    '<div class="controls">'+
                        '<select id="active_'+id+'">'+
                            '<option value="1" '+( $('#td_active_'+id).text() == 'Active'? 'selected="selected"':'')+'>Active</option>'+
                            '<option value="0" '+( $('#td_active_'+id).text() == 'Disabled'? 'selected="selected"':'')+'>Disabled</option>'+
                        '</select>'+
                    '</div>'+
               '</div>'+                       
               '<div class="control-group">'+ 
                    '<div class="controls">'+
                        '<a class="btn btn-info" onclick="save_letter('+id+',this)">Save</a>'+
                        '<a class="btn btn-info" id="btn_close_'+id+'" onclick="$(\'#row_'+id+'\').children().first().trigger(\'click\')">Cancel</a>'+
                    '</div>'+
               '</div>'+                          
            '</form>';                    
            oTable.fnOpen( nTr, html, css_class );
        } 
    } 

                
                if( $('#div_home_messages').length > 0 ) {
                    $('#a_home').addClass('active').parent().addClass('active');
                }
                if( $('#div_order_details').length > 0 || $('#div_view_orders').length > 0 ) {
                    $('#a_orders').addClass('active').parent().addClass('active');
                }            
                if( $('#div_clients_table').length > 0 ) {
                    $('#a_clients').addClass('active').parent().addClass('active');
                } 
                if( $('#div_inspectors_table').length > 0 ) {
                    $('#a_inspectors').addClass('active').parent().addClass('active');
                } 
                if( $('#div_schedule').length > 0 ) {
                    $('#a_schedule').addClass('active').parent().addClass('active');
                }  
                if( $('#div_stats_filter').length > 0 ) {
                    $('#a_stats').addClass('active').parent().addClass('active');
                }
                if( $('#div_companies_table').length > 0 ) {
                    $('#a_admin').addClass('active').parent().addClass('active');
                } 
                
                $('.alert').click(
                    function(){
                        $(this).hide();
                    }
                )
      

                $('#btn_window_close').click(
                    function(){
                        window.close();
                    }
                )
                /**
                * Set default look for all tables                        
                */
                
                /**
                * Admin
                */
                $('#company_select').change(
                    function(){
                        $('#form_companies').submit();
                    }
                )
                /**
                * Types
                */
                $('#type_select').change(
                    function(){
                        $('#form_types').submit();
                    }
                )    
      
                /**
                * Admin company select box
                */
                $('#select_admin_view_companies').change(
                    function(){
                        $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"/admin/ajax_control", 
                            { company_id: $(this).attr('value') },
                            //Fill in Person form with data
                            function(data) {                            
                                location.reload();
                            });                    
                    }
                )
                         
                 
    } else if(String(window.location).match(/index.php\/schedule/)){
        if( $('#select_refresh').attr('value') != undefined ){            
            window.onunload = function (e) {
                opener.refreshItems(); 
            };           
        } 
        $('[name="select_edit"]').change(
                function(){
                   var item_id   = $(this).attr('value');
                   var ministry_id   = $('#ministry_id').attr('value');
                   document.location.href=String(window.location).match(/(.*)index.php/).pop()+"index.php/schedule/add_new/"+ministry_id+"/"+item_id;
                }
            )
    } else if(String(window.location).match(/index.php\/giving/)){
        if( $('#select_refresh').attr('value') != undefined ){            
            window.onunload = function (e) {
                opener.refreshItems(); 
            };           
        } 
        $('[name="select_edit"]').change(
                function(){
                   var item_id   = $(this).attr('value');                   
                   document.location.href=String(window.location).match(/(.*)index.php/).pop()+"index.php/giving/add_new/6/"+item_id;
                }
            )
    }

                
   
   
 
  
            /**
            * Admin
            */
            $('#company_select').change(
                function(){
                    $('#form_companies').submit();
                }
            )
            /**
            * Types
            */
            $(".collapse").collapse();
            $('#type_select').change(
                function(){
                    $('#form_types').submit();
                }
            )
    
    
                // $('[name="select_edit"]').change(function(){					
                //     var item_type = $('[name="object_type"]').attr('value');
                //     var item_id   = $(this).attr('value');
                //     var selected_item_type = 'funds';
                //     $.post(String(window.location).match(/(.*)(?:home|visitors)/).pop()+"visitors/ajax_control",
                //     { type: selected_item_type, item_type: item_type, id: item_id },
                //     function(data) {	
                //             var ret_val  = jQuery.parseJSON( data );						 
                //             for( var key in ret_val ){							 
                //                 $('#' + key).attr('value', ret_val[ key ]);
                //             }
                //     });
					
                // });

               /* $(document).on('submit','form.well',function(){
                        var datastring = 'name=' + $('form.well input#name').val() + '&description=' + $('form.well input#description').val() + '&active=' + $('form.well select#active').val() + '&company_id=' + $('form.well input#company_id').val() + '&type=' + $('form.well input#type').val() + '&object_type=' + $('form.well input[name="object_type"]').val() + '&control_id=' + $('form.well input[name="control_id"]').val() + '&save_p=' + $('form.well input[name="save_p"]').val();
                        
                        $.ajax({
                            type: "GET",
                            url: String(window.location).match(/(.*)(?:home|visitors|members)/).pop()+"visitors/add_new_s",
                            data: datastring,
                            success: function(result){
                                alert(result);
                            }
                        });
                        return false;                 
                })*/

    
            
        }
        
        
);
/**
* Select box - Item change callback - Fills in description and price. Recalculates order value
*/
function change_val(){

    var item_type = $('[name="object_type"]').attr('value');
    var item_id   = $('[name="select_edit"]').attr('value');
    var selected_item_type = 'funds';
    
    $.post(String(window.location).match(/(.*)(?:home|visitors|members)/).pop()+"visitors/ajax_control",
        { type: selected_item_type, item_type: item_type, id: item_id },
        function(data) {    
                var ret_val  = jQuery.parseJSON( data );                         
                for( var key in ret_val ){   
                    if(key == 'id'){
                        $('.well #' + key).attr('value', ret_val[ key ]);
                        //div_add_new_dropdown
                    }     
                    else{                 
                        $('#' + key).attr('value', ret_val[ key ]);
                    }
                
                }
    });

    
    
}
function submit_via_ajax(){

}
function item_change(){
    
    if( $(this).attr('value').match(/new_item/)){
      var name = $(this).attr('name');
      var type = $(this).attr('value');
      window.open(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"/orders/add_new?type="+type+"&select_name="+name,"Add new", "status=1,height=720,width=475");                    
    } else {
     //Send AJAX request
     $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"/orders/ajax_control", 
            { value: $(this).attr('value'), name: $(this).attr('name'), type: 'item'},
            //Fill in Item description and price, recalculate new totals, etc.
            function(data) {                            
                 var ret_val  = jQuery.parseJSON( data );
                 $('#item_description_' + ret_val["row"]).attr('value', ret_val["description"]);
                 $('#item_price_' + ret_val["row"]).attr('value', ret_val["price"]);
                 
                 
                 if($('#item_price_' + ret_val["row"]).attr('value').indexOf('.') == -1 ){
                    $('#item_price_' + ret_val["row"]).attr('value', $('#item_price_' + ret_val["row"]).attr('value') + '.00' );      
                 }               
                 /**
                 * Perform calculation
                 */
                 calculate();
            });                        
    }
                    
}

function get_message_div( data )
{
  var div =
    '<div id="div_message_drill">'+
      '<table>'+
        '<tr><td class="drill_down_message"><b>Message</b></td><td><textarea class="ta_home_message" rows ="3" name="message_in">'+data['message']+'</textarea></td></tr>'+
        '<tr><td class="drill_down_message"><b>Response</b></td><td><textarea class="ta_home_message" name="message" rows="3"></textarea></td></tr>'+
      '</table>'+
      '<input name="btn_send"   type="submit" class="btn" value="Reply"/>'+
      '<input name="btn_delete" type="submit" class="btn" value="Delete"/>'+
      '<input name="to_user_id" type="hidden" class="btn" value="'+data['from_user_id']+'"/>'+
      '<input name="type" type="hidden" class="btn" value="'+data['type']+'"/>'+
      '<input name="message_id" type="hidden" class="btn" value="'+data['message_id']+'"/>'+
    '</div>';
    
  return div;
}
function message_send(){
               
    var message    = $(this).parent().find('[name="message"]').text();
    var to_user_id = $(this).parent().find('[name="to_user_id"]').attr('value');
    var type       = $(this).parent().find('[name="type"]').attr('value');
    
    if( to_user_id == null ){
        to_user_id = $(this).parent().find('[name="to_user_id"]').attr('value');
    }
    
    var parent = $(this).parent();
     
    $.post(String(window.location).match(/(.*)(?:home|orders|clients|stats|inspectors|schedule|admin)/).pop()+"/home/send_message", 
        { message: escape(message), to_user_id: to_user_id, type: type },
        //Fill in Item description and price, recalculate new totals, etc.
        function(data) {
            var response = jQuery.parseJSON( data );
            /**
            if( response['response'] == 'success'){
                //success div
                //disable reply    
            } else {
                
            } */
            
        });                         
    return false;                 
}
  
  
 
  
  

function refresh_all(){
    $('#name, #client_name, #inspector_id, #terms, #type_of_inspection, #type_of_structure, #type_of_foundation, #type_of_utilities').trigger('click');
}
function refresh_item_select( id ){
    // alert(id);
    //var parent = $('[name="'+id+'"]');
    
    $.post(String(window.location).match(/(.*)(?:home|visitors|members)/).pop()+"visitors/ajax_select_refresh", 
        { id:id },
        function(result) {
            
            var ret_val  = jQuery.parseJSON( result );
            var ids      = new Array();  
            var texts    = new Array();
            var results    = new Array(); 

            for( var key in ret_val ){
                results.push(ret_val[key]);
            }
            $('[name="'+id+'"] > option').each(function() {
                if( jQuery.inArray(this.text ,results) == -1){
                    $(this).remove();
                }
            });

            var parent = $('[name="'+id+'"]');

            parent.children().each(
                function(){
                    ids.push( $(this).attr('value'));
                    //texts.push( $(this).text())
                }
            )
            
            for( var key in ret_val ){
                if( jQuery.inArray(key ,ids) == -1){
                    parent.children().removeAttr('selected');
                    parent.append('<option selected="selected" value="'+key+'">'+ret_val[key]+'</option>');
                    parent.trigger('change');
                }
            }
        });
}
function get_url(){
    var location = String(window.location);
}
function format_currency( object){
    /* 
    if(object.text().indexOf('.') == -1 ){
       object.text( object.text() + '.00');
    }
    object.text( object.text() + '$');*/
    object.formatCurrency();
    object.parent().addClass('right');   
}
/**
* Visitors
*/
function set_google_map(){
    var center = escape($('#address').attr('value')+','+$('#city').attr('value')+','+$('#state').attr('value')+','+$('#zip').attr('value'));         
    var src = 'http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q='+center+'&ie=UTF8&z=12&t=m&iwloc=near&output=embed';
               
    $('#frame1').attr('src', src );
    $('#map_print').attr('href','http://maps.googleapis.com/maps/api/staticmap?center='+center+'&zoom=14&size=1024x768&sensor=false&markers=size:mid%7Ccolor:red%7C'+center);                    
}
function split( val ) {
    return val.split( /,\s*/ );
}
function extractLast( term ) {
    return split( term ).pop();
}
function get_emails(){
    var emails = new Array();
    $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/email/ajax_get_emails", 
        {},
        function(data) {
            
            var ret_val  = jQuery.parseJSON( data );
            for( var key in ret_val ){
                emails.push(ret_val[key]);
            }
        });
    return emails;    
}
function get_church_emails(){
    var emails = new Array();
    $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/email/ajax_get_church_emails", 
        {},
        function(data) {
            
            var ret_val  = jQuery.parseJSON( data );
            for( var key in ret_val ){
                emails.push(ret_val[key]);
            }
        });
    return emails;    
}
function save_notes(id){
    var emails = new Array();
    var notes  = $("#text_notes_"+id).attr('value');
    var attachUrl = String(window.location).match(/(.*)index.php/).pop()+"index.php/visitors/ajax_save_notes";
    if(String(window.location).match(/index.php\/members/)) {
        attachUrl = String(window.location).match(/(.*)index.php/).pop()+"index.php/members/ajax_save_notes";
    }

    if(String(window.location).match(/index.php\/giving/)) {
        attachUrl = String(window.location).match(/(.*)index.php/).pop()+"index.php/giving/ajax_save_notes";
    }

    $.post(attachUrl, 
        {
            notes: notes,
            id: id
        },
        function(data) {            
            var ret_val  = jQuery.parseJSON( data );
            if( ret_val == 'success'){
                $('#div_status_'+id).addClass('alert alert-success').removeClass('hidden');
                $('#div_status_'+id).text('User updated');
                $('#btn_cancel_'+id).text('Close');
                $('#notes_'+id).attr('data-content', $('#div_status_'+id).parent().html());
                
                var replacement = $('#notes_'+id).attr('data-content').replace(/id="text_notes_120">.*?<\/textarea>/,'id="text_notes_"'+id+'>'+notes+'<\/textarea>');
                $('#notes_'+id).attr('data-content', replacement );                
                
            } else {
                $('#div_status_'+id).addClass('alert alert-error').removeClass('hidden');
                $('#div_status_'+id).show();
                $('#div_status_'+id).html('<strong>Update failed</strong>');                
            }
    });
    return emails;        
}
function visitor(type,id){
    
     
    $.ajax({
           type: "POST",
           url: String(window.location).match(/(.*)index.php/).pop()+'index.php/visitors/new_visitor',
           data: $('#form_new_visitor').serialize(),
           success: function(data){
           }
    });        
     
    
    var form = $( document.createElement('form') );                  
    form.attr('action', String(window.location).match(/(.*)index.php/).pop()+'index.php/visitors/process/'+id+'/'+type);
    form.attr('method','get');
    $('.container').append(form);
    form.submit();    
}
function member(type,id){
    
     
    $.ajax({
           type: "POST",
           url: String(window.location).match(/(.*)index.php/).pop()+'index.php/members/new_member',
           data: $('#form_new_visitor').serialize(),
           success: function(data){
           }
    });        
     
    var form = $( document.createElement('form') );                  
    form.attr('action', String(window.location).match(/(.*)index.php/).pop()+'index.php/members/process/'+id+'/'+type);
    form.attr('method','get');
    $('.container').append(form);
    form.submit();    
}
function hide_notes(id){
    $('#notes_'+id).popover('toggle');
    var url = String(window.location).match(/(.*)index.php/).pop()+'index.php/visitors/get_visitor_notes_popup/';
    
    if(String(window.location).match(/index.php\/giving/)){
        url = String(window.location).match(/(.*)index.php/).pop()+'index.php/giving/get_visitor_notes_popup/';
    }

    if(String(window.location).match(/index.php\/members/)){
        url = String(window.location).match(/(.*)index.php/).pop()+'index.php/members/get_visitor_notes_popup/';
    }


    $.post(url, {id:id},function(data){
        var parent = $('#notes_'+id).parent();
        $('#notes_'+id).remove();                
        parent.append(data);                 
        $('#notes_'+id).click(
            function(){
                $(this).popover('toggle');        
            }
        );  
    });   
}

function hide_notes_members(id){
    $('#notes_'+id).popover('toggle');
    
    var url = String(window.location).match(/(.*)index.php/).pop()+'index.php/members/get_visitor_notes_popup/';
    if(String(window.location).match(/index.php\/giving/)){
        url = String(window.location).match(/(.*)index.php/).pop()+'index.php/giving/get_visitor_notes_popup/';
    }

    $.post(url, {id:id},function(data){
        var parent = $('#notes_'+id).parent();
        $('#notes_'+id).remove();                
        parent.append(data);                 
        $('#notes_'+id).click(
            function(){
                $(this).popover('toggle');        
            }
        );  
    });   
}

function chaord( order_id, obj  ){
    
    var status = $(obj).attr('value');
    //Update Status
    //Sent Notification email
    $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/admin/ajax_change", 
        {
            id: order_id,
            status: status
        },
        function(data) {            
            var ret_val  = jQuery.parseJSON( data );
            $('#row_'+order_id).removeClass().addClass(ret_val); 
    });    
}
function save_letter(id,obj){
    
    var price = $('#price_'+id).removeClass().attr('value');
    var description = $('#description_'+id).removeClass().attr('value');
    var front_postcard = $('#type_front_side_postcard_'+id).is(':checked')? 1 : 0;
    var back_postcard  = $('#type_back_side_postcard_'+id).is(':checked') ? 1 : 0;
    var front_letter   = $('#type_front_side_letter_'+id).is(':checked')  ? 1 : 0;
    var back_letter    = $('#type_back_side_letter_'+id).is(':checked')   ? 1 : 0;
                
    var active = $('#active_'+id).removeClass().attr('value');
    
    //Update Status
    //Sent Notification email
    $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/admin/ajax_change_letter", 
        {
            price: price,
            description: description,
            front_postcard: front_postcard,
            back_postcard: back_postcard,
            active: active,
            id:id
        },
        function(data) {            
            var ret_val  = jQuery.parseJSON( data );
            if( ret_val['status'] == 'success'){
                $('#div_order_added_'+id).removeClass().addClass('alert alert-success');
                $('#div_order_added_'+id +' span').text('Letter has been updated');
                
                $('#td_description_'+id).text( description );
                $('#td_price_'+id).text( price );
                
                var text = $('#td_type_'+id);
                text.text('');
 
                if( front_postcard == 1){
                    text.text(text.text()+'Postcard front');
                }
                if( back_postcard == 1){
                    text.text(text.text()+'Postcard back');
                }
                
                 
                $('#td_active_'+id).text( active == 1 ?'Active':'Disabled' );                
                $('#row_'+id).removeClass().addClass( active == 1 ? 'info':'warning');
                
                $(obj).next().text('Close');
            } else {
                $('#div_order_added_'+id).removeClass().addClass('alert alert-error');
                $('#div_order_added_'+id +' span').text('Update failed, please check fields');
                var fields = ret_val['fields'] 
                for( key in fields ){
                    $('#'+key+'_'+id).parent().parent().addClass('error');
                }
            }
            
    });    
}
function template( id, type ){
    $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/email/ajax_template", 
        { 
            id: id,
            type: type
        },
        //Fill in Person form with data
        function(data) {
            var ret_val  = jQuery.parseJSON( data );                                   
            if( ret_val['status'] == 'success'){
                if( type == 1 ){
                    $('#a_popover_toggle_'+id).popover('toggle');
                    $('#row_template_'+id).remove();                    
                } else {
                    $('#div_status_'+id).removeClass().addClass('alert alert-success').find('span').text('Template emails have been sent');
                }
            } else {
                if( type == 1 ){
                    $('#div_status_'+id).removeClass().addClass('alert alert-error').find('span').text('Template cannot be deleted at this time');
                } else {
                    $('#div_status_'+id).removeClass().addClass('alert alert-success').find('span').text('Template email havent been sent');
                }                    
            }
            
        });
}
function draw_chart(){
    $('#div_status').hide();
    var start_date = $('#start_date').attr('value');
    var end_date = $('#end_date').attr('value');
    
    if( start_date && end_date ){
        $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/reports/ajax_get_stats", 
            { 
                start_date: start_date,
                end_date: end_date
            },
             
            function(data) {
                var ret_val  = jQuery.parseJSON( data );     
                if( ret_val['status'] == 'success'){
                    
                    $('.charts').show();
                    
                    if(ret_val['grand_total_funding']) {
                        $('#grand_total_funding').html(ret_val['grand_total_funding']);
                    }
                    
                    //reset start date
                    $('#start_date').attr('value', ret_val['start_date']);
                    $('#end_date').attr('value', ret_val['end_date']);
                    
                    var data;
                    var options;
                    var chart;

                    for( key in ret_val['charts']){
                        var chart_data = ret_val['charts'][key];
                        
                        // Create the data table.
                        data = new google.visualization.DataTable();
                        data.addColumn('string', 'Topping');
                        data.addColumn('number', 'Slices');
                        
                        for( row in chart_data['rows']) {
                            data.addRow([ row, chart_data['rows'][row] ]);
                        }
                        options = {
                            'title': chart_data['title'],
                            'width': 500,
                            'height': 300
                        };
                        
                        if( document.getElementById( chart_data['id']) ){
                            chart = new google.visualization.PieChart(document.getElementById( chart_data['id']));
                            chart.draw(data, options);                         
                        }
                                                          
                    }
                    
                    
                } else {
                    $('.charts').hide();
                    $('#div_status').show();
                }
                
            }
        );       
    }
    
}

function  change_name(obj){
    $(obj).prev().attr('value', $(obj).find('[value="'+$(obj).attr('value')+'"]').text());
    $(obj).next().attr('value', $(obj).attr('value'));        
}
function  add_visit(id){    
    var visit_date = $('#new_visit_'+id).attr('value');
    $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/visitors/ajax_add_visit", 
        { 
            visit_date: visit_date,
            id: id,
        },
        function(data) {
            var ret_val  = jQuery.parseJSON( data );                                   
            if( ret_val == 'success'){
                $('#dates_'+id).text(visit_date);
                $('#div_status_visits_'+id).removeClass('hidden').addClass('alert alert-success');
                $('#div_status_visits_'+id).text('Visit date added');
                $('#dates_'+id+'_table tbody').append('<tr><td>'+visit_date+'</td></tr>');
                                                
            } else {
                $('#div_status_visits_'+id).removeClass('hidden').addClass('alert alert-error');
                $('#div_status_visits_'+id).text('Visit date not added');   
            }
        }
    );
    return false;    
}
function  add_attende(id){    
    var visit_date = $('#new_visit_'+id).attr('value');
    $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/members/ajax_add_visit", 
        { 
            visit_date: visit_date,
            id: id,
        },
        function(data) {
            var ret_val  = jQuery.parseJSON( data );                                   
            if( ret_val == 'success'){
                $('#div_status_visits_'+id).removeClass('hidden').addClass('alert alert-success');
                $('#div_status_visits_'+id).text('Attended date added');   
                                                
            } else {
                $('#div_status_visits_'+id).removeClass('hidden').addClass('alert alert-error');
                $('#div_status_visits_'+id).text('Attended date not added');   
            }
        }
    );
    return false;    
}
function hide_visit_log(id){
    $('#dates_'+id).popover('toggle');
 
    $.post(
        String(window.location).match(/(.*)index.php/).pop()+'index.php/visitors/get_visitor_dates_popup/', 
        {id:id},
        function(data){
            var parent = $('#dates_'+id).parent();
            $('#dates_'+id).remove();                
            parent.append(data);                 
            $('#dates_'+id).click(
                function(){
                    $(this).popover('toggle');        
                }
            );  
        }
    );    
}
function hide_member_log(id){
    $('#dates_'+id).popover('toggle');   
}
function create_table(obj){
    
    $(obj).dataTable( {
                        "bProcessing": true,
                        "iDisplayLength":15,
                        "bLengthChange": false,
                        "sPaginationType": "two_button",
                        "oLanguage": {
                            "sInfo": "_TOTAL_ visits (_START_ to _END_)",
                            "sSearch": "Search:",
                            "sLengthMenu": "_MENU_",
                            "oPaginate": {
                                 "sNext": "Next",
                                 "sPrevious": "Prev",
                            }                         
                        },
                                                              
   });
   
;
}
function add_type( id, name, type ){
    var lang_select =  $('[data-type="'+type+'"]');
    lang_select.children().removeAttr('selected');
    lang_select.append('<option selected="selected" value="'+id+'">'+name+'</option>');                 
}

function add_doneby_type( id, name, item_id ){
    var lang_select =  $('#'+id);
    lang_select.children().removeAttr('selected');
    lang_select.append('<option selected="selected" value="'+item_id+'">'+name+'</option>');                 
}

function add_visitor( id, name, type ){
 
    var control_id = type == 'done_by' ? '#new_assignment_doneby_person_id': '#select_referer';
    var visitor_select =  $(control_id);
    visitor_select.children().removeAttr('selected');
    visitor_select.append('<option selected="selected" value="'+id+'">'+name+'</option>');
    $('control_id').attr('value', name );
}
function session_user( id,status){
    $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/paper/ajax_recipient", 
        {
            id: id,
            status: status,
        },
        function(data) {                      
    });    
}
function get_server_url(additional){
    return String(window.location).match(/(.*)index.php/).pop()+additional;
}
function assignment_hover(){
   
}

function assignment_delete(){
    
}
function add_assignments( parent ){
       
}

function append_status_div( parent_id, status, msg ){
    var str_html = 
    '<div id="div_export_alert" class="alert alert-'+status+'">'+
            '<a class="close" data-dismiss="alert">�</a>'+
            '<span>'+msg+'</span>'+
    '</div>';
    $(parent_id).append(str_html);
}

function delete_contacts(object){
    if(confirm('Are you sure that you want to delete selected contacts?')){
        
    } else {
        return false;
    }
}

function delete_contact(){
    if(confirm('Are you sure that you want to delete current contact?')){
        $('#form_new_visitor').find('[name="save"]').remove();
        $('#form_new_visitor').append('<input type="hidden" name="delete" value="delete"/>');
        $('#form_new_visitor').submit();    
    } else {
        return false;
    }    
}

function add_new_type(object){
    if($(object).attr('value') == 'add_new'){
        var type = $(object).attr('data-type');
        var id   = $(object).attr('id');
        $.ajax({
            method: "GET",
            url: "visitors/add_new",
            data: { type: type, id: id }
        })
        .done(function( msg ) {

            $("#load_view").html(msg);
            $("#myModal").modal('show');
            
           //alert( "Data Saved: " + msg );
        });
        //window.open(String(window.location).match(/(.*)index.php/).pop()+"index.php/visitors/add_new/"+type+"/"+id,"Add new", "status=1,height=720,width=475");
    }      
}

/*function add_new_type(object){
    if($(object).attr('value') == 'add_new'){
        var type = $(object).attr('data-type');
        var id   = $(object).attr('id');
        $("#myModal").modal('show');
        // window.open(String(window.location).match(/(.*)index.php/).pop()+"index.php/visitors/add_new/"+type+"/"+id,"Add new", "status=1,height=720,width=475");
    }      
}*/

function cancel_new_contact(){
    if(confirm('Are you sure that you want to cancel, you will lose all your changes?')){
        window.location.assign(String(window.location).match(/(.*)index.php/).pop()+'index.php/visitors/');
    } else {
        return false;
    }
}

function update_feed_table(use_timestamp){
    
    if( $('#check_feed').val() == 'false') {
        return;
    }
    if( $('#check_feed').val() == 'once') {
        $('#check_feed').val('false');    
    }
    
    var start_date = $('#start_date').val();
    var end_date   = $('#end_date').val();
    $.post(String(window.location).match(/(.*)index.php/).pop()+"index.php/dashboard/get_feed", 
        {
            use_timestamp: use_timestamp,
            start_date: start_date,
            end_date: end_date 
        },
        function(data) {
            var ret_val  = jQuery.parseJSON( data );                                   
            if( ret_val['status'] == 'success'){
                var feed_table = $('#feed_body');
                feed_table.children().removeClass('alert alert-danger');
                if(feed_table.find('.dataTables_empty').length > 0){
                    feed_table.find('.dataTables_empty').parent().remove();    
                }
                for( key in ret_val['data']){
                    feed_table.prepend(
                        '<tr class=""><td>'+ret_val['data'][key]['timestamp']+'</td><td>'+ret_val['data'][key]['name']+'</td><td>'+ret_val['data'][key]['type']+'</td><td>'+ret_val['data'][key]['text']+'</td></tr>'
                    );
                }                                      
            } 
        }
    );    
}

function change_note(object){
    $(object).removeAttr('data-clean');
}

function remove_notes(object){
    $('[data-clean="true"]').remove();
}
function afterLoadTable() {
    $('.dropdown-toggle').dropdown();
    $( "#div_visitors .btn" ).click(function() {
        $(this).popover('toggle');
        $('.datepickerpop').datepicker();
        $('.datepickerpop').change(
            function(){ 
                var value = $(this).attr('value');
                //$(this).attr('value', value.substring(5,7)+'-'+value.substring(8,10)+'-'+value.substring(0,4));
                $(this).attr('value', value);

            }
        );
    });

    $( "#div_members .btn" ).click(function() {
        $(this).popover('toggle');
        $('.datepickerpop').datepicker();
        $('.datepickerpop').change(
            function(){
                var value = $(this).attr('value');
                $(this).attr('value', value.substring(5,7)+'-'+value.substring(8,10)+'-'+value.substring(0,4));

            }
        );
    });

    $( "#div_givings .btn" ).click(function() {
        $(this).popover('toggle');
        $('.datepickerpop').datepicker();
        $('.datepickerpop').change(
            function(){
                var value = $(this).attr('value');
                $(this).attr('value', value.substring(5,7)+'-'+value.substring(8,10)+'-'+value.substring(0,4));
            }
        );
    });
}

jQuery(document).ready(function($) {
    $(".dashboard_icon").hover(function() {
        $(this).addClass('animated bounceIn');
    }, function() {
        $(this).removeClass('animated bounceIn');
    });    

    $('#qam_datetime').datepicker({
                            // minDate: 1,
                            // onSelect: function(theDate) {
                            //     $("#dataEnd").datepicker('option', 'minDate', new Date(theDate));
                            // },
                            // beforeShow: function() {
                            //     $('#ui-datepicker-div').css('z-index', 9999);
                            // },
                            // dateFormat: 'mm-dd-yy'
                            dateFormat: 'yy-mm-dd'
                        });
    //set show current date
    $('#qam_datetime').datepicker("setDate", new Date());

    var is_member_quick_add_processing = false;
    $("#btn_quick_add_member").click(function(event) {
        $("#member_modal_quick_add").modal('toggle');
        $('#member_modal_quick_add').on('hidden.bs.modal', function (event) {
            event.stopPropagation();
        })
    });

    $("#btn_save_member_quick_add").click(function(){
        if(is_member_quick_add_processing){
            return false;
        }
        _quickAddMember();
    });

    function _utilityResetButton(isprocessing){
        if(isprocessing){
            $("#btn_save_member_quick_add").removeClass('btn-primary').addClass('.btn-default').html('processing...');
            return;
        }
        $("#btn_save_member_quick_add").removeClass('btn-default').addClass('btn-primary').html('Save changes');
        is_member_quick_add_processing = false;
        _resetFields();
    }

    function _isValidEmailAddress(emailAddress) {
         var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
        return pattern.test(emailAddress);
    };

    function _resetFields(){
        $("#qam_address").val('');
        $("#qam_city").val('');
        $("#qam_zip").val('');
        $("#qam_phone_1").val('');
        $("#qam_phone_2").val('');
        $("#qam_phone_3").val('');
        $("#qam_gender").val('');
        $("#qam_email").val('');
        is_member_quick_add_processing = false;
        
        // qam_select_month = $("#qam_select_month").val();
        // qam_select_day = $("#qam_select_day").val();
        // qam_select_year = $("#qam_select_year").val();
    }

    function _quickAddMember(){
        _utilityResetButton(true);
         is_member_quick_add_processing = true;
        qam_datetime = $("#qam_datetime").val();
        qam_first_name = $("#qam_first_name").val();
        if(qam_first_name == ""){
            $("#qam_first_name").addClass('error');
            $("#qam_first_name").attr('placeholder', 'First name cannot be null!');
            _utilityResetButton();
            return;
        }
        $("#qam_first_name").removeClass('error');
        
        qam_last_name = $("#qam_last_name").val();
        if(qam_last_name == ""){
            $("#qam_last_name").addClass('error');
            $("#qam_last_name").attr('placeholder', 'Last name cannot be null!');
            _utilityResetButton();
            return;
        }
        $("#qam_last_name").removeClass('error');

        qam_address = $("#qam_address").val();
        qam_city = $("#qam_city").val();
        qam_state = $("#qam_state").val();
        qam_zip = $("#qam_zip").val();
        qam_phone_1 = $("#qam_phone_1").val();
        qam_phone_2 = $("#qam_phone_2").val();
        qam_phone_3 = $("#qam_phone_3").val();
        qam_gender = $("#qam_gender").val();
        
        qam_email = $("#qam_email").val();
        if(qam_email == "" || !_isValidEmailAddress(qam_email)){
            $("#qam_email").addClass('error');
            $("#qam_email").attr('placeholder', 'Email adress is not valid!');
            _utilityResetButton();
            return;
        }

        qam_head_household = $("#qam_head_household").val();
        qam_active = $("#qam_active").val();
        qam_select_month = $("#qam_select_month").val();
        qam_select_day = $("#qam_select_day").val();
        qam_select_year = $("#qam_select_year").val();

        var isVisitor = false;
        if(String(window.location).match(/index.php\/members/)){
            var url = String(window.location).match(/(.*)index.php/).pop()+'index.php/members/ajax_add_new/';    
        }else{
            isVisitor = true;
            var url = String(window.location).match(/(.*)index.php/).pop()+'index.php/visitors/ajax_add_new/';    
        }
        
        var data = {
            first_name: qam_first_name,
            last_name : qam_last_name,
            email : qam_email,
            phone_1: qam_phone_1,
            phone_2: qam_phone_2,
            phone_3: qam_phone_3,
            month: qam_select_month,
            day: qam_select_day,
            year: qam_select_year,
            gender:qam_gender,
            head_household: qam_head_household,
            active: qam_active,
            zip: qam_zip,
            state: qam_state,
            city: qam_city,
            address : qam_address,
            datetime : qam_datetime
        };

        $.post(url, data, function(data, textStatus, xhr) {
            is_member_quick_add_processing = false;
            _utilityResetButton();

            /*optional stuff to do after success */
            if(data.err == 0){
                if(isVisitor){
                    toastr.success('Create visitor is successful!');
                    $('#div_visitors table').dataTable()._fnAjaxUpdate();
                }else{
                    toastr.success('Create member is successful!');
                    $('#div_members table').dataTable()._fnAjaxUpdate();
                }
            }else{
                toastr.error('Error happens, please try again!');
            }
            $("#member_modal_quick_add").modal('hide');
        }, 'json');
        
         
    }
});


$(document).ready(function () {
    $(document).on('submit','form.well',function(e){
        e.preventDefault();
        var controlid = $('form.well input[name="control_id"]').val();
        
        $.ajax({
            url : $(this).attr('action'),
            type: "POST",
            data: $(this).serialize(),
            success: function (data) {
                
                refresh_item_select(controlid);
                $("#load_view").html(data);
                //$("#myModal").modal('show');
                //$("#form_output").html(data);
            },
            error: function (jXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    });

   /* $('input[name="btn_cancel"]').click(function() {
       $('#mymodel').modal('hide');
    })*/
});