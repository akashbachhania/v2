<?php
$from ='';
$to = '';
$subject = '';

switch( $status ){
    case Email::STATUS_SENT:
        $msg = "Emails have been sent!";
        $class = 'alert alert-success';
    break;    
    case Email::STATUS_SAVED:
        $msg = "New template has been created!";
        $class = 'alert alert-success';
    break;
    case Email::STATUS_UPDATE:
        $msg = "Template has been updated";
        $class = 'alert alert-info'; 
    break;
    case Email::STATUS_ERROR:
        $msg = "Error while saving template, please fill in all fields";
        $class = 'alert alert-error'; 
    break;
    case Email::STATUS_NOTSENT:
        $msg = "One or more mails couldnt be sent at this time, please contact server administrator";
        $class = 'alert alert-error'; 
    break;    
    default:
        $msg = '';
        $class = '';
}

$mail_body = '';
$mail_from = '';
$mail_to   = '';
$mail_from = '';
$mail_subject = '';
 
foreach( $template->errors as $error=>$value){
    $$error = 'error';
}

if( !isset($show_event)){
    $show_event = false;
}
?>
<style type="text/css">
    .dropdown-menu {
    background-color: #ffffff!important;
}
</style>
<div id="div_email" class="widgets_area first">
  <div class="row-fluid">
      <div class="span8">
          <div class="well red">
              <div class="well-header">
                  <h5>Send email</h5>
              </div>

              <div class="well-content no-search">
                  <?php if( $status ){ ?>
                    <div id="div_order_added" class="<?php echo $class ?>">
                            <a class="close" data-dismiss="alert">x</a>
                            <?php echo $msg ?>
                    </div>
                    <?php } ?> 
                    <form class="form-horizontal" id="form_visitors" action="<?php echo site_url('email/process')?>" method="post">
                    
                    <div class="form_row">
                      <label class="field_name align_right">From:</label>
                      <div class="field">
                <input id="from" type="text" class="span6 <?php echo $mail_from ?>" name="mail_from" value="<?php echo $template->get_mail_from() ?>"/>
                      </div>
                    </div>
                    <div class="form_row">
                      <label class="field_name align_right">To:</label>
                      <div class="field">
                <input id="to" type="text" placeholder="Non visitor email addresses go here.." class="span6 <?php echo $mail_to ?>" name="mail_to[]" value="<?php echo $template->get_anonymous_recipients_string() ?>"/>                
                      </div>
                    </div>
                    <div class="form_row">
                      <label class="field_name align_right"></label>
                      <div class="field">
                <select id="select_recipients" name="mail_to[]" multiple="multiple" size="5">
                <?php foreach ( $visitors as $visitor ) {?>
                    <option value="<?php echo $visitor->id?>" <?php echo $template->is_recipient($visitor->id) ? ' selected="selected"' : ''?> > <?php echo $visitor->first_name .''.$visitor->last_name?></option>';
                <?php } ?>       
                </select>
                <a class="btn" id="select_all" data-state="select">Select all</a>              
                      </div>
                    </div>
                    <div class="form_row">
                      <label class="field_name align_right">Subject:</label>
                      <div class="field">
                        <input id="subject" type="text" class="span5 <?php echo $mail_subject ?>" name="mail_subject" value="<?php echo $template->mail_subject ?>"/>
                      </div>
                    </div>
                    <?php if($show_event) {?>
                    <div class="form_row">
                        <label class="field_name align_right">Event:</label>
                        <div class="field">
                            <label data-placement="left" rel="tooltip"  data-original-title="This email template will be automatically sent on the creation of new user" class="radio">
                                <input type="radio" name="event" <?php echo $template->event==Template::EVENT_CREATION ? ' checked="checked"':'' ?> value="<?php echo Template::EVENT_CREATION ?>">On Creation
                            </label>
                        </div>
                    </div>
                    <div class="form_row">
                        <div class="field">
                            <label data-placement="left" rel="tooltip"  data-original-title="This email template will be automaticaly sent for the birday of the provided recipients, or if none provided, for the birthday of any visitor" class="radio">
                                <input type="radio" name="event" <?php echo $template->event==Template::EVENT_BIRTHDAY ? ' checked="checked"':'' ?> value="<?php echo Template::EVENT_BIRTHDAY ?>">On Birthday
                            </label>
                        </div>
                    </div>            
                 <?php }?>


                    <div class="form_row">
                      <label class="field_name align_right">Body:</label>
                      <div class="field">
                        <!-- <textarea <?php /* if(!$makeReadOnly) {?>id="body"<?php }?> name="mail_body" style="width: 98%" class="<?php echo $mail_body ?>" cols="10" rows="20" <?php echo $makeReadOnly;?>><?php echo (isset($message) and $message->htmlBody !='') ? $message->htmlBody : ''/*/?></textarea> -->
                        <textarea class="textarea" name="mail_body" style="width: 100%; height: 300px"><?php echo (isset($message) and $message->htmlBody !='') ? $message->htmlBody : ''?></textarea>
                        <br><br>
                        <input class="btn blue" type="submit" value="Send" name="action_send"/>
                        <input class="btn blue" type="submit" value="Save" name="action_save"/>
                        <input class="btn blue" type="submit" value="Cancel" name="action_cancel"/>
                        <?php if( $template->id ){ ?>
                            <input class="btn blue" type="submit" value="Delete" name="action_delete"/>        
                        <?php }?> 
                      </div>
                    </div>
                    <input type="hidden" value="<?php echo $template->id?>" name="id"/>
                  </form>
              </div>
              
          </div>
      </div>
      <div id="div_templates" class="span3 well-content no-search">
        
        <?php
        if($user_info->vr_username !='' and $user_info->vr_password !='') {
             ?>
        <a href="<?php echo site_url('email/icontact')?>" style="margin-bottom: 20px;"><img src="<?php echo str_replace('index.php','',site_url()).'application/views/assets/img/'?>verticalresponse-logo_0.png"/></a>   
              <?php
         }
        
        
        $this->html_table->set_heading(array('<div id="div_templates_title">Templates</div>'));
        
         
        $this->html_table->add_row( 
            array(

                '<select class="span4">
                <option>New</option>
                    <option><a href="'.site_url('email').'">New email template</a></option>
                    <option><a href="'.site_url('email/scheduled').'">New scheduled template</a></option>
                </select>

                ') 
        ); 
         
        if( $templates ){
            foreach( $templates as $template ){
                $this->html_table->add_row( 
                    array(
                        get_popover_data($template, $template_id)
                    ),
                    array(
                        'id' => 'row_template_'.$template->id
                    )
                 );
            }            
        } else {
            $this->html_table->add_row( array('<div class="templates" >No saved templates.</div>'));    
        }

        echo $this->html_table->generate();
        
        ?>
        
        
    </div>
  </div>
</div>



<?php
function get_popover_data( Template $template, $template_id ){
 
    $data = '<div>'.
                '<div id="div_status_'.$template->id.'" class="hidden">'.
                    '<a class="close" data-dismiss="alert">x</a>'.
                    '<span></span>'.
                '</div>'.
                '<a href="'.site_url('email/index/'.$template->id).'" class="btn blue" onclick="window.location($(this).attr(\"href\"))">Open</a>'.
                '<button type="button" class="btn blue" onclick="template('.$template->id.',1)" >Delete</button>'.
                '<button type="button" class="btn blue" onclick="template('.$template->id.',2)" >Send</button>'.
            '</div>';
            
    
    //$div_template = '<div  data-content=\''.$data.'\' data-html="" data-trigger=\'click\' class="templates" ><a '.($template_id == $template->id ? 'class="text-info"':'').'href="'.site_url('email/index/'.$template->id).'">'.$template->mail_subject.'</a></div>';
    $div_template = '<div  class="templates" id="div_template_'.$template->id.'"><a   id="a_popover_toggle_'.$template->id.'" data-content=\''.$data.'\' data-html="" data-trigger=\'click\' '.($template_id == $template->id ? ' ':'').'> '.( strlen($template->mail_subject)<=15 ? substr($template->mail_subject,0,15): substr($template->mail_subject,0,15).'...' ).'</a></div>';
     
    return $div_template;   
}
?>