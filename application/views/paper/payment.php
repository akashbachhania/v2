<?php
 
$front_side = null;
$back_side  = null;
foreach( $postcard->_letter as $letter ){
    if( $letter->id == $postcard->front_letter_id && $front_side == null){
        $front_side = $letter;
    } else {
        $back_side = $letter;
    }
}
$recipients = array();

$visitors = is_array($postcard->_visitor) ? $postcard->_visitor : array($postcard->_visitor);
foreach( $visitors as $visitor ){
    $recipients[] = $visitor->first_name .' '.$visitor->last_name;
}
$count = count($recipients);
$recipients = implode(', ', $recipients );

 
$total_front = $count * $front_side->price;
$total_back  = $count * $back_side->price;
$grand_total = '$'.($total_front+$total_back);

/**
* @var Order
*/
$postcard;
$this->html_table->add_row( array(
    '<img  id="a_change_front" data-placement="right" rel="tooltip"  data-original-title="Click on image to change" class="img-polaroid" src="'.$path.'/'.$front_side->path_thumb.'" height="150" width="100" border="1"/>',        
    'Front side',        
    $front_side->description,
    '$'.$front_side->price,         
    $count,        
    '$'.$total_front
    ),
    array(
        //'class' => ($postcard->status == Order::STATUS_PENDING ? 'warning' : 'success'),
        'id' => 'row_front' 
    )       
);
$this->html_table->add_row( array(
    '<img  id="a_change_back" data-placement="right" rel="tooltip"  data-original-title="Click on image to change" class="img-polaroid" src="'.$path.'/'.$back_side->path_thumb.'" height="150" width="100" border="1"/>',
    'Back side',        
    $back_side->description,                           
    '$'.$back_side->price,
     $count,
    '$'.$total_back
    ),
    array(
        //'class' => ($postcard->status == Order::STATUS_PENDING ? 'warning' : 'success'),
        'id' => 'row_back'
    )       
);
$this->html_table->add_row( array(
        array('data'=> '', 'attributes'=>array('colspan' => '4')  ),            
        'Total:',            
        '<label class="td amount">'.$grand_total.'</label> '            
    ),
    array(
        //'class' => ($postcard->status == Order::STATUS_PENDING ? 'warning' : 'success'),
        'id' => 'row_total',
        
    )       
);
/*
$this->html_table->add_row( array(
    $recipients,        
    $count.' recipients',
    '$'.$grand_total
    ),
    array(
        //'class' => ($postcard->status == Order::STATUS_PENDING ? 'warning' : 'success'),
        'id' => 'row_' 
    )       
);*/
$this->html_table->set_heading(array('Cover','Side','Description','Unit price','#','Total'));
$table = $this->html_table->generate();
$this->html_table->clear();

 
$front_images = '';
$front_letters = !is_array($front_letters)? array($front_letters):$front_letters;
foreach( $front_letters as $letter ){        
   $front_images .= '<a  href="'.$path.'/'.$letter->path_full.'"><img id="'.$letter->id.'" src="'.$path.'/'.$letter->path_thumb.'" data-title="My title" data-description="'.$letter->description.'" data-side="front"></a>';
}

$back_images = '';
$back_letters = !is_array($back_letters)? array($back_letters):$back_letters;   
foreach( $back_letters as $letter ){        
   $back_images .= '<a  href="'.$path.'/'.$letter->path_full.'"><img id="'.$letter->id.'" src="'.$path.'/'.$letter->path_thumb.'" data-title="My title" data-description="'.$letter->description.'" data-side="back"></a>';
}



$this->html_table->set_heading(array('','','','','','')); 
$addresses = array();
foreach( $all_visitors as $index=>$visitor){
    if( isset($selected_visitors[$visitor->id])){
       $_visitor = array();
       
       $comps = array();
       $comp = array('city','state','zip');
       foreach ($comp as $value) {
            if($visitor->$value){
                $comps[] = $visitor->$value;
            }
       }
       $comps_str = ($comps_str = implode(',', $comps)) ? $comps_str .'</br>':'';
       
       $phone_str = '';
       if( $visitor->phone_1 ){
           $phone_str = "<abbr title=\"Phone\">P:</abbr>$visitor->phone_1</br>";
       }
       
       $address =
             
            "<span class='' id='span_$visitor->id'>
             <address>
              <strong>$visitor->first_name $visitor->last_name</strong></br>
              ".($visitor->address ? $visitor->address .'</br>': '')."
              $comps_str
              $phone_str 
            </address>
           </span> 
            ";                  
       $addresses[] = $address;
    }
}
/*$row = array();
foreach( $addresses as $address){
    if( count($row) <=4){
        $row[] = $address;
    } else {
        $this->html_table->add_row( $row );
        $row = array();
        $row[] = $address;
    }
}
if( $row ){
    $this->html_table->add_row( $row );
} */

//$table_address = $this->html_table->generate(); 
$table_address = '<div id="div_addresses">'.implode('',$addresses).'</div>';

switch( $status ){
    case Paper::O_CCFAIL:
        $class  = 'alert-error';
        $message = '<strong>Error:</strong>'.$message;
    break;   
}
?>
<div id="div_payment" class="row-fluid">
    <div id="div_payment_inner" class="span12">            
            
            <div class="well red">
                <div class="well-header">
                    <?php if( isset($status) && $status !==null ){ ?>
                        <div id="div_payment_status" class="alert <?php echo $class?>">
                                <a class="close" data-dismiss="alert"></a>
                                <?php echo $message ?>
                        </div>
                    <?php } ?>
                    <h5>Order details:</h5>        
                </div>
                <div class="well-content no-search table-responsive">

                    <?php
                        echo $table;
                        
                        echo '<div id="div_recipients" class="form-inline">';
                            echo '<span><strong>Recipients:</strong></span>';
                            echo '<select id="select_recipients" name="recipients[]" multiple="multiple" size="5">';
                            foreach ( $all_visitors as $visitor ) {
                                echo '<option value="'.$visitor->id.'" ' .(isset($selected_visitors[$visitor->id]) ? ' selected="selected"' : '') . ' >' . $visitor->first_name .''.$visitor->last_name. '</option>';
                            }       
                            echo '</select>';
                            echo '<a id="a_show_addr" class="btn red" style="margin-left:5px;">Show Adresses</a>';
                        echo '</div>';
                        echo $table_address;
                    ?>        

                </div>
            </div>      
                            
    </div>
</div>
<div class="row-fluid">
    <div class="offset2 span8" id="div_id_charge" name="controls">
        <div class="well red">
                <div class="well-header">
                    <h5><i name="icon_collapse">&nbsp;&nbsp;</i>&nbsp;&nbsp;Finalize order</h5>
                </div>
                <div class="well-content no-search table-responsive">
                    <fieldset style="height: 280px;">
                         <table id="table_charge" class="">
                            <tbody><tr>
                                <td>
                                     <div><img src="http://homeinspectorplus.com/live/application/views/assets/img/cc_cirius.png" class="cc_image"></div> 
                                     <div><img src="http://homeinspectorplus.com/live/application/views/assets/img/cc_master.png" class="cc_image"></div> 
                                     <div><img src="http://homeinspectorplus.com/live/application/views/assets/img/cc_amex.png"   class="cc_image"></div> 
                                     <div><img src="http://homeinspectorplus.com/live/application/views/assets/img/cc_visa.png"   class="cc_image"></div> 
                                </td>
                                <td>
                                    <img src="http://homeinspectorplus.com/live/application/views/assets/img/credit-cards-icon.png" class="cc_image"><br>
                                </td>
                                <td>
                                    <div name="controls cc_image">
                                    <div class="control-group">
                                        <div class="controls">
                                        </div>
                                        <div class="control-group">
                                            <div class="controls">
                                                    <label class="inline_label amount_label">Order Total: </label><label class="inline_label amount"><?php echo $grand_total?></label>
                                            </div>
                                        </div>
                                    </div>                                        
                                    <a id="btn_charge"   class="btn blue class=">Pay order with your CC</a> 
                                    <a id="btn_cancel"   class="btn light class=">Cancel order</a>  
                                </div></td>
                            </tr>
                         </tbody></table>
                      </fieldset>
                </div>
         </div>                         
    </div>
</div>
<style type="text/css">
  #charge_modal,#letter_back_modal,#letter_front_modal
  {
    position: fixed;

  }
  .modal.fade.in {
    top: 10% !important;
}
</style>
<!-- Modal -->
<div id="charge_modal"  class="modal fade"  tabindex="-1" role="dialog" aria-labelledby="memberquickadd" style="display:none; overflow:scroll; margin-top:30px;margin-bottom:90px;" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="memberquickadd">Charge Credit Card</h4>
      </div>
        <div class="modal-body">
          <form class="form-horizontal" id="form_cc" action="<?php echo site_url('paper/finalize')?>" method="post">            

          <div class="control-group">
            <div id="div_order_charged">
                  <a class="close" data-dismiss="alert"></a>
                  <span></span>
            </div>
              <label class="control-label" for="subtotal">Charge Amount:</label>
              <div class="controls">                                         
                  <div class="input-prepend">
                      <span class="add-on">$</span>
                      <input id="amount" disabled="disabled" class="input-small" type="text" value="<?php echo substr($grand_total, 1, strlen($grand_total))?>" name="amount">
                      <span  name="span_help"     class="help-inline hidden"></span>
                  </div>
              </div>                                  
          </div>
          <div class="control-group">
              <label class="control-label" for="subtotal">CC Number:</label>                         
              <div class="controls">                           
                   <input id="cc_num" class="input-small form-control" type="text" value="" name="cc_num">
                   <span  name="span_help"     class="help-inline hidden"></span>                                           
              </div>
          </div>                                
          <div class="control-group">
              <label class="control-label" for="subtotal">Expire Date:</label>                          
              <div class="controls">                           
                   <input id="cc_date" class="input-small" type="text" value="" name="cc_date">
                   <span  name="span_help"     class="help-inline hidden"></span>                                          
              </div>
          </div>
          <div class="control-group">
              <label class="control-label" for="subtotal">Security code:</label>                          
              <div class="controls">                           
                   <input id="cc_cvn" class="input-small" type="text" value="" name="cc_cvn">
                   <span  name="span_help"     class="help-inline hidden"></span>                                          
              </div>
          </div>                                
          <div class="control-group">
              <label class="control-label" for="subtotal">Billing address:</label>                          
              <div class="controls">                           
                   <select id="select_fill_in">
                      <option value="1">No billing address</option>
                      <option value="2">Copy from user details</option>
                      <option value="3">Copy from company details</option>
                      <option value="5">Manual fill in</option>
                   </select>
                   <span  name="span_help"     class="help-inline hidden"></span>                                          
              </div>
          </div>                                 
          <div class="control-group fill_in">
              <label class="control-label" for="subtotal">First Name</label>                          
              <div class="controls">                           
                   <input id="cc_date" class="input-small" type="text" value="" name="first_name">
                   <span  name="span_help"     class="help-inline hidden"></span>                                          
              </div>
          </div>
          <div class="control-group fill_in">
              <label class="control-label" for="subtotal">Last Name</label>                          
              <div class="controls">                           
                   <input id="cc_date" class="input-small" type="text" value="" name="last_name">
                   <span  name="span_help"     class="help-inline hidden"></span>                                          
              </div>
          </div> 
          <div class="control-group fill_in">
              <label class="control-label" for="subtotal">Address</label>                          
              <div class="controls">                           
                   <input id="cc_date" class="input-small" type="text" value="" name="address">
                   <span  name="span_help"     class="help-inline hidden"></span>                                          
              </div>
          </div> 
          <div class="control-group fill_in">
              <label class="control-label" for="subtotal">City</label>                          
              <div class="controls">                           
                   <input id="cc_date" class="input-small" type="text" value="" name="city">
                   <span  name="span_help"     class="help-inline hidden"></span>                                          
              </div>
          </div>
          <div class="control-group fill_in">
              <label class="control-label" for="subtotal">Zip</label>                          
              <div class="controls">                           
                   <input id="cc_date" class="input-small" type="text" value="" name="zip">
                   <span  name="span_help"     class="help-inline hidden"></span>                                          
              </div>
          </div>
          <div class="control-group fill_in">
              <label class="control-label" for="subtotal">State</label>                          
              <div class="controls">                           
                   <?php echo form_dropdown('state', $us_states, '' )?>
                   <span  name="span_help"     class="help-inline hidden"></span>                                          
              </div>
          </div>
          <div class="control-group fill_in">
              <label class="control-label" for="subtotal">Country</label>                          
              <div class="controls">                           
                   <input id="cc_date" class="input-small" type="text" value="" name="country">
                   <span  name="span_help"     class="help-inline hidden"></span>                                          
              </div>
          </div>
          <input type="hidden" name="order_id"   value="<?php echo $postcard->id?>"/>
          <input type="hidden" name="recipients" value="<?php echo $front_side->id?>"/>
          <input type="hidden" name="front_side" value="<?php echo $back_side->id?>"/>
          <input type="hidden" name="recipients" value="<?php echo $postcard->id?>"/>
          <input type="hidden" name="amount" class="amount" value="<?php echo $grand_total?>"/>                                                                         
                

        </div>
      <div class="modal-footer">
        <a href="#" class="btn red btn-primary" id="btn_charge_modal">Charge</a>
        <a data-dismiss="modal" href="#" class="btn btn-default">Close</a>
      </div>
    </div>
  </div>
</div>
<div>&nbsp;</div>

<!-- Modal -->
<div id="letter_front_modal"  class="modal fade"  tabindex="-1" role="dialog" aria-labelledby="" style="display:none;" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="memberquickadd">Select front cover</h4>
      </div>
        <div class="modal-body">
                  

          <div id="div_gallery_frame">
                <div id="div_galleria_front">
                    <?php echo $front_images ?>             
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <a data-dismiss="modal" href="#" class="btn btn red">Close</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div id="letter_back_modal"  class="modal fade"  tabindex="-1" role="dialog" aria-labelledby="" style="display:none; " >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="memberquickadd">Select back cover</h4>
      </div>
        <div class="modal-body">
          <div id="div_gallery_frame_back">
                <div id="div_galleria_back">
                    <?php echo $back_images ?>             
                </div>
            </div>  
        </div>
      <div class="modal-footer">
        <a data-dismiss="modal" href="#" class="btn red">Close</a>
      </div>
    </div>
  </div>
</div>
