<?php
$action = '';
switch( $order_type ){
    case Order::ORDER_LETTER:
        $front_side_label = 'Envelope';
        $back_side_label  = 'Inside';
    break;
    case Order::ORDER_POSTCARD:
        $front_side_label = 'Front side';
        $back_side_label  = 'Back side';   
    break;
}

foreach ($postcards as $postcard) {
    foreach( $postcard->_letter as $letter ){
        if( $letter->id == $postcard->front_letter_id){
            $front_side = $letter->path_thumb;
        } else {
            $back_side = $letter->path_thumb;
        }
    }
    $recipients = array();
     
    $visitors = is_array($postcard->_visitor) ? $postcard->_visitor : array($postcard->_visitor);
    foreach( $visitors as $visitor ){
        $recipients[] = $visitor->first_name .' '.$visitor->last_name;
    }
    $recipients = implode(', ', $recipients );
    /**
    * @var Order
    */
    $postcard;
    $this->html_table->add_row( array(
        '<img  class="img-polaroid" src="'.$path.'/'.$front_side.'" height="150" width="100" border="1"/>',        
        '<img  class="img-polaroid" src="'.$path.'/'.$back_side.'" height="150" width="100" border="1"/>',
        get_popover_data($recipients,'Recipients'),
        get_popover_data($postcard->text, 'Text'),
        $postcard->total ? $postcard->total: '$0.00' ,
        $postcard->timestamp,
        $postcard->status == Order::STATUS_PENDING ? '<strong>Pending!</strong>' : '<strong>Sent!</strong>',
        ),
        array(
            'class' => ($postcard->status == Order::STATUS_PENDING ? 'warning' : 'success'),
            'id' => 'row_'.$postcard->id 
        )       
    );
}
$this->html_table->set_heading(array($front_side_label, $back_side_label,'Recipients','Text','Total', 'Created',''));
$table = $this->html_table->generate();

function get_popover_data( $field, $title ){     
    return '<span data-content="'.$field.'" rel="popover" data-original-title="'.$title.'">'.substr($field,0,100).(strlen($field)>100?'...':'').'</span>';
}  
switch( $status ){
    case Paper::O_SAVED:
        $status = 'Your CC has been charged and your order has been saved';
        $class  = 'alert-success';
    break;
    case Paper::O_UPDATED:
        $status = 'Order has been updated';
        $class  = 'alert-info';
    break;    
}
?>
<div id="div_postcards" class="row">

    <div id="div_postcards_inner" class="col-md-12">
    <?php if( isset($status) && $status ){ ?>
        <div id="div_order_added" class="alert <?php echo $class?>">
                <a class="close" data-dismiss="alert">�</a>
                <strong><?php echo $status ?></strong>
        </div>
    <?php } ?>    
        <form action="<?php echo $action ?>" method="post">
            <?php echo $table ?>  
        </form>
    </div>
</div>