<?php
  
if( $step == 1){
    switch( $order_type ){
        case Order::ORDER_LETTER:
            $label = 'Choose the envelope of your letter';
            $legend = 'Select letter to mail';
        break;
        case Order::ORDER_POSTCARD:
            $label = 'Choose the front of your postcard';
            $legend = 'Select postcard to mail';
        break;
    }
    $var   = 'front_side';
    $action = site_url('paper/new_order_step_2'. ($postcard->id ? "/$postcard->id" :''));
    $field = 'front_letter_id';
} else {
    switch( $order_type ){
        case Order::ORDER_LETTER:
            $label = 'Choose the inside of your';
            $legend = 'Select letter to mail';
        break;
        case Order::ORDER_POSTCARD:
            $label = 'Choose the back side of your postcard';
            $legend = 'Select postcard to mail';
        break;
    }
    $var    = 'back_side';
    $action = site_url('paper/final_step' . ($postcard->id ? "/$postcard->id" :''));
    $field = 'back_letter_id'; 
}

 
$images = '';   
foreach( $letters as $letter ){        
   $images .= '<a  href="'.$path.'/'.$letter->path_full.'"><img id="'.$letter->id.'" src="'.$path.'/'.$letter->path_thumb.'" data-title="My title" data-description="'.$letter->description.'"></a>';
}
   
 
?>
<div id="div_print" class="row">
    <div id="div_print_inner" class="span12">
        <?php if( isset($status) && $status ){ ?>
            <div id="div_order_added" class="alert alert-error">
                    <a class="close" data-dismiss="alert">�</a>
                    <strong>Oops! </strong><?php echo $status ?>
            </div>
        <?php } ?>     
        <legend><?php echo $legend?></legend>     
        <form action="<?php echo $action ?>" method="post">
          
            <?php
            echo '<div id="div_recipients">';
                echo '<select id="select_recipients" name="recipients[]" multiple="multiple" size="5">';                
                foreach ( $visitors as $visitor ) {
                    echo '<option value="'.$visitor->id.'" ' .(isset($selected_visitors[$visitor->id]) ? ' selected="selected"' : '') . ' >' . $visitor->first_name .''.$visitor->last_name. '</option>';
                }       
                echo '</select>';
                echo '<a id="select_all" class="btn" data-state="select">Select all</a>';
            echo '</div>';
            echo '<div id="div_covers">';
                echo '<legend class="center">'.$label.'</legend>'; 
                echo '<div id="div_gallery_frame">';
                    echo '<div id="div_galleria">';
                        echo $images;
                    echo '</div>';
                echo '</div>';
            if( $step == 2 ){
                echo '<div class="hero-unit">';
                echo '<legend class="center">Enter your message here</legend>';
                echo '<textarea name="text" class="text_centered">'.($postcard->text ? $postcard->text : '').'</textarea>';
                echo '<input type="hidden"  id="back_side" name="back_side" value=""/>';
                echo '</div>';
            }
            echo '</div>';
            ?>
            <div>
                <input class="btn btn-info" type="submit" id="btn_next"   name="next"   value="Next"/>
                <input class="btn btn-info" type="submit" id="btn_cancel" name="cancel" value="Cancel"/>
                <input type="hidden" id="front_side" name="front_side" value=""/>
            </div>   
        </form>
        
    </div>
</div>
