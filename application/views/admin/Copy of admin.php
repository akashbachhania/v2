<?php                       
if( $saved !== null){
    if( $saved === true ){
        $saved = 'alert alert-success';
        $msg   = '<strong>Success: </strong>Your order has been saved';                    
    } else if ( $saved === false ) {
        $saved = 'alert alert-error';
        $msg   = '<strong>Error: </strong>Your order has not been saved (Please correct fields with red border)';            
    } else if ( $saved === 'update') {
        $saved = 'alert alert-info';
        $msg   = '<strong>Success: </strong>Your order has been updated successfully!';             
    }
} else {
    $saved = '';                         
    $msg  = '';
}

foreach( $companies as $company ){
    $select_array[ $company->id ] = $company->name;
}
$select_array[0] =  '--Select Company--';

$person_fields = array(
'name','address','city','state','zip','password','active'
);
?>
            <div class="row div_header">
                <div id="div_companies_table" class="span5 well">
                    <form id="form_companies" class="form-horizontal" action="<?php echo site_url('admin')?>" method="post" >
                        <fieldset>
                            <legend>Companies</legend>
                            <div class="control-group">
                                <label class="control-label" for="input01">Company list</label>
                                <div class="controls">
                                    <?php echo form_dropdown('company_id', $select_array, $selected_company ? $selected_company->id : 0, ' id="company_select"' )?>
                                    <p class="help-block">Select company</p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="input01"></label>
                                <div class="controls">
                                    <a href="<?php echo site_url('admin/add_new/Company')?>" target="_blank" class="btn">Add new</a>  
                                    <p class="help-block">Add new company</p>
                                </div>
                            </div>                            
                        </fieldset>                                                                    
                    </form>                    
                    
                </div>
                <div id="div_company_users" class="span6">
                    <?php if( $saved ){ ?>
                        <div id="div_order_added" class="<?php echo $saved ?>">
                                <a class="close" data-dismiss="alert">�</a>
                                <?php echo $msg ?>
                        </div>
                    <?php } ?>                            
                    <?php 
                                             
                        if( $selected_company ){
                            $fields = array_diff( array_keys(get_object_vars( $selected_company )), array( 'table','datetime', 'errors', 'user_id', 'required','order_by') );
                            echo $this->order_helper->get_object_form( $selected_company, $fields, $this->table, 
                                array(                            
                                    'active' => array(
                                        'element' => Order_helper::ELEMENT_DROPDOWN,
                                        'options' => $statuses,
                                        'selected' => $selected_company->active
                                        ),
                                     'state'=> array('element'=> Order_helper::ELEMENT_DROPDOWN,
                                                          'options'=> $states,
                                                          'selected'=>$selected_company->state ),
                                    'payment_gateway'=>
                                        array(
                                              'element'=> Order_helper::ELEMENT_DROPDOWN,
                                              'options'=> array('1' => 'Uses Authorize.net', '0' => 'No payment gateway'),
                                              'selected'=>$selected_company->payment_gateway                                                    
                                        ),                                                                                                  
                                    '__legend' => $selected_company->name,
                                    '__form'   => array(
                                        'action' => site_url('admin'), 
                                        'submit_value' => 'Save',
                                        'submit_name'  => 'save_company'
                                        )
                                    )
                             );
                        ?>
                        <div class="well">
                            <frameset>
                                <legend>Users</legend>
                                <?php                                  
                                    if( $users ){
                                        $coordinators = array();
                                        foreach( $users as $index=>&$user ){                                    
                                            if( $user->person_type == Person::TYPE_USER ){
                                                $coordinators[] = $user;
                                            }
                                        }
                                          
                                        echo $this->order_helper->generate_table( $coordinators, $person_fields, $this->table,
                                            array(
                                                '__href' => array(
                                                    'href' => site_url('admin/add_new/Person/<id>/show_user'),
                                                    'id' => '<id>',
                                                    'name' => 'person',
                                                )
                                            )                                        
                                        
                                         );                            
                                    }
                                                                 
                                ?>
                            </frameset>
                            <a class="btn" target="__blank" href="<?php echo site_url('admin/add_new/Person/'.$selected_company->id )?>">New user</a>
                        </div> 
                        <?php }?>
                </div> 
            </div>