<?php
$action = '';
$statusses = array(
    Order::STATUS_PENDING =>  'Pending',
    Order::STATUS_SENT =>   'Sent'
);
$front_side_label = '';
$back_side_label = '';
foreach ($postcards as $postcard) {
    $front_side = '';
    $back_side = '';
    
    if( !$front_side_label ){
        switch( $postcard->type ){
            case Order::ORDER_LETTER:
                $front_side_label = 'Envelope';
                $back_side_label  = 'Inside';
            break;
            case Order::ORDER_POSTCARD:
                $front_side_label = 'Front side';
                $back_side_label  = 'Back side';   
            break;
        }    
    }

    
    if( isset($postcard->_letter) && is_array($postcard->_letter)){
        foreach( $postcard->_letter as $letter ){
            if( $letter->id == $postcard->front_letter_id){
                $front_side = $letter->path_thumb;
            } else {
                $back_side = $letter->path_thumb;
            } 
        }       
    }

    $recipients = array();
   
    $visitors = is_array($postcard->_visitor) ? $postcard->_visitor : array($postcard->_visitor);
    foreach( $visitors as $visitor ){
        $recipients[] = $visitor->first_name .' '.$visitor->last_name;
    }
    $recipients = implode(', ', $recipients );
    /**
    * @var Order
    */
    $postcard;
    $this->html_table->add_row( array(
        '<img  id="'.$postcard->id.'" src="'.str_replace('index.php','',site_url()).'application/views/assets/img/add-icon.png'.'"  border="1"/>',
        '<img  class="img-polaroid" src="'.$path.'/'.$front_side.'" height="50" width="100" border="1"/>',        
        '<img  class="img-polaroid" src="'.$path.'/'.$back_side.'" height="50" width="100" border="1"/>',
        get_popover_data($recipients,'Postcard recipients'),
        get_popover_data($postcard->text, 'Postcard text'),
        '<strong>'.($postcard->total ? $postcard->total: '$0.00').'</strong>' ,
        $postcard->timestamp,
        form_dropdown('change_status', $statusses, $postcard->status,'id="select_'.$postcard->id.'" class="input-small" onchange="chaord('.$postcard->id.',this)"')
        ),
        array(
            'class' => ($postcard->status == Order::STATUS_PENDING ? 'warning' : 'success'),
            'id' => 'row_'.$postcard->id
        )        
    );
}
$this->html_table->set_heading(array('', $front_side_label,$back_side_label,'Recipients','Text','Total', 'Created','Status'));
$table = $this->html_table->generate();

function get_popover_data( $field, $title ){     
    return '<span data-content="'.$field.'" rel="popover" data-original-title="'.$title.'">'.substr($field,0,100).(strlen($field)>100?'...':'').'</span>';
}
/*  
switch( $status ){
    case Paper::O_SAVED:
        $status = 'Order has been saved';
        $class  = 'alert-success';
    break;
    case Paper::O_UPDATED:
        $status = 'Order has been updated';
        $class  = 'alert-info';
    break;
}*/
?>
<div id="div_postcards" class="row">

    <div id="div_postcards_inner" class="span12">
    <?php if( isset($status) && $status ){ ?>
        <div id="div_order_added" class="alert <?php echo $class?>">
                <a class="close" data-dismiss="alert">�</a>
                <strong><?php echo $status ?></strong>
        </div>
    <?php } ?>    
        <form action="<?php echo $action ?>" method="post">
            <?php echo $table ?>  
        </form>
    </div>
</div>
<?php
    
   
?>