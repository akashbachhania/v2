
      <script src="<?php echo base_url()?>assets/template/js/library/bootstrap-datepicker.js"></script>
    
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/bootstrap.min.js"></script>

        <script type="text/javascript" src="<?php echo base_url() . 'application/views/assets/js/underscore/underscore-min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'application/views/assets/js/calendar.js' ?>"></script>

<script type="text/javascript" src="<?php echo base_url() . 'application/views/assets/js/callender_intial.js' ?>"></script>

        <!-- Proton base scripts: -->
        
        <script src="<?php echo base_url() ?>assets/scripts/main.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/proton/common.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/proton/main-nav.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/proton/user-nav.js"></script>

        <!-- Page-specific scripts: -->
        <script src="<?php echo base_url() ?>assets/scripts/proton/sidebar.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/proton/tables.js"></script>
        <!-- jsTree -->
        <script src="<?php echo base_url() ?>assets/scripts/vendor/jquery.jstree.js"></script>
        <!-- Data Tables -->
        <!-- http://datatables.net/ -->
            <script src="<?php echo base_url() ?>assets/scripts/vendor/jquery.dataTables.min.js"></script>
        <!-- Data Tables for BS3 -->
        <!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
        <!-- NOTE: Original JS file is modified -->
            <script src="<?php echo base_url() ?>assets/scripts/vendor/datatables.js"></script>
        <!-- Select2 Required To Style Datatable Select Box(es) -->
        <!-- https://github.com/fk/select2-bootstrap-css -->
            <script src="<?php echo base_url() ?>assets/scripts/vendor/select2.min.js"></script>

        <!-- Date Time Picker -->
        <!-- https://github.com/smalot/bootstrap-datetimepicker -->
        <!-- NOTE: Original JS file is modified: Proton is forcing bootstrap 2 plugin mode in order to support font icons -->
        <!-- old site js -->
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-datepicker.js' ?>" type="text/javascript"/></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/custom.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-tooltip.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-popover.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-modal.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-collapse.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-dropdown.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/estilos.js' ?>" type="text/javascript"/></script>        
        <script src="<?php echo base_url() . 'application/views/assets/js/popup.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/jquery.formatCurrency.all.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/galleria/galleria-1.2.8.js' ?>" type="text/javascript"/></script>
        
        <script src="<?php echo base_url() . 'application/views/assets/js/htmlarea/jHtmlArea-0.7.5.js' ?>" type="text/javascript"/></script>
        
         <?php  if( isset( $load_charts ) ) {?>
            <script src="https://www.google.com/jsapi" type="text/javascript"/></script>
            <script type="text/javascript">
                google.load('visualization', '1.0', {'packages':['corechart']});
                google.setOnLoadCallback(draw_chart);            
            </script>
            
        <?php } ?>

         <?php  if( isset( $jquery_ui ) && $jquery_ui == true) {?>
            <script src="<?php echo base_url() . 'application/views/assets/js/jquery.ui.core.js' ?>" type="text/javascript"/></script>
            <script src="<?php echo base_url() . 'application/views/assets/js/jquery.ui.widget.js' ?>" type="text/javascript"/></script>
            <script src="<?php echo base_url() . 'application/views/assets/js/jquery-ui-1.8.21.custom.js' ?>" type="text/javascript"/></script>
            <script src="<?php echo base_url() . 'application/views/assets/js/jquery.ui.position.js' ?>" type="text/javascript"/></script>
            <script src="<?php echo base_url() . 'application/views/assets/js/jquery.ui.mouse.js' ?>" type="text/javascript"/></script>
            <script src="<?php echo base_url() . 'application/views/assets/js/jquery.ui.autocomplete.js' ?>" type="text/javascript"/></script>
            <script src="<?php echo base_url() . 'application/views/assets/js/jquery.multiselect.js' ?>" type="text/javascript"/></script> 
            
        <?php } ?>
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyChZoXfwuOEaMUWyD82YFD20K_aLgg7l18&sensor=false" type="text/javascript"/></script>
        <!-- old site js -->

<!--file upload js-->

      <script src="<?php echo base_url()?>assets/template/js/library/bootstrap-fileupload.js"></script>

        <script type="text/javascript">
            
            $('#div_visitors_feed #feed_table').dataTable();
            //$('#div_visitors_table table').dataTable();
            $('#div_visitors_total table').addClass('table-bordered');
        </script>
<!--for textarea -->
    <script src="<?php echo base_url()?>assets/template/js/library/editor/wysihtml5-0.3.0.js"></script>
    <script src="<?php echo base_url()?>assets/template/js/library/editor/bootstrap-wysihtml5.js"></script>

    <script>
        jQuery(document).ready(function($) {
            // pass in your custom templates on init
            $('.textarea').wysihtml5();
        });
    </script>

    <!--Script for autocomplete start-->
    
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.mockjax.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.autocomplete.js"></script>
    <!--Script for autocomplete end-->






