<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7 lt-ie10"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8 lt-ie10"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <title>Proton UI - Tables</title>
        <meta name="description" content="Page Description">
        <!-- //http://localhost/visitorplus/application/views/assets/style/bootstrap.css
 -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    
    <?php $url=uri_string(); 
//echo $this->uri->segment(1);die;
    //die;
  //  if($url=='members/new_member' || $url=='visitors/new_visitor' || $url=='reports' || $this->uri->segment(2)=='show_member' || $this->uri->segment(2)=='show_visitor' || $url=='email/icontact' || $url=="email/process_icontact" || $url=="email" || $url=="email/process" || $url=="email/scheduled")
  if($url=='members/new_member' || $url=='visitors/new_visitor' || $url=='reports' || $this->uri->segment(2)=='show_member' || $this->uri->segment(2)=='show_visitor' || $this->uri->segment(1)=='email'|| $this->uri->segment(2)=='new_order_step_1' || $this->uri->segment(2)=='new_order_step_2' || $this->uri->segment(2)=='final_step')
      { ?>

    <link href="<?php echo base_url()?>assets/template/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/template/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/template/css/stylesheet.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/template/css/library/bootstrap-fileupload.css" rel="stylesheet">
<!--css for editer-->
  <link href="<?php echo base_url()?>assets/template/css/library/jquery-ui-1.10.3.css" rel="stylesheet">


<?php  }else {
    ?>
               <link rel="stylesheet" href="<?php echo base_url()?>assets/stylesnew/bootstrap.css">
<?php } ?> 

    <link href="<?php echo base_url()?>assets/template/icon/font-awesome.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>application/views/assets/style/extensions.css"/>


        <!-- temporary css -->

            <link rel="stylesheet" href="<?php echo base_url()?>assets/stylesnew/vendor/datatables.css" media="screen" />
            <link rel="stylesheet" href="<?php echo base_url()?>assets/stylesnew/proton.css">
            <link rel="stylesheet" href="<?php echo base_url()?>assets/stylesnew/vendor/animate.css">
    
 

            <!--[if lt IE 9]>
            <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
            <script src="scripts/vendor/respond.min.js"></script>
        <![endif]-->


        <!-- temporary css -->
    
<?php if($this->uri->segment(1)=='reports' /*|| $this->uri->segment(1)=='dashboard'*/){ ?>
               <link rel="stylesheet" href="<?php echo base_url()?>assets/stylesnew/bootstrap.css">
<?php } ?> 


        <!-- Proton CSS: -->
        <!-- <link rel="stylesheet" href="<?php echo base_url()?>assets/styles/proton2.css"> -->


        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css"> -->

        <!-- Page-specific Plugin CSS: -->
        <!-- <link rel="stylesheet" href="<?php echo base_url() ?>assets/styles/vendor/select2/select2.css"> -->
        <!-- <link rel="stylesheet" href="<?php echo base_url() ?>assets/styles/vendor/datatables.css" media="screen" /> -->


        <!-- Proton CSS: -->
         <!-- <link rel="stylesheet" href="<?php echo base_url() ?>assets/styles/proton.css"> -->
        <!-- <link rel="stylesheet" href="<?php echo base_url() ?>assets/styles/vendor/animate.css"> -->

        <!-- adds CSS media query support to IE8   -->
        <!--[if lt IE 9]>
            <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
            <script src="<?php echo base_url() ?>assets/scripts/vendor/respond.min.js"></script>
        <![endif]-->

        <!--DatePicker-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/views/assets/style/datepicker.css"/>
<!--         <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css">
 -->
        <!-- Fonts CSS: -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/styles/font-awesome.css" type="text/css" />
        <!-- <link rel="stylesheet" href="<?php echo base_url() ?>assets/styles/font-titillium.css" type="text/css" /> -->
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/styles/custom.css"> -->
        <!-- Common Scripts: -->
        <!--for datepiker-->
        <link href="<?php echo base_url()?>application/views/assets/style/jquery-ui-1.8.21.custom.css" rel="stylesheet">


<!--header code-->
        <script>
        (function () {
          var js;
          if (typeof JSON !== 'undefined' && 'querySelector' in document && 'addEventListener' in window) {
            js = 'http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js';
          } else {
            js = 'http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js';
          }
          document.write('<script src="' + js + '"><\/script>');
        }());
        </script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.4.0/jquery-migrate.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/vendor/modernizr.js"></script>
        <script src="<?php echo base_url() ?>assets/scripts/vendor/jquery.cookie.js"></script>
        

<!--end-->


        

    </head>

    <body>
       <script>
            var theme = $.cookie('protonTheme') || 'default';
            $('body').removeClass (function (index, css) {
                return (css.match (/\btheme-\S+/g) || []).join(' ');
            });
            if (theme !== 'default') $('body').addClass(theme);
        </script>
        <!--[if lt IE 8]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <nav class="main-menu" data-step='2' data-intro='This is the extendable Main Navigation Menu.' data-position='right'>
            <ul>
                <li id="a_dashboard">

                   <?php if( in_array($user->person_type, array( Person::TYPE_USER )) ) {?> 
                    <a href="<?php echo site_url('dashboard')?>">
                        <i class="icon-home nav-icon"></i>
                        <span class="nav-text">
                            Dashboard
                        </span>
                    </a>
                </li>
                <li id="a_members" class="has-subnav">
                    <a href="<?php echo site_url('members')?>">
                        <i class="icon-laptop nav-icon"></i>
                        <span class="nav-text">
                            Members
                        </span>
                        <i class="icon-angle-right"></i>
                    </a>
                    <ul>
                        <li>
                            <a class="subnav-text" href="<?php echo site_url('members/new_member')?>">
                                Add Member
                            </a>
                        </li>
                        <li>
                            <a class="subnav-text" href="<?php echo site_url('members')?>">
                                View Members
                            </a>
                        </li>
                        <li>
                            <a class="subnav-text" href="<?php echo site_url('members/export')?>">
                                Export Members
                            </a>
                        </li>
                    </ul>
                </li>
                <li id="a_visitors" class="has-subnav">
                    <a href="<?php echo site_url('visitors')?>">
                        <i class="icon-list nav-icon"></i>
                        <span class="nav-text">
                            Visitors
                        </span>
                        <i class="icon-angle-right"></i>
                    </a>
                    <ul>
                        <li>
                            <a class="subnav-text" href="<?php echo site_url('visitors/new_visitor')?>">
                                Add new visitor
                            </a>
                        </li>
                        <li>
                            <a class="subnav-text" href="<?php echo site_url('visitors')?>">
                                View Visitors
                            </a>
                        </li>
                        <li>
                            <a class="subnav-text" href="<?php echo site_url('visitors/export')?>">
                                Export Visitors
                            </a>
                        </li>
                    </ul>
                </li>
                <?php if($user->member[0]->user_id) { ?>
                <li id="a_giving">
                    <a href="<?php echo site_url('giving')?>">
                        <i class="icon-folder-open nav-icon"></i>
                        <span class="nav-text">
                            Giving
                        </span>

                    </a>
                </li>
                <?php  } ?>
                <li id="a_schedules">
                    <a href="<?php echo site_url('schedule')?>">
                        <i class="icon-bar-chart nav-icon"></i>
                        <span class="nav-text">
                            Schedule
                        </span>
                    </a>
                </li>
                <li id="a_reports">
                    <a href="<?php echo site_url('reports')?>">
                        <i class="icon-font nav-icon"></i>
                        <span class="nav-text">
                            Reports
                        </span>
                    </a>
                </li>
                <li id="a_email" class="has-subnav">
                    <a href="javascript:void(0)">
                        <i class="icon-table nav-icon"></i>
                        <span class="nav-text">
                            Email
                        </span>
                        <i class="icon-angle-right"></i>
                    </a>
                    <ul>
                        <li>
                            <a class="subnav-text" href="<?php echo site_url('email')?>">
                                Message
                            </a>
                        </li>
                        <li>
                            <a class="subnav-text" href="<?php echo site_url('email/icontact')?>">
                                Newsletter
                            </a>
                        </li>
                    </ul>
                </li>

                <?php 
                  if( isset($postcard_menu )){
                    $caption = $order_type == Order::ORDER_LETTER ? 'Letter' : 'Postcard';
                    $controller = $order_type == Order::ORDER_LETTER ? 'envelope' : 'paper';
                ?>
                <li id="a_paper" class="has-subnav">
                    <a href="<?php echo site_url('paper/new_order_step_1')?>">
                        <i class="icon-map-marker nav-icon"></i>
                        <span class="nav-text">
                            Postcards
                        </span>
                        <i class="icon-angle-right"></i>
                    </a>

                    <ul>
                        <li>
                            <a class="subnav-text" href="<?php echo site_url($controller.'/new_order_step_1')?>">
                                New <?php echo $caption ?>
                            </a>
                        </li>
                        <li>
                            <a class="subnav-text" href="<?php echo site_url($controller)?>">
                                <?php echo $caption ?>s
                            </a>
                        </li>
                        <li>
                            <a class="subnav-text" href="<?php echo site_url($controller.'/paper_progress')?>">
                                <?php echo $caption ?>s in progress
                            </a>
                        </li>
                        <li>
                            <a class='subnav-text' href="<?php echo site_url($controller.'/paper_sent')?>">
                                <?php echo $caption ?>s sent
                            </a>
                        </li>
                    </ul>
                    
                </li>
                <?php }else{ ?>

                <li id="a_paper">
                    <a href="<?php echo site_url('paper/new_order_step_1')?>">
                        <i class="icon-map-marker nav-icon"></i>
                        <span class="nav-text">
                            Postcards
                        </span>
                    </a>
                </li>

                <?php } ?>
                <li id="a_reports">
                    <a href="<?php echo site_url('dashboard/settings')?>" target="_blank">
                        <i class="icon-cogs nav-icon"></i>
                        <span class="nav-text">
                            My Settings
                        </span>
                    </a>
                </li>

              <?php } ?>
              <?php if( in_array($user->person_type, array(Person::TYPE_ADMIN)) ) { ?>

                <li id="a_admin">
                    <a href="<?php echo site_url('admin')?>">
                        <i class="icon-map-marker nav-icon"></i>
                        <span class="nav-text">
                            Admin
                        </span>
                    </a>
                </li>
                <li id="a_admin_orders">
                    <a href="<?php echo site_url('admin/orders')?>">
                        <i class="icon-map-marker nav-icon"></i>
                        <span class="nav-text">
                            Orders
                        </span>
                    </a>
                </li>
                <li id="a_admin_postcards">
                    <a href="<?php echo site_url('admin/postcards')?>">
                        <i class="icon-map-marker nav-icon"></i>
                        <span class="nav-text">
                            Postcards
                        </span>
                    </a>
                </li>
              <?php } ?>
            </ul>

            <ul class="logout">
                <li>
                    <a href="<?php echo site_url('login')?>">
                        <i class="icon-off nav-icon"></i>
                        <span class="nav-text">
                            Logout
                        </span>
                    </a>
                </li>  
            </ul>
        </nav>

        <script>
            $(document).ready(function() {
                if ($.cookie('protonSidebar') == 'retracted') {
                    $('.sidebar').removeClass('extended').addClass('retracted');
                    $('.wrapper').removeClass('retracted').addClass('extended');
                }
                if ($.cookie('protonSidebar') == 'extended') {
                    $('.wrapper').removeClass('extended').addClass('retracted');
                    $('.sidebar').removeClass('retracted').addClass('extended');
                }
            });
        </script>


<section class="wrapper extended scrollable">
    <script>
        if (!($('body').is('.dashboard-page') || $('body').is('.login-page'))){
            if ($.cookie('protonSidebar') == 'retracted') {
                $('.wrapper').removeClass('retracted').addClass('extended');
            }
            if ($.cookie('protonSidebar') == 'extended') {
                $('.wrapper').removeClass('extended').addClass('retracted');
            }
        }
    </script>

    <nav class="user-menu">
        <a href="javascript:;" class="main-menu-access">
            <i class="icon-proton-logo"></i>
            <i class="icon-reorder"></i>
        </a>
    </nav>


    <section class="title-bar">
        <div>
            <span><?php echo $user->name ?></span>
        </div>
    </section>
    
    <nav class="quick-launch-bar">
        <ul>

                   <?php if( in_array($user->person_type, array( Person::TYPE_USER )) ) {?> 
                <li id="a_dashboard">
                    <a href="<?php echo site_url('dashboard')?>">
                        <i class="icon-home nav-icon"></i>
                        <span class="nav-text">
                            Dashboard
                        </span>
                    </a>
                </li>
                <li id="a_members" class="has-subnav">
                    <a href="<?php echo site_url('members')?>">
                        <i class="icon-laptop nav-icon"></i>
                        <span class="nav-text">
                            Members
                        </span>
                    </a>
                </li>    
                   
                <li id="a_visitors" class="has-subnav">
                    <a href="<?php echo site_url('visitors')?>">
                        <i class="icon-list nav-icon"></i>
                        <span class="nav-text">
                            Visitors
                        </span>
                    </a>
                   
                </li>
                <?php if($user->member[0]->user_id) { ?>

                <li id="a_giving">
                    <a href="<?php echo site_url('giving')?>">
                        <i class="icon-folder-open nav-icon"></i>
                        <span class="nav-text">
                            Giving
                        </span>

                    </a>
                </li>
                <?php } ?>
                <li id="a_schedules">
                    <a href="<?php echo site_url('schedule')?>">
                        <i class="icon-bar-chart nav-icon"></i>
                        <span class="nav-text">
                            Schedule
                        </span>
                    </a>
                </li>
                <li id="a_reports">
                    <a href="<?php echo site_url('reports')?>">
                        <i class="icon-font nav-icon"></i>
                        <span class="nav-text">
                            Reports
                        </span>
                    </a>
                </li>
                <li id="a_email" class="has-subnav">
                    <a href="<?php echo site_url('email')?>">
                        <i class="icon-table nav-icon"></i>
                        <span class="nav-text">
                            Email
                        </span>
                    </a>
                    
                </li>
                <?php 
                  if( isset($postcard_menu )){
                    $caption = $order_type == Order::ORDER_LETTER ? 'Letter' : 'Postcard';
                    $controller = $order_type == Order::ORDER_LETTER ? 'envelope' : 'paper';
                ?>
                <li id="a_paper" class="has-subnav">
                    <a href="<?php echo site_url('paper/new_order_step_1')?>">
                        <i class="icon-map-marker nav-icon"></i>
                        <span class="nav-text">
                            Postcards
                        </span>
                    </a>

                    
                </li>
                <?php }else{ ?>

                <li id="a_paper">
                    <a href="<?php echo site_url('paper/new_order_step_1')?>">
                        <i class="icon-map-marker nav-icon"></i>
                        <span class="nav-text">
                            Postcards
                        </span>
                    </a>
                </li>
                <li id="a_admin_postcards">
                    <a href="<?php echo site_url('dashboard/settings')?>" target="_blank">
                        <i class="icon-cogs" aria-hidden="true"></i>
                        <span class="nav-text">
                            My Settings
                        </span>
                    </a>
                </li> 
                <?php } ?>
              <?php } ?>
              <?php if( in_array($user->person_type, array(Person::TYPE_ADMIN)) ) { ?>

                <li id="a_admin">
                    <a href="<?php echo site_url('admin')?>">
                        <i class="icon-map-marker nav-icon"></i>
                        <span class="nav-text">
                            Admin
                        </span>
                    </a>
                </li>
                <li id="a_admin_orders">
                    <a href="<?php echo site_url('admin/orders')?>">
                        <i class="icon-map-marker nav-icon"></i>
                        <span class="nav-text">
                            Orders
                        </span>
                    </a>
                </li>
                <li id="a_admin_postcards">
                    <a href="<?php echo site_url('admin/postcards')?>">
                        <i class="icon-map-marker nav-icon"></i>
                        <span class="nav-text">
                            Postcards
                        </span>
                    </a>
                </li>

              <?php } ?>

        </ul>
        
    </nav>
<style type="text/css">
    .panel-heading h4 {
    color: #fff !important;
}
 .alert-success {
    background-color: #dff0d8!important;
    border-color: #46886a!important;
    color: #468847!important;
}
.alert-danger, .alert-error {
    background-color: #f2dede!important;
    border-color: #b94a48!important;
    color: #b94a48!important;
}
.well .alert {
    margin-bottom: 15px;
}
.alert-danger, .alert-error {
    background-color: #f2dede;
    border-color: #b94a48;
    color: #b94a48;
}
.alert, .alert h4 {
    color: #9e703c;
}
.alert .close {
    line-height: 20px;
    position: relative;
    right: 0px;
    top: 0px;
}
button.close {
    background: transparent none repeat scroll 0 0;
    border: 0 none;
    cursor: pointer;
    padding: 0;
}
.close {
    color: #333;
    float: right;
    font-size: 14px;
    font-weight: bold;
    line-height: 20px;
    opacity: 0.5;
    text-shadow: 0 1px 0 #ffffff;
}
.myclass {
    margin-top: -55px;
}
</style>
