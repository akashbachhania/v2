<!DOCTYPE html>
<html>
<head>
    <title>Visitor Plus</title>
    <?php $this->load->view('login/login_header');?>
</head>
<body>        
    <div class="container">
      <section id="gridSystem">
          <div>
              <div class="span6 offset2 well">
                <form class="form-horizontal" action="<?php echo site_url('login/update_pwd')?>" method="post">
                    <fieldset>
                        <legend><img src="http://visitorplus.net/live/application/views/assets/img/visitorplus.logo.png" width="157" height="50" alt="Visitor Plus logo"></legend>
                        <div class="control-group">
                          <label class="control-label" for="input01">Username</label>
                          <div class="controls">
                            <input type="text" class="input-xlarge" name="username"id="input01">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="input02"></label>
                        <div class="controls">
                            <input type="submit" class="btn span2" name="submit" id="submit" value="Ok">
                        </div>
                    </div>                                                           
                </fieldset>
            </form>
            <h3><?= isset($msg)?$msg:'';?></h3>          
        </div>          
    </div>
</section>
</div>
</body>
</html>            