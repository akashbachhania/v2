<!DOCTYPE html>
<html>
<head>
    <title>Visitor Plus</title>
    <?php $this->load->view('login/login_header');?>
</head>
<body>        
    <div class="container">
      <section id="gridSystem">
          <div>
              <div class="span6 offset2 well">
                <form class="form-horizontal" action="" method="post">
                    <fieldset>
                        <legend><img src="http://visitorplus.net/live/application/views/assets/img/visitorplus.logo.png" width="157" height="50" alt="Visitor Plus logo"></legend>
                        <div class="control-group">
                        <label class="control-label" for="input01"><span style="color:#000;">Password</span></label>
                          <div class="controls">
                            <input type="password" class="input-xlarge" name="password"id="input01">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="input02"><span style="color:#000;">Confirm Password</span></label>
                        <div class="controls">
                            <input type="password" class="input-xlarge" name="cnf_password" id="input02">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="input02"></label>
                        <div class="controls">
                            <input type="hidden" name="id" id="id" value="<?= $id?>">
                            <input type="submit" class="btn span2" name="submit" id="submit" value="Ok"><br/><br/>
                            
                        </div>
                    </div>                                                           
                </fieldset>
            </form>          
        </div>          
    </div>
</section>
</div>
</body>
</html>            
<script type="text/javascript">
    
    $('form').submit(function(event){
        event.preventDefault();
        var pwd = $('#input01').val();
        var cnf_pwd = $('#input02').val();
        var id = $('#id').val();
        if( pwd == cnf_pwd ){
          // alert(String(window.location));
            $.post(String(window.location),{ password:pwd,id:id },function(data){
                $('form').hide();
                $('#gridSystem > div > div.span6').append('<h2 style="color:#000;text-align:center;">'+data+'</h2><br/><br/><a class="btn btn-default" href="<?= base_url()?>login" style="margin-left:40%;">Login</a>');
            });
        }


    })

</script>