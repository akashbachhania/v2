<?php
class Schedule_model extends CI_Model{

    function getAllMembers(){
        $this->db->select('dr_persons.id,first_name,last_name');
        $this->db->from('dr_persons');
        $result = $this->db->get();
        if($result->num_rows() > 0) {
            return $result->result();
        } 
        return false;
    }   

    function getMembersByMinistryVistor($ministry_id,$company_id,$visitor_id){
        $this->db->select('id');
        $this->db->from('dr_ministries_log');
        $this->db->where('dr_ministries_log.type_id',$ministry_id);
        $this->db->where('dr_ministries_log.company_id',$company_id);
        $this->db->where('dr_ministries_log.visitor_id',$visitor_id);
        $result = $this->db->get();
        if($result->num_rows() > 0) {
            return $result->result();
        } 
        return false;
    }

    function getMembersByMinsitry($ministry_id,$company_id) {
        $this->db->select('dr_persons.id,first_name,last_name');
        $this->db->from('dr_ministries_log, dr_persons');
        $this->db->where('dr_ministries_log.visitor_id = dr_persons.id');
        $this->db->where('dr_ministries_log.type_id',$ministry_id);
        $this->db->where('dr_ministries_log.company_id',$company_id);
        $result = $this->db->get();
        if($result->num_rows() > 0) {
            return $result->result();
        } 
        return false;
    }

    function getDateSchedule($from_date,$to_date,$company_id) {        
        $this->db->select("sch.*,mins.name as ministry_name");
        $this->db->from("dr_schedules as sch,dr_types as mins");
        $this->db->where("mins.id = sch.ministry_id");
        $this->db->where("sch.date_schedule >=", date('Y-m-d',$from_date));
        $this->db->where("sch.date_schedule <=", date('Y-m-d',$to_date));
        $this->db->where("sch.company_id", $company_id);
        $this->db->order_by("sch.date_schedule", 'ASC');
        $query = $this->db->get();

        if( 0 < $query->num_rows() ) {
            return $query->result();
        }
        return false;
    }

    function getInfoDateSchedule($from_date,$to_date,$company_id) { 
        $this->db->select(" `sch`.*, `sch`.id as sid, 
                          `mins`.`name` as ministry_name,
                          `per`.`first_name`,
                          `per`.`last_name`,
                          `per`.`email`,
                          `per`.`id` as person_id");
        $this->db->from(" `dr_schedules` AS sch,
                          `dr_types` AS mins,
                          `dr_schedules_relation` AS re,
                          `dr_persons` AS per");
        $this->db->where("mins.id = sch.ministry_id");
        $this->db->where("re.member_id = per.id");
        $this->db->where("re.schedule_id = sch.id");
        $this->db->where("sch.date_schedule >=", date('Y-m-d',$from_date));
        $this->db->where("sch.date_schedule <=", date('Y-m-d',$to_date));
        $this->db->where("sch.company_id", $company_id);
        $this->db->where("mins.type", 5);
        $this->db->order_by("sch.date_schedule", 'ASC');
        $query = $this->db->get();
   
        if( 0 < $query->num_rows() ) {
            return $query->result();
        }
        return false;
    }

    function getScheduleInfo($schedule_id) {
        //role_id is ministry_id
        $this->db->select("type.name rol_name,per.first_name,per.last_name,per.email,per.id as person_id,type.id as role_id");
        $this->db->from("dr_schedules_relation as re,dr_types as type,dr_persons as per");
        $this->db->where("re.schedule_id",$schedule_id);
        $this->db->where("type.type",5);
        $this->db->where("re.member_id = per.id");
        $this->db->where("type.id = re.role_id");
        $this->db->order_by("re.role_id",'ASC');
        $query = $this->db->get();

        if( 0 < $query->num_rows() ) {
            return $query->result();
        }
        return false;
    }
}
