<?php
class Cron_model extends orm{    
    public function get_new_visitors($company_id,$start_date,$last_date){
        
        $this->db->select('first_name,last_name,phone_1,address,id');
        $this->db->from('dr_persons');
        $this->db->where('registered_date >=',$start_date);
        $this->db->where('registered_date <=',$last_date);
        $this->db->where('company_id',$company_id);
        $result = $this->db->get();
        
        if($result->num_rows() > 0)
            return $result->result();
        else
            return false;
    }
    public function getAllCompaniesWithPrimaryContact() {
        $this->db->select('id,primary_contact');
        $this->db->from('dr_companies');
        $this->db->where('primary_contact !=','');
        $result = $this->db->get();
        
        if($result->num_rows() > 0)
            return $result->result();
        else
            return false;
    }
    function getTodayBirthdayPersons($company_id) {
        //SELECT * FROM dr_persons WHERE MONTH(birth) = '9' and DAY(birth) = '19' ORDER BY `id` DESC 
        
        $this->db->select('id');
        $this->db->from('dr_persons');
        $this->db->where('MONTH(birth)',date('m'));
        $this->db->where('DAY(birth)',date('d'));
        $this->db->where('company_id',$company_id);
        $this->db->where('person_type',1);
        $result = $this->db->get();
        
        if($result->num_rows() > 0)
            return $result->result();
        else
            return false;
    }
    
    public function getAllCompanies() {
        $this->db->select('id');
        $this->db->from('dr_companies');
        $result = $this->db->get();
        
        if($result->num_rows() > 0)
            return $result->result();
        else
            return false;
    }
}
