<?php
class Giving_model extends CI_Model{
    
    function getGivingMember($member_id,$giving_id,$date) {
        
        $this->db->select('*');
        $this->db->from('dr_member_giving');
        $this->db->where('member_id',$member_id);
        $this->db->where('giving_id',$giving_id);
        $this->db->where('giving_date',$date);
        $result = $this->db->get();
        if($result->num_rows() > 0) {
            return $result->result();
        } 
        return false;
    }
    function getGivingMemberTotalDate($member_id,$giving_id,$start_date,$end_date) {
        $this->db->select('sum(value) as total');
        $this->db->from('dr_member_giving');
        $this->db->where('member_id',$member_id);
        $this->db->where('giving_id',$giving_id);
        $this->db->where('giving_date >=',$start_date);
        $this->db->where('giving_date <=',$end_date);
        $result = $this->db->get();
        
        if($result->num_rows() > 0) {
            $row = $result->row();
            
            return $row->total;
        } 
        return 0;
    }
}
