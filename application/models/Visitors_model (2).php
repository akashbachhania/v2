<?php
class Visitors_model extends Orm{
    public function valid_user( $user_id, Person $user){ 
        $filter = new Person();
        $filter->set_company_id( $user->company_id);
        $filter->set_id($user_id);
        
        $object = $this->get_object( $filter, true );
        
        $valid = false;
        if( $object ){
            $valid = true;
        }
        return $valid;
    }
 
    public function get_assignments(){        
        $assignments = $this->model->get_object( new Type(null, $this->user->company_id, Type::TYPE_ASSIGNMENT));
        $array = array();
        foreach( $assignments as $assignment){
            $array[$assignment->id] = $assignment->name;
        }
        $array = array(''=>'Select Item','add_new'=>'Add Item')+$array;
        return $array;
    }
    
    
    public function get_ministries(){        
        $ministries = $this->model->get_object( new Type(null, $this->user->company_id, Type::TYPE_MINISTRY));
        $array = array();
        foreach( $ministries as $ministry){
            $array[$ministry->id] = $ministry->name;
        }
        $array = array(''=>'Select Item','add_new'=>'Add Item')+$array;
        return $array;
    }
    
    
    public function get_ministries_roles($ministry_id){        
        $ministries = $this->model->get_object( new Ministry(null, $this->user->company_id, $ministry_id));
        $array = array();
        foreach( $ministries as $ministry){
            $array[$ministry->id] = $ministry->name;
        }
        $array = array(''=>'Select Item','add_new'=>'Add Item')+$array;
        return $array;
    }
    
    public function get_members(){
        $members = array();
        $all = $this->orm->get_object( new Visitor(null, $this->user->company_id));
        foreach( $all as $person){
            $members[$person->id] = $person->first_name.' '.$person->last_name;
        }
        return array(''=>'No')+ $members;        
    }
    
    public function get_referers(){
        $referers = array(
            ''        => 'Select',
            'add_new' => 'Add New',
            Visitor::REFERRED_BY_FRIEND    => 'Friend',  
            Visitor::REFERRED_BY_NEWSPAPER => 'Newspaper',
            Visitor::REFERRED_BY_OTHER     => 'Other', 
            Visitor::REFERRED_BY_WALKIN    => 'Walkin', 
            Visitor::REFERRED_BY_MEMBER    => 'Member',    
        );
        $all = $this->orm->get_object( new Type(null, $this->user->company_id, Type::TYPE_REFERER));
        foreach( $all as $type){
            $referers[$type->id] = $type->name;
        }
        
        return $referers;         
    }
    
    public function get_done_by_persons(){
        $done_bys = array(
            ''        => 'Select',
            'add_new' => 'Add New',
    
        );
        $all = $this->orm->get_object( new Type(null, $this->user->company_id, Type::TYPE_DONE_BY));
        foreach( $all as $type){
            $done_bys[$type->id] = $type->name;
        }
        

        return $done_bys;        
    }
    
    function getRangeVisitors ($start_date, $end_date,$company_id) {
        $queryOne = "select first_name,last_name,city,address,zip,state,phone_1,email 
                        from dr_persons                         
                        where registered_date >= '".$start_date."' and registered_date <= '".$end_date."' and company_id =".$company_id.' and person_type='.Person::TYPE_VISITOR.' order by id ASC';
       
        $query  = $this->db->query( $queryOne );      
        
        if( 0 < $query->num_rows() ) {            
            return ( $query->result() );
        }

        return false;
    }
    
    function getRangeMembers ($start_date, $end_date,$company_id) {
        $queryOne = "select first_name,last_name,city,address,zip,state,phone_1,email 
                        from dr_persons                         
                        where registered_date >= '".$start_date."' and registered_date <= '".$end_date."' and company_id =".$company_id.' and person_type='.Person::TYPE_MEMBER.' order by id ASC';
       
        $query  = $this->db->query( $queryOne );      
        
        if( 0 < $query->num_rows() ) {            
            return ( $query->result() );
        }

        return false;
    }
    
}  
?>
