<?php
class Dashboard_model extends orm{
    function getGrantMembers($company_id) {
        $query = $this->db->get_where('dr_funds_members', array('company_id' => $company_id));
        if($query->num_rows() > 0)
            return $query->result();
        return false;
    }
}
