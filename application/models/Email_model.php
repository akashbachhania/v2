<?php
class Email_model extends orm{
    
    public function get_recipients( $ids ){
        $visitors = array();
        if( $ids ){
            foreach( $ids as $id ){
                $visitors[] = $this->get_object( new Visitor($id), true);
            }
        } else {
            $visitors = $this->get_object( new Visitor());
        }
        return $visitors;        
    }
    
    public function get_templates($event){
        $template = new Template();
        $template->set_event($event);
        $templates = $this->get_object(
            $template
        );
        return $templates;
    }
        
}
