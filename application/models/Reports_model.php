<?php
class Reports_model extends CI_Model{
    
    function getMembersByMinsitry($start_date,$end_date) {
        
        $this->db->select("person.first_name,person.last_name,giving.giving_date,giving.value,funds.name fund_name");
        $this->db->from('dr_member_giving as giving,dr_persons as person,dr_types as funds');
        $this->db->where('giving.member_id = person.id');
        $this->db->where('giving.giving_id = funds.id');
        $this->db->where('giving.giving_date >=',$start_date);
        $this->db->where('giving.giving_date <=',$end_date);
        $result = $this->db->get();
        if($result->num_rows() > 0) {
            return $result->result();
        } 
        return false;
    }
}
