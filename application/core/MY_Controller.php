<?php   
class MY_Controller extends CI_Controller{
    /**
    * put your comment there...
    * 
    * @var orm
    */
    protected $orm;
    protected $user;
    protected $companies;
    public function __construct(){
        $this->companies = array();
        parent::__construct();

        // ini_set('display_errors', 1);
        // error_reporting(E_ALL ^ E_NOTICE);

        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        //Main Model for this project. Contains DB communication and Record classes
        $this->load->model('orm','model', TRUE );
        $this->config->load('visitors_config');              
    }
    protected function authorize_or_redirect( $authorize_types = array()){ 
        $id = $this->session->userdata('id');
        $person_type = $this->session->userdata('person_type');
        $company_id  = ($company_id =$this->session->userdata('company_id')) ? $company_id : 0;
         
        if( ($id == FALSE  && $person_type == FALSE) || 
             ($authorize_types && !in_array( $person_type, $authorize_types ))   ){
             $this->redirect();
        } else {
 
            $filter  = new Person();
            $filter->set_id( $id );
            $filter->set_person_type( $person_type );
            
            $persons = $this->model->get_object( $filter );
            
            if( $persons ){
                $person = reset( $persons );
                $this->user = $person;
                $member = $this->model->get_funds_member($id);
                $this->user->member = $member;
                /**
                * Override company id for ADMIN (can see all companies)
                */
                if( $this->user->person_type == Person::TYPE_ADMIN ){
                    $this->user->set_company_id( $company_id );
                    
                    $this->companies = $this->model->get_companies( $this->user ) + array('0' => '-Select Company-');
                    
                }
                //Some helper libraries our view uses
                $this->load->library('table');
                //Used for Record form creation
                $this->load->library('output_helper');
                //Used for Mapping of Input to Record class
                $this->load->library('input_mapper', array('input'=>$this->input));                
            }  else {
                $this->redirect();
            }
        }
    }
    protected function redirect(){
        header('Location: ' . site_url('login/'));        
    }
    protected function get_path()    {
        $path = str_replace('index.php','', site_url() ); 
        $path = $path . 'application/views/assets/img/covers/';
        return $path;        
    }
    protected function get_local_path(){
        $path = parse_url( site_url()); 
        $path = str_replace('index.php','',$path['path']);        
        return $_SERVER['DOCUMENT_ROOT'].$path;
    }
    protected function get_mail_config(){
        return array(
            'mailtype'  => $this->config->item('mailtype'),    
            'protocol'  => $this->config->item('protocol'),
            'smtp_host' => $this->config->item('smtp_host'),
            'smtp_user' => $this->config->item('smtp_user'),
            'smtp_pass' => $this->config->item('smtp_pass')                        
        );   
    }
    
    function get_us_date_format( $date ) {
        if( preg_match('!(?P<year>\d\d\d\d)-(?P<month>\d\d)-(?P<day>\d\d)\s*(?P<rest>.*)!', $date, $matches ) ){
            $date = $matches['month'].'-'.$matches['day'].'-'.$matches['year'].' '.( isset($matches['rest']) ?' '.$matches['rest']: '' );
        }
        return $date;    
    }    
}
