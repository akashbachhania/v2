<?php
class Schedule_report{
    
   public function __construct(){ 
       require_once('application/libraries/MPDF54/mpdf.php');
       
   }
   public function create_pdf( $company,$schedules,$start_date, $end_date,  $file = false ){
   
    if(is_array($schedules) and count($schedules) > 0) {
        $temp = array();
        foreach($schedules as $skey => $schedule){
            $time_type = array();
            foreach ($schedule as $key => $row)
            {
                $time_type[$key] = $row['time_type'];
            }
            array_multisort($time_type, SORT_ASC, $schedule);
            $temp[$skey] = $schedule;
        }
        $schedules = $temp;
    }

    $temp = array();
    foreach ($schedules as $key => $schedule) {
        $date = $key;
        foreach ($schedule as $key => $row){
            if($row['time_type'] == 'AM'){
                $temp[$date.'/AM'][] = $row;
            }else{
                $temp[$date.'/PM'][] = $row;
            }
        }
    }

    $schedules = $temp;
    //echo "<pre>";print_r($temp);die;

    //echo "<pre>";print_r($schedules);die;

    //$schedules = new Array('2016-02-02', '2016-02-03', '2016-02-05', '2016-02-07'));

    //echo "<pre>";print_r($schedules);die;
        $mpdf=new mPDF('win-1252','A4-L','','arial',20,15,25,10,10,10);
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle( $company->name . '-Funding:' . $company->id );
        $mpdf->SetAuthor($company->name);
        $mpdf->SetWatermarkText("Paid");
        $mpdf->showWatermarkText = false;
        $mpdf->watermark_font = 'DejaVuSansCondensed';
        $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->keep_table_proportions = true;
        $mpdf->showImageErrors = true;
        
        $html = '<html>
    <head>
        <style>
            body {font-family: sans-serif;
                font-size: 12pt;
            }
            p { margin: 0pt;
            }
            td { vertical-align: top; }  
            ul{width:20%; float:left;list-style:none !important;}
            ul li{width:20%; float:left;list-style:none !important;}
            </style>
    </head>
    <body>
        

        <!--mpdf
        <htmlpageheader name="myheader">
        <table width="100%" style=""><tr>
        <td width="50%" style="color:#000000;font-size:10px;"><span style="font-weight: bold; font-size: 12pt;">'.$company->name.'</span>&nbsp;&nbsp;'.$company->address.', '.$company->city.','.$company->state.','. $company->zip. ', '.$company->phone_1.', '. $company->email .'</td>
        </tr></table>

        </htmlpageheader>

        <htmlpagefooter name="myfooter">
        <div style="border-top: 1px solid #000000; font-size: 12pt; text-align: center; padding-top: 3mm; ">
        Page {PAGENO} of {nb}
        </div>
        </htmlpagefooter>

        <sethtmlpageheader name="myheader" value="on" show-this-page="1" />
        <sethtmlpagefooter name="myfooter" value="on" />
        mpdf-->';
        
        
        if(is_array($schedules) and count($schedules) > 0) {

            $html .= '<table style="width:150%!important;font-family: sans;border-collapse: collapse; font-size: 16px !important; margin-top:-10px !important" cellpadding="2">';
            $i = 0;
            $v = 0;
            foreach($schedules as $date => $schedule) {
                $data = explode("/",$date);
                $date = $data[0];
                $s_c = count($schedule);
                if($v == ($s_c - 1)){
                    $v = 0;
                }
                if($i%4 == 0){
                    $html .= '<tr>';
                }
                    $html .= '<td>';
                        $html .= '<table width="100%" style="font-family: sans;border-collapse: collapse;" cellpadding="2">';
                
                        $html .='<tr><td> <b>'.date('l',strtotime($date)).' '.date('m-d-Y',strtotime($date)).'</b><br/>'.$schedule[$v]["time_type"].'</td></tr><br/><br/>';   
                            
                        if(is_array($schedule) and count($schedule) > 0) {
                            $subTotal = 0;
                            $j = 0;
                            foreach($schedule as $info) {  
                                if( $info['time_type'] == 'AM'){                      
                                    $html .= '<tr><td width="25%">'.$info['title'].'</td></tr>';
                                    $j++;
                                }else{
                                    if($j != 0){
                                        $k = 0;
                                        if($k == 0){ 
                                            $html .= '</table><br/><br/><br/>';
                                            $html .= '</td>';
                                            $html .= '<td>';
                                            $html .= '<table width="100%" style="font-family: sans;border-collapse: collapse;" cellpadding="2">';
                                            $html .= '<tr><td> <b>'.date('l',strtotime($date)).' '.date('m-d-Y',strtotime($date)).'</b><br/>'.$info["time_type"].'</tr></td><br/><br/>';
                                            $html .= '<tr><td width="25%">'.$info['title'].'</tr></td>';
                                            $j = 0;
                                        }else{
                                            $html .= '<tr><td width="25%">'.$info['title'].'</td></tr>';
                                        }
                                    }else{
                                        $html .= '<tr><td width="25%">'.$info['title'].'</td></tr>';
                                    }
                                }

                             }                   
                        }
                        $html .= '</table><br/><br/><br/>';
                    $html .= '</td>';
                $i++;
                //echo $i;
                if($i%4 == 0){
                    $html .= '</tr>';
                }
            }
            
            $html .= '</table>';

                // $html .= '<ul>';
                
                // $html .='<li> <b>'.date('l',strtotime($date)).'<br />'.date('m-d-Y',strtotime($date)).'</b></li><br/><br/>';   
                    
                // if(is_array($schedule) and count($schedule) > 0) {
                //     $subTotal = 0;
                    
                //     foreach($schedule as $info) {                        
                //         $html .='<li style="margin-bottom:5px;">'.$info['title'].'</li>';
                //     }                   
                // }
                // $html .='</ul><br/><br/><br/>';
                
            
        }
                

    $html .= '</body>
</html>';      

//echo $html;die;  
   
        $mpdf->WriteHTML( $html );
        if( $file ){
            $mpdf->Output( $file, 'F');    
        } else {
            $mpdf->Output( $start_date, 'I');    
        }
   }
}