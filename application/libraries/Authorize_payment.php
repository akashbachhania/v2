<?php
class Authorize_payment{
    
    const STATUS_FAILED = 'failed';
    const STATUS_SUCCESS = 'success';
    
    public $request  = 'No request sent';
    public $response = 'No response';
    
    public $auth_code = null;
    public $transaction_id = null;
    
      
    public function send_payment_request( $amount, $cc_num, $cc_date, $order_id, $merchant, $transaction, $url, $cvn = null, $first_name=null, $last_name=null, $company=null, $address=null, $city=null, $state=null, $zip=null, $country=null, $transaction_type = 'authOnlyTransaction'){
        $response = array();    
        $response['status'] = self::STATUS_FAILED;
        $response['message'] = 'Request has not been sent to Authorize.net';
        
        $bill_to = false;
        if( $first_name || $last_name || $address || $company || $state|| $city || $zip || $country ){
            $bill_to = true;
        }
         
        if( $merchant && $transaction && $url ){
            $request = 
            '<?xml version="1.0" encoding="utf-8"?>
            <createTransactionRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                <merchantAuthentication>
                    <name>'.$merchant.'</name>
                    <transactionKey>'.$transaction.'</transactionKey>
                </merchantAuthentication>
                <transactionRequest>
                    <transactionType>'.$transaction_type.'</transactionType>
                    <amount>'.$amount.'</amount>
                    <payment>
                        <creditCard>
                            <cardNumber>'.$cc_num.'</cardNumber>
                            <expirationDate>'.$cc_date.'</expirationDate>
                            '.($cvn ? '<cardCode>'.$cvn.'</cardCode>' :'').'
                        </creditCard>
                    </payment>                
                '.($bill_to ?
                '<billTo>
                    '.($first_name ? '<firstName>'.$first_name.'</firstName>':'').'
                    '.($last_name ?'<lastName>'.$last_name.'</lastName>':'').'
                    '.($company ?'<company>'.$company.'</company>':'').'
                    '.($address ?'<address>'.$address.'</address>':'').'
                    '.($city ?'<city>'.$city .'</city>':'').'
                    '.($state ?'<state>'.$state.'</state>':'').'
                    '.($zip ?'<zip>'.$zip.'</zip>':'').'
                    '.($country ?'<country>'.$country.'</country>':'').
                '</billTo>':'').'
                </transactionRequest>                
            </createTransactionRequest>';
            
            $xml_response = $this->curl( $request, $url );
             
            if( preg_match('!<resultCode>Error</resultCode>!', $xml_response)){
                $response['status'] = self::STATUS_FAILED; 
                if( preg_match('!<errorText>(?P<error>.*?)</errorText>!', $xml_response, $match) || preg_match('!<text>(?P<error>.*?)</text>!', $xml_response, $match)){
                    $response['message'] = $match['error'];
                }  else {
                    $response['message'] = 'There was an error with this transction';                                     
                }
            } else {
                $response['status'] = self::STATUS_SUCCESS; 
                if( preg_match('!<resultCode>Ok</resultCode>!', $xml_response)){
                    $response['message'] = 'Transaction was successfull';
                    if(preg_match('!<authCode>(?P<code>.+?)</authCode>!', $xml_response, $match)){
                        $this->auth_code = $match['code'];
                    }
                    if(preg_match('!<transId>(?P<id>.+?)</transId>!', $xml_response, $match)){
                        $this->transaction_id = $match['id'];
                    }                    
                }
            }
            
        }
        return $response;
    }
    /**
    <createTransactionResponse xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
       <messages>
          <resultCode>Ok</resultCode>
          <message>
             <code>I00001</code>
             <text>Successful.</text>
          </message>
       </messages>
       <transactionResponse>
          <responseCode>1</responseCode>
          <authCode>3DRNCE</authCode>
          <avsResultCode>Y</avsResultCode>
          <cvvResultCode/>
          <cavvResultCode>2</cavvResultCode>
          <transId>2177698954</transId>
          <refTransID/>
          <transHash>C018095242A7F7E3FA6816C8FE0438A0</transHash>
          <testRequest>0</testRequest>
          <accountNumber>XXXX0015</accountNumber>
          <accountType>MasterCard</accountType>
          <messages>
             <message>
                <code>1</code>
                <description>This transaction has been approved.</description>
             </message>
          </messages>
       </transactionResponse>
    </createTransactionResponse>
    * @param mixed $request
    */
    private function curl( $xml, $url ){
 

        $headers = array( 'Content-Type: text/xml' );
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
        curl_setopt($curl, CURLOPT_URL, $url );
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers );
        $result = curl_exec ($curl);

        if(curl_error($curl)){
            $result = curl_error($curl); 
        }
        curl_close ($curl);

        $this->request = $xml;
        $this->response = $result;
                
        return $result;                            
    }
}
