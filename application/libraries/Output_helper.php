<?php
class Output_helper{       
    
    private $cached = array();
    
    const ELEMENT_CHECKBOX = 'checkbox';
    const ELEMENT_DROPDOWN = 'dropdown';
    const ELEMENT_TEXTAREA = 'textarea';
    const ELEMENT_TEXT     = 'text';
    const INLINE_END = 'inline';
    
    const FORM_INLINE      = 'inline';
    const FORM_SEARCH      = 'search';
    const FORM_HORIZONTAL  = 'horizontal';
    private $table_name_override = array(
    
    );
    private $name_override = array(
        'agent::name' => 'agent'
    );
    /**
    * Fields below are not part of form
    */
    private $ignore = array(
        'table','datetime', 'errors', 'user_id', 'required', 'active'
    );
    /**
    * These fields are part of the form, but are hidden
    */
    private $hidden = array(
        'id','type','company_id','person_type','ministry_id'
    );
    
    private $label_override = array(
        'inspector_id' => 'Inspector' 
    );
    /**
    * Create DIV element with input fiels for this Record object or its subset
    * 
    * @param Record $object
    * @param array  $attributes
    * @param string $type
    * @param array  $special
    * @param string $id_prefix
    */
    public function get_object_form( $object, $attributes = array(), $type = null, $special = array(), $id_prefixes = null, $overrides = array() ){
         
        $string  = '';        
        $counter = 0;
        
        $default_class = $type == self::FORM_INLINE ? 'input-small' : 'input-large';
        
         
        if( $attributes ){
            $fields = $attributes;
        } else {
            $fields = array_diff( array_keys(get_object_vars( $object )), $this->ignore );
        }
                                                                                                             
        foreach( $fields as $name ){
            
            $value = $this->get_object_value( $object, $name );
            
            //Some fiels are hidden (ids, etc )
            $input_type = 'text';
            if( in_array( preg_replace('/.+::/','',$name), $this->hidden )){
                $input_type = 'hidden';
            }            
            //prefix id with param prefix or class name
            
            if( $id_prefixes ){
                 
                $object_name = $this->get_object_name( $object, $name );
                
                if( is_array( $id_prefixes) && isset( $id_prefixes[$object_name])){
                   $id_prefix = $id_prefixes[$object_name];  
                } else if( is_string( $id_prefixes )){
                   $id_prefix = $id_prefixes; 
                }
            }
            
            $id_name = preg_replace('/.+::/','',$name);
            if( isset($id_prefix) ){
                $id = $id_prefix . '_' .$id_name;    
            } else{
                if( get_class( $object ) == 'Item'){
                    $id = $id_name.'_'.$counter++;    
                } else {
                    $id = $id_name;
                }
            }
            $date = ''; 
            if( isset( $special[ $name ]['date'] ) && $special[ $name ]['date'] === true){
                $date = ' data-datepicker="datepicker" ';    
            }            
            /**
            * If this field is wrong
            */
            $error = '';
            if( in_array( $name, array_keys($object->errors) )){
                $error = ' error';
            }
            /**
            * Use Default class or one supplied in $special array
            */
            $class = $default_class;
            if( isset( $special[ $name ]['class'] ) && $special[ $name ]['class']){
                $class = $special[ $name ]['class'];     
            }                         
            $control = null;
            
            if( $special && isset( $special[ $name ]['element'] ) &&   $special[ $name ]['element'] ){
                    
                    switch( $special[ $name ][ 'element' ]  ){
                        case self::ELEMENT_CHECKBOX:
                            $control = '<input id="'.$id.'" name="'.$id.'" type="checkbox" '.($value ? 'checked="yes"' : '').'>';    
                        break;
                        case self::ELEMENT_DROPDOWN:
                            $control = form_dropdown( $id, 
                                                      $special[ $name ][ 'options' ],
                                                      $special[ $name ][ 'selected' ], 
                                                      ' id="'.$id.'" class="'.$class.'"' );
                        break;
                        case self::ELEMENT_TEXTAREA:  
                            $control = '<textarea class="'.$class.'" id="'.$id.'" name="'.$id.'" rows="'.(isset( $special[ $name ][ 'rows' ]) ? $special[ $name ][ 'rows' ] : 3) .'">'.$value.'</textarea>';
                        break;
                        default:
                            $control = '<input '.$date.' type="text" class="'.$class.'" id="'.$id.'" name="'.$id.'" value="'.$value.'"/>';
                        break;    
                    }

            }  
           
            if( !$control ){
                 $control = '<input '.$date.' type="'.$input_type.'" class="'.$class.$error.'" id="'.$id.'" name="'.$id.'" value="'.$value.'"/>';    
            }
            /**
            <span class="add-on">$</span>
            <input id="appendedPrependedInput" class="span2" type="text" size="16">
            <span class="add-on">.00</span>             
            
            */
            if( $special && isset( $special[ $name ]['prepend']) ){
                $control = '<div class="input-prepend"><span class="add-on">'.$special[ $name ]['prepend'].'</span>' . $control .'</div> ';             
            }
            /**
            * Is this element displayed inline with some other element?
            */
            if( $special && isset( $special[ $name ]['inline'])){
                $inline = $special[ $name ]['inline'];
            }
            /**
            * Second inline element found, will be merged with first
            */
            if( isset($inline) && $name == $inline ){
                $inline = self::INLINE_END;
            }
            
            
            if( isset( $this->name_override[ $name ] )){
                $name = $this->name_override[ $name ];
            } else if( isset( $overrides[ $name ]) ){
                $name = $overrides[ $name ];
            }            
            //Hidden elements dont have label 
            $label   = $input_type == 'hidden' ? '' : ( isset( $this->label_override[$name]) ? $this->label_override[$name] : ucfirst(preg_replace('/.+::/','',  str_replace('_',' ', $name))));
            /**
            * Is this inline form or horizontal ( classic ) form ?
            */
            switch( $type ){
                case self::FORM_INLINE:
                    $form_class = 'form-inline';
                    $string .= '<label class="control-label '.$error.'" for="'.$id.'">'.$label.'</label>
                                '.$control.'';    
                break;
                case self::FORM_SEARCH:
                break;
                default:
                     
                        if( isset($inline) && $inline ){
                            if( $inline == self::INLINE_END) {
                                
                                $id = $inline_data['id'];
                                $label = $inline_data['label'];
                                $error = $inline_data['error'];
                                $control = $inline_data['control'] . $control;
                                
                                $inline_data = array();
                                $inline = false;                                                         
                            } else {
                                $inline_data = array();
                                $inline_data['id'] = $id;
                                $inline_data['label'] = $label;
                                $inline_data['error'] = $error;
                                $inline_data['control'] = $control;                            
                            }                            
                        }

                        if( !isset($inline) || !$inline ){
                            $form_class = 'form-horizontal';
                            $string .= '<div class="control-group'.$error.'">
                                            <label class="control-label" for="'.$id.'">'.$label.'</label>                            
                                            <div class="controls">                           
                                                '.$control.'                                         
                                            </div>
                                       </div>';                            
                        }

                       
                      
                     
 
                break;                
            }
 
        }
 
        if( $special && isset( $special['__form']['hidden'] )){
            foreach( $special['__form']['hidden'] as $field=>$value){
                $string .= '<input type="hidden" name="'.$field.'" value="'.$value.'">';
            }
        }
        
        $legend = '';
        if( $special && isset( $special['__legend'])){
            $legend = '<legend>' .$special['__legend'].'</legend>';
        }
        $element = 'div';
        $submit  = '';
        $attributes  = '';
        if( $special && isset( $special['__form'])){
            $element = 'form';
            $attributes = ' action="'.$special['__form']['action'].'" method="post" ';
            if(isset($special['__form']['submit_js'])){
                $submit  = '<input class="btn" type="submit" value="'.$special['__form']['submit_value'].'" name="'.(isset($special['__form']['submit_name']) ?  $special['__form']['submit_name'] : 'Save') .'" onclick="'.(isset($special['__form']['submit_js']) ?  $special['__form']['submit_js'] : '') .'"/>';    
            }
            else{
                $submit  = '<input class="btn" type="submit" value="'.$special['__form']['submit_value'].'" name="'.(isset($special['__form']['submit_name']) ?  $special['__form']['submit_name'] : 'Save') .'"/>';            
            }

            $submit .= '<input class="btn" value="'.( $object->id ? 'Close' : 'Cancel').'" name="btn_cancel" style="width:50px;"/>';
            
            if(isset($special['__form']['close_btn'])){
                $submit .= '<input class="btn" id="c_btn" value="Close" style="width:50px;">';    
            }
        }
        /**
        * Return form div
        */
        $return = 
        '<'.$element.' '.$attributes.' class="'.$form_class.' well">            
            <fieldset>
             '.$legend.'
             <div name="controls">'.$string.'</div>
            </fieldset>
            '.$submit.'
        </'.$element.'>';
              
        return $return;    
    }
    public function get_text_area( $id, $name, $value, $class = 'default' ){
        $default_class = $class === 'default' ? 'input-xlarge': $class;
        return  '<textarea class="'.$default_class.'" id="'.$id.'" rows="1" name="'.$id.'" value="'.$value.'"></textarea>';    
        
    }
    public function get_text_field( $id, $name, $value, $class = 'default' ){
        $default_class = $class === 'default' ? 'input-xlarge': $class;
        return  '<input type="text" class="'.$default_class.'" id="'.$id.'" name="'.$id.'" value="'.$value.'"/>';         
    }
    public function get_order_table( array $orders, array $fields = array() , $table, $overrides = array() , $row_href = true ){
        
        $this->table_name_override = $overrides;
        $table->clear();
        
        if( !$fields ){
            $fields = array_diff( get_class_vars( Order ), $this->ignore )  ;
        }
        
        foreach( $fields as $field ){
            $headings[] = isset($this->table_name_override[$field]) ? $this->table_name_override[$field] :ucfirst( str_replace('_',' ', $field ));
        }
        $table->set_heading( $headings );

        foreach( $orders as $order ){
            $row = array();
            foreach( $fields as $field ){

                if( preg_match('/(?P<object>.+)::(?P<field>.+)/', $field, $matches )){
                    $object = $matches['object'];
                    $field  = $matches['field'];
                    $value = $order->$object->$field;
                } else {
                    $value = $order->$field;    
                } 
                if( in_array( $field, array('inspection_date','order_date'))){
                    $value = $this->get_us_date_format( $value );
                }
                if( $row_href ) {
                    $row[] = '<a href="'.site_url('orders/show_order/' . $order->id).'">'.$value.'</a>';    
                } else {
                    $row[] = $value;
                }
                  
            }
            $table->add_row( $row );    
        }
        
        return $table->generate();             
    }
    public function get_clients_table( array $orders, array $fields = array() , $table, $url = '' ){
        
        $table->clear();
        
        if( !$fields ){
            $fields = array_diff( get_class_vars( Clients ), $this->ignore )  ;
        }
        
        foreach( $fields as $field ){
            if( $field != 'sid' ){
                $headings[] = isset($this->table_name_override[$field]) ? $this->table_name_override[$field] :ucfirst( str_replace('_',' ', $field ));
            }
        }
        $table->set_heading( $headings );

        foreach( $orders as $order ){
            $row = array();
            foreach( $fields as $field ){
                if( preg_match('/(?P<object>.+)::(?P<field>.+)/', $field, $matches )){
                    $object = $matches['object'];
                    $field  = $matches['field'];
                    $row[] = '<a href="'.site_url('clients/show_client/' . $order->id).'">'.$order->$object->$field.'</a>';
                } else {
                    if( $field == 'sid'){
                        $row[] = '<input type="hidden" name="client_id" value="'.$order->id.'">';     
                    } else {
                        //$row[] = '<a href="'.site_url('clients/show_client/' . $order->id).'">'.$order->$field.'</a>';     
                        $row[] =  '<span value="'.$order->$field.'">'.$order->$field.'</span>';     
                    }
                       
                }   
            }
            $table->add_row( $row );    
        }
        
        return $table->generate();             
    }
     public function generate_table( array $records, array $fields = array(), $table, array $special = array()){
        $table->clear();
        if( !$fields ){
             $fields = array_diff( array_keys(get_class_vars( get_class( $records['0']) )), $this->ignore )  ;
        }
        
        foreach( $fields as $field ){
                $headings[] = !empty( $special[ $field ]['name']) ? $special[ $field ]['name'] :ucfirst( str_replace('_',' ', $field ));
        }  
        $table->set_heading( $headings );        
        foreach( $records as $record ){
            $row = array();
            foreach( $fields as $field ){
                //Get value of this object field
                $value = '<span>'.$this->get_object_value( $record, $field ).'</span>';
                //Value override?
                if( isset($special[$field]['values'][$value])){
                    $value = $special[$field]['values'][$value];
                }
                /**
                * HREF is on the row level
                */
                if( isset( $special['__href']) && !empty($special['__href']) ){
                    $row[] = $this->get_href(  $record, $field, $value, $special['__href'] );
                } else if ( isset($special[ $field ]['href']) && !empty($special[ $field ]['href']) ){
                    $row[] = $this->get_href(  $record, $field, $value, $special[ $field ] );                     
                } else {
                    $row[] = $value;    
                }                                
            }
            $table->add_row( $row );
        }
        $this->cached = array();
        return $table->generate();
    }
    private function get_object_value( $record, $field ){
         
        if( preg_match('/(?P<object>.+)::(?P<field>.+)/', $field, $matches )){
            $object   = $matches['object'];
            $o_field  = $matches['field'];  
            $record_object = $record->$object; 
            $value = $record_object[0]->$o_field;  
        } else {
            $value = $record->$field;  
        }
        
        if( in_array( $field, array('datetime'))){
            $value = $this->get_us_date_format( $value );
        }
        
        return $value;        
    }
    private function get_object_name( $record, $field ){
        
        $object = null;
        if( preg_match('/(?P<object>.+)::(?P<field>.+)/', $field, $matches )){
            $object   = $matches['object'];  
        }          
        return $object;         
    }
    public function get_us_date_format( $date ){
        if( preg_match('!(?P<year>\d\d\d\d)-(?P<month>\d\d)-(?P<day>\d\d)\s*(?P<rest>.*)!', $date, $matches ) ){
            $date = $matches['month'].'-'.$matches['day'].'-'.$matches['year'].' '.( isset($matches['rest']) ?' '.$matches['rest']: '' );
        }
        return $date;
    }
    private function get_href( $record, $field, $value,  $special ){
        $href = $special['href'];
           
        if( preg_match_all('!<(?P<fields>.+?)>!', $href, $matches )){
            foreach( $matches['fields'] as $replace_field ){
                $href = preg_replace( "!<$replace_field>!", $this->get_object_value( $record, $replace_field ), $href);
            }
        }
        $id = null;
        if( isset($special['id']) ){
            if( preg_match_all('!<(?P<fields>.+?)>!', $special['id'], $matches )){
                foreach( $matches['fields'] as $replace_field ){
                    $id = preg_replace( "!<$replace_field>!", $this->get_object_value( $record, $replace_field ), $special['id']);
                    break;
                }
            }
            if( $id ){
                $id = ' id="'.$id.'" ';
            }
        }
        return '<a '.$id.' href="'.$href.'" target="_blank">'.$value.'</a>';        
    }
}
?>
