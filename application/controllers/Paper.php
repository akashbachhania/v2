<?php
class Paper extends MY_Controller{
    const O_SAVED = '1';
    const O_UPDATED = '2';
    const O_ERROR = '3';
    const O_CCFAIL = '4';
    
    protected $order_type;
    public function __construct(){  
        parent::__construct();  
        $this->authorize_or_redirect();
        $this->load->model('dashboard_model','model', TRUE );
        $this->load->library('html_table');        
        $this->order_type = Order::ORDER_POSTCARD;               
    }   
    public function index( $status = null  ){     
        $postcards = $this->model->get_object(new Order(null,$this->user->company_id, $this->order_type));
        
        $this->load->view('include/sidebar', array('user'=>$this->user, 'jquery_ui'=>true,'postcard_menu'=>true, 'order_type'=>$this->order_type));
        $this->load->view('paper/postcards', 
            array( 
                'user'=>$this->user, 
                'path'=>$this->get_path(),
                'postcards'=>$postcards,
                'status'=>$status,
                'order_type'=>$this->order_type
            )
        );
        $this->load->view('include/footer');    
    }
    public function new_order_step_1( $status = '', $order = null ){   
        $param = $this->uri->segment(3);
        
        $persons = array();
        $selected_persons = array();
 
        if( $order && is_object($order) ){
            if($tos = unserialize($order->visitor_ids)){
                foreach($tos as $id){
                    $selected_persons[$id]=true;
                }                 
            }
           
        }
        $order = is_object($order) ? $order: new Order(); 
        if( $param == 'batch_paper' &&   ( ($visitor_ids = $this->input->post('visitors')) || ($visitor_ids = (array)$this->uri->segment(4)) )  ){                         
            foreach($visitor_ids as $id){
                $selected_persons[$id]=true;
            }
            $status = null;
        } else if( $param == 'modify' && $order_id = $this->uri->segment(4)){
             
            $order = $this->model->get_object(new Order( $order_id, $this->user->company_id ),true);
            $visitors = is_array($order->_visitor) ? $order->_visitor : array($order->_visitor);            
            foreach( $visitors as $visitor ){
                $selected_persons[$visitor->id] = true;
            }
            $status = null;
        }
        
        $visitors =  $this->model->get_object( new Visitor( null, $this->user->company_id));
         
        $letters = new Letter();
        $letters->set_order_type($this->order_type);
        $letters->set_type( Letter::TYPE_FRONT );
        $letters->set_active(1); 
        $letters = $this->model->get_object( $letters );  
         
        $this->session->set_userdata(array('order'=>serialize($order)));
                   
        $this->load->view('include/sidebar', array('user'=>$this->user, 'jquery_ui'=>true, 'postcard_menu'=>true, 'postcard_menu'=>true, 'order_type'=>$this->order_type));
        $this->load->view('paper/new_order_step_1', 
            array(
                'user'=>$this->user,
                'letters'=>$letters,
                'path'=>$this->get_path(),
                'visitors'=>$visitors,
                'selected_visitors'=>$selected_persons,
                'step'=> 1,
                'postcard'=>$order,
                'status' =>$status,
                'order_type'=>$this->order_type
            )
        );
        $this->load->view('include/footer');         
    }
    public function new_order_step_2( $status = '', Order $order = null ){ 
        $status = is_numeric($status) ? null: $status;
        $letters = new Letter();
        $letters->set_order_type($this->order_type);
        $letters->set_type( Letter::TYPE_BACK );   
        $letters->set_active(1);   
        $letters = $this->model->get_object( $letters  );
         
        $selected_visitors = array(); 
        if( $order ){
            if($tos = unserialize($order->visitor_ids)){
                foreach($tos as $id){
                    $selected_visitors[$id]=true;
                }                 
            }
            $recipients = unserialize($order->visitor_ids);
            $front_side = $order->front_letter_id;
             
        } else {
            $recipients = $this->input->post('recipients');
            $front_side     = $this->input->post('front_side');            
        }       
        $order = is_object($order) ? $order: new Order(); 
                 
        if( !$recipients || !$front_side ){
            $order = new Order();
            $order->set_front_letter_id($front_side);
            $order->set_visitor_ids(serialize($recipients));
            $this->new_order_step_1('Please select at least one recipient and front cover!', $order);
        } else {
            
            if($order_id = $this->uri->segment(3)){
                $order = $this->model->get_object(new Order($order_id, $this->user->company_id), true);
            }
            
            
            foreach( $recipients as $recipient ){
                $selected_visitors[$recipient] = true;
            }
             
            $session_order = unserialize($this->session->userdata('order')); 
            $session_order->set_front_letter_id( $front_side );
            $session_order->set_visitor_ids( serialize($recipients) );
            
            $this->session->set_userdata(array('order'=>serialize($session_order)));
             
            $this->session->set_userdata(
                array(
                    'recipients' => $recipients,
                    'front_side' => $front_side,
                )            
            );
            
            $visitors =  $this->model->get_object( new Visitor( null, $this->user->company_id));  
                    
            $this->load->view('include/sidebar', array('user'=>$this->user, 'jquery_ui'=>true, 'postcard_menu'=>true, 'order_type'=>$this->order_type));
            $this->load->view('paper/new_order_step_1', 
                array(
                    'user'=>$this->user,
                    'letters'=>$letters,
                    'path'=>$this->get_path(),
                    'visitors'=>$visitors,
                    'selected_visitors'=>$selected_visitors,
                    'step'=> 2,
                    'postcard'=>$order,
                    'status' => $status,
                    'order_type'=>$this->order_type
                )
            );
            $this->load->view('include/footer');             
        }
    }
    public function final_step( $status = null, Order $order = null, $message = null){  
          
         
        
        //Redirect back from charging
        if( $status !== null && $order ){
            $session_order = $order;
            $recipients = unserialize($session_order->visitor_ids);
            $front_side = $session_order->front_letter_id;
            $back_side  = $session_order->back_letter_id;
            $text       = $session_order->text;                        
        } else {
            $session_order = unserialize($this->session->userdata('order'));
            $recipients = $this->input->post('recipients');
            $front_side = $session_order->front_letter_id;
            $back_side  = $this->input->post('back_side');
            $text       = $this->input->post('text');            
        }

        $order = new Order(); 
        foreach( $recipients as $id ){
            if( $visitor = $this->model->get_object( new Visitor($id, $this->user->company_id), true )){
                $valid_recipients[$visitor->id] = $visitor->id;
                $order->add_visitor( $visitor );
            }
        }
         
        if( ($front_letter = $this->model->get_object(new Letter( $front_side),true)) && 
            ($back_letter = $this->model->get_object(new Letter( $back_side),true)) && $text){

            
            if( $order_id = $this->uri->segment(3)){
                $session_order->set_id($order_id);
            }
            
            
            $order->set_company_id( $this->user->company_id );
            $order->set_visitor_ids( serialize($valid_recipients) );
            $order->set_text( $text );
            $order->set_front_letter_id( $front_letter->id );
            $order->set_back_letter_id( $back_letter->id );
            $order->set_timestamp( gmdate('Y-m-d h:i:s'));
            $order->set_status( Order::STATUS_PENDING );
            
            $order->add_letter($front_letter);
            $order->add_letter($back_letter);
            
            $this->session->set_userdata(array('order'=>serialize($order)));
            
            $letters = new Letter();
            $letters->set_type( Letter::TYPE_FRONT );   
            $letters->set_active(1 );   
            $front_letters = $this->model->get_object( $letters  );
            
            $letters->set_type( Letter::TYPE_BACK );
            $back_letters = $this->model->get_object( $letters  );
            
            $visitors =  $this->model->get_object( new Visitor( null, $this->user->company_id)); 
            $states   = $this->model->get_us_states(); 
            $this->load->view('include/sidebar', array('user'=>$this->user, 'jquery_ui'=>true, 'postcard_menu'=>true,  'order_type'=>$this->order_type));
            $this->load->view('paper/payment', array(
                'user'=>$this->user, 
                'path'=>$this->get_path(), 
                'postcard'=>$order, 
                'status'=>$status,                 
                'message'=>$message,                 
                'action'=> site_url('paper/finalize'), 
                'front_letters'=>$front_letters, 
                'back_letters'=>$back_letters, 
                'all_visitors'=>$visitors, 
                'selected_visitors' => $valid_recipients, 
                'us_states'=>$states   ));
            $this->load->view('include/footer');
            
             
        } else {   
            
           $order->set_visitor_ids(serialize($recipients));
           $order->set_back_letter_id($back_side);
           $order->set_front_letter_id($front_side);
           $order->set_text($text);
           
           $this->new_order_step_2('Plese select back cover and postcard text!', $session_order );
        }
                
    }
    public function finalize(){
         
        if( ($cc_num  = $this->input->post('cc_num')) &&
            ($cc_date = $this->input->post('cc_date')) &&
            ($amount  = $this->input->post('amount')) ){
            
            $order = unserialize($this->session->userdata('order'));
            $charge_amount = str_replace('$','', $this->input->post('amount'));
            $count = count(unserialize($order->visitor_ids));
            $order_total = ($count*$order->_letter[0]->price)+($count*$order->_letter[1]->price);               
            
            if( $charge_amount == $order_total ){
                                  
                $order->set_total($order_total); 
                
                if( ($merchant = $this->config->item('payment_merchant') ) &&
                    ($transaction = $this->config->item('payment_transaction') ) &&
                    ($url = $this->config->item('payment_url') )) {
                    
                    $this->load->library('authorize_payment');
                    $result = $this->authorize_payment->send_payment_request( 
                        $charge_amount, 
                        $cc_num, 
                        $cc_date, 
                        null, 
                        $merchant, 
                        $transaction, 
                        $url,
                        htmlentities($this->input->post('cc_cvn')),
                        htmlentities($this->input->post('first_name')),
                        htmlentities($this->input->post('last_name')),
                        htmlentities($this->input->post('company')),
                        htmlentities($this->input->post('address')),
                        htmlentities($this->input->post('city')),
                        htmlentities($this->input->post('state')),
                        htmlentities($this->input->post('country'))
                    );
                    
                    if( $result['status'] == Authorize_payment::STATUS_SUCCESS ){
                        $order->set_total($order_total);
                        $order->set_type($this->order_type);
                        //$order->set_transaction_id( $this->authorize_payment->transaction_id);
                        //$order->set_auth_code( $this->authorize_payment->auth_code);
                         
                        $this->model->save_object($order);
                        $saved = Authorize_payment::STATUS_SUCCESS;
                    }
                    if( $result['message']) {
                        $order->errors[] = $result['message'];
                    }
                    
                    /* 
                    $transaction = new Transaction();
                    $transaction->set_order_id( $order_id )
                    ->set_request( $this->authorize_payment->request )
                    ->set_response( $this->authorize_payment->response )
                    ->set_datetime( gmdate('Y-m-d h:i:s'))
                    ->set_status( $result['status'])
                    ->set_amount( $amount )
                    ->set_message( $result['message'] );
                    */
                                 
                    if( $result['status'] == Authorize_payment::STATUS_SUCCESS){
                        $this->index(self::O_SAVED);
                    } else {
                        $this->final_step( self::O_CCFAIL, $order, $result['message']);
                    }                        
                } else {
                    $this->final_step( self::O_CCFAIL, $order, 'Invalid Payment Gateway configuration');
                }
            } else {   
               $this->final_step( self::O_CCFAIL, $order, 'Invalid amount value');
            }   
        }  else {
            $this->final_step( self::O_CCFAIL, $order, 'Missing cc details');     
        }
    }
    
    public function postcards( ){ 
        $postcards = $this->model->get_object(new Order(null,$this->user->company_id));
        
        $this->load->view('include/sidebar', array('user'=>$this->user, 'jquery_ui'=>true, 'postcard_menu'=>true, 'order_type'=>$this->order_type));
        $this->load->view('paper/postcards', 
            array(
                'user'=>$this->user,
                'path'=>$this->get_path(),
                'postcards'=>$postcards,
                'status'=>$status,
                'order_type'=>$this-order_type
            )
        );
        $this->load->view('include/footer');          
    }

    public function paper_progress(){ 
        $order = new Order(null,$this->user->company_id);
        $order->set_status(Order::STATUS_PENDING);
        $postcards = $this->model->get_object($order);
        
        $this->load->view('include/sidebar', array('user'=>$this->user, 'jquery_ui'=>true, 'postcard_menu'=>true,'order_type'=>$this->order_type));
        $this->load->view('paper/postcards', array('user'=>$this->user, 'path'=>$this->get_path(), 'postcards'=>$postcards, 'status'=>null, 'order_type'=>$this->order_type   ));
        $this->load->view('include/footer');           
    }
    public function paper_sent(){
        $order = new Order(null,$this->user->company_id);
        $order->set_status(Order::STATUS_SENT);
        $postcards = $this->model->get_object($order);
        
        $this->load->view('include/sidebar', array('user'=>$this->user, 'jquery_ui'=>true, 'postcard_menu'=>true,'order_type'=>$this->order_type));
        $this->load->view('paper/postcards', array('user'=>$this->user, 'path'=>$this->get_path(), 'postcards'=>$postcards, 'status'=>null, 'order_type'=>$this->order_type   ));
        $this->load->view('include/footer');          
    }
    public function ajax_letter(){
        
        $response = array();        
        if( ($id = $this->input->post('id')) && ($side = $this->input->post('side')) ){
            $letter = $this->model->get_object(new Letter($id),true);
            if( $letter ){
                $response['status'] = 'success';
                //update session

                $session_order = unserialize($this->session->userdata('order'));
                if( $side == 'front' ){
                    $session_order->set_front_letter_id( $letter->id);
                } else {
                    $session_order->set_back_letter_id( $letter->id);
                }
                $session_order->add_letter($letter);
                foreach($letter as $key=>$value){
                    $response[$key]=$value;
                }
                $response['total'] = $session_order->get_total();
                $this->session->set_userdata(array('order'=>serialize($session_order)));
            }
        }
        echo json_encode($response);
    }
    public function get_recipient(){  
        $visitor_id = $this->input->post('id');
 
         
        $visitors = array();
        if($visitor = $this->model->get_object(new Visitor($visitor_id),$this->user->company_id)){
               if( is_object( $visitor )){
                   $_visitor = array();
                   
                   $comps = array();
                   $comp = array('city','state','zip');
                   foreach ($comp as $value) {
                        if($visitor->$value){
                            $comps[] = $visitor->$value;
                        }
                   }
                   $comps_str = ($comps_str = implode(',', $comps)) ? $comps_str .'</br>':'';
                   
                   $phone_str = '';
                   if( $visitor->phone_1 ){
                       $phone_str = "<abbr title=\"Phone\">P:</abbr>$visitor->phone_1</br>";
                   }
                   
                   $address = 
                        "<span id='span_$visitor->id'>
                         <address>
                          <strong>$visitor->first_name $visitor->last_name</strong></br>
                          ".($visitor->address ? $visitor->address .'</br>': '')."
                          $comps_str
                          $phone_str 
                        </address>
                        </span>
                        
                        ";                  
                   $visitors = array('address'=>$address, 'status'=>'success');
                   
                   $session_order = unserialize($this->session->userdata('order'));
                   $recipients = unserialize($session_order->visitor_ids);
                   $recipients[] = $visitor->id;
                   $session_order->set_visitor_ids($recipients);
                   $this->session->set_userdata(array('order'=>serialize($session_order)));
               }
        }  
        echo json_encode($visitors); 
    }
    public function get_details(){
        $return = array();
        if( $type = $this->input->post('id')){
            switch($type){
                case 'user':
                    $fields = array('name','address','city','zip','state');
                    $object = $this->user;
                break;
                case 'company':
                    $fields = array('address','city','zip','state');
                    $object = $this->user->_company;
                break;                
            }
            if( $fields ){
                $return['status'] = 'success';
                foreach( $fields as $field ){
                    $return['details'][$field] = $object->$field;
                }
            }
        }
        echo json_encode($return);
    }
    public function ajax_recipient(){
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        
        $order = unserialize($this->session->userdata('order'));
        $recipients = unserialize( $order->visitor_ids );
        if( $status == '1'){
            if( $visitor = $this->model->get_object(new Visitor($id, $this->user->company_id ), true )){
                if( !in_array( $id, $recipients )){
                    $recipients[$id] = $id;
                    $order->set_visitor_ids( serialize($recipients));
                    $order->add_visitor($visitor);
                    $this->session->set_userdata(array('order'=>serialize($order)));
                }
            }
        } else {
            if( ($index = array_search( $id, $recipients )) !== false ){
                unset( $recipients[$index]);
                $order->set_visitor_ids( serialize($recipients));
                $this->session->set_userdata(array('order'=>serialize($order)));
            }
        }
    } 
}