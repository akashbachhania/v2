<?php
class Schedule extends MY_Controller{
    const STATUS_UPDATE = 'update';
    const STATUS_ERROR  = 'error';
    const STATUS_SAVED  = 'saved';
    const PDF_SCHEDULE_FOLDER = 'application/giving_pdfs/';
    /**
    * @var visitors_model
    */
    public $orm;
    
    public function __construct(){
        parent::__construct();  
        $this->authorize_or_redirect();
        $this->load->model('visitors_model','orm', TRUE );
        $this->load->model('schedule_model');
        $this->orm->set_user($this->user);    
    }

    public function index(){
        $ministries = $this->orm->get_ministries();
        //unset($ministries['add_new']);
        $this->load->view('include/sidebar', array('user'=>$this->user));
        $this->load->view('scheduls/index', array('user'=> $this->user,'ministries' =>$ministries));
        $this->load->view('include/footer');
    }

    public function add_new_members(){
        $ministry_id = $this->input->post('ministry_id');
        $visitor_id  = $this->input->post('visitor_id');
        $output = $this->schedule_model->getMembersByMinistryVistor($ministry_id,$this->user->company_id, $visitor_id);
        if($output){
            echo "true";
        }else{
            $ministry_log = new Ministry_log();
            $ministry_log->set_type_id($ministry_id);
            $ministry_log->set_visitor_id($visitor_id);
            $ministry_log->set_company_id($this->user->company_id);
            $id = $this->orm->save_object( $ministry_log );
            echo $id;            
            //echo "<pre>";print_r($ministry_log);die;
        }
    }

    public function get_all_members(){
        $members = $this->schedule_model->getAllMembers();
        $mem = array();
        if($members) {                
            foreach($members as $member) {
                $mem[$member->id] = $member->first_name.' '.$member->last_name;//array($member->id => $member->first_name.' '.$member->last_name);
            }                 
        }
        //echo "<pre>";print_r($mem);die;
        echo json_encode($mem);die;
        //echo "<pre>";print_r($mem);die;
    }

    public function get_members() {
        $ministry_id = $this->input->post('ministry_id');
        $data = array('success' => false);
        if($ministry_id !='') {
            $data = array('success' => true);
            $members = $this->schedule_model->getMembersByMinsitry($ministry_id,$this->user->company_id );  
            $mem = array();
            if($members) {                
                foreach($members as $member) {
                    $mem[] = array('id' => $member->id,'name' => $member->first_name.' '.$member->last_name);
                }                 
            }
            $data['members'] = $mem;          
            $data['roles'] = $this->orm->get_ministries_roles($ministry_id);    
            $data['roles']['add_new'] = 'Add Role | Edit Role';
        }
        echo json_encode($data);
    }

    function events_show() {
        $last_day   = date( 'd', strtotime( 'last day of ' . date( 'F Y')));
        $first_day  = date( 'd', strtotime( 'first day of ' . date( 'F Y')));
        $from   = (isset($_GET['from']) and $_GET['from'] !='') ? ($this->input->get('from') / 1000) : $first_day;
        $to     = (isset($_GET['to']) and $_GET['to'] !='') ? ($this->input->get('to') / 1000) : $last_day;
        // $scheduling = $this->schedule_model->getDateSchedule($from,$to,$this->user->company_id);
        $scheduling = $this->schedule_model->getInfoDateSchedule($from,$to,$this->user->company_id);

        $data = array('success' => 1);
        $row = array();

        $scheduleFormat = array();
        if($scheduling) {

            foreach($scheduling as $schedule) {
                $fullname = $schedule->first_name.' '.$schedule->last_name;
                $isDuplicateMemer = false;
                
                if(sizeof($scheduleFormat[$schedule->date_schedule][$schedule->ministry_name]['name']) > 0){
                    foreach ($scheduleFormat[$schedule->date_schedule][$schedule->ministry_name]['name'] as $key => $value) {
                        if(strcmp($value['name'],$fullname)==0){
                            $isDuplicateMemer = true;
                            break;
                        }
                    }
                }

                if($isDuplicateMemer == false ){
                    if($fullname==" ") continue;
                    // $scheduleFormat[$schedule->date_schedule][$schedule->ministry_name]['name'][] =  $fullname;
                    $scheduleFormat[$schedule->date_schedule][$schedule->ministry_name]['name'][] =  array('id' => $schedule->person_id,'name' => $fullname, 'sid'=> $schedule->sid);
                    $scheduleFormat[$schedule->date_schedule][$schedule->ministry_name]['id'] =  $schedule->id;
                    $scheduleFormat[$schedule->date_schedule][$schedule->ministry_name]['ministry_id'] =  $schedule->ministry_id;
                    $scheduleFormat[$schedule->date_schedule][$schedule->ministry_name]['time_type'] = $schedule->time_type;
                }
            }
            
            foreach ($scheduleFormat as $date => $ministrylist) {

                $tmp = array();
                $tmp['start'] = strtotime($date.' 00:00:00') . '000';
                $tmp['end']   = strtotime($date.' 00:00:00') . '000';
                $tmp['date']  = $date;
              
                foreach ($ministrylist as $ministryname => $person) {
                    
                    $listname = '';
                    $tmp['class'] = 'event-info event-edit';

                    $listname = "";
                    $listnameh = "";
                    foreach ($person['name'] as $key => $value) {
                        $id = $value['id'];
                        $name = $value['name'];
                        $sid = $value['sid'];
                        $listnameh .="<span style='display:none;' class='member-id-edit' data-sid='$sid' id='$id'>$name,</span>";
                        // $listname .="<span class='member-id-edit' data-sid='$sid' id='$id'>$name</span>,";
                        // $listname .= implode(', ', $person['name'][]);
                        $listname .= $name.',';
                    }
                    $tmp['time_type'] = $person['time_type'];
                    $tmp['list_name'] =  $listnameh;
                    $tmp['title'] = $ministryname.': '.$listname;
                    $tmp['id'] = $person['id'];
                    $tmp['ministry_id'] = $person['ministry_id'];
                    $row[] = $tmp;
                    
                }
            }
        }
        
        $data['result'] = $row;
        echo json_encode($data);
    }

    function add_new() {
        $ministry_id = $this->uri->segment(3,0);
        $edit_id = $this->uri->segment(4,0);
        $saved = null; 
       
        $role = new Ministry();              
        $role->set_company_id( $this->user->company_id );
        $role->set_ministry_id( $ministry_id );            
        $roles = $this->orm->get_object( $role );            
        if($edit_id !='' and $edit_id > 0)
            $role->set_id( $edit_id );
        if($edit_id !='' and $edit_id > 0){
            $role = $this->orm->get_object( $role,true );
        }            
        
        if( $this->input->post('save')){
            $role = $this->input_mapper->create_object_from_input( 'Ministry',Input_mapper::SOURCE_POST);
            if($role->validate()){                    
                $id = $role->id;
                $role->set_company_id( $this->user->company_id );                
                if($id == ($new_id = $this->orm->save_object($role)) ){
                    $saved = 'update';
                } else {
                    $saved = true;
                }
                $role->set_id($new_id);
            } else {
                $saved = false;
            }
        }
        $this->load->view('common/header',  array('user'=>$this->user,'dont_display_header'=>true));
        $this->load->view('common/add_new', array('selected_item' => $edit_id,'action' => site_url('schedule/add_new/'.$ministry_id.'/'.$edit_id), 'objects'=>$roles, 'object'=>$role, 'saved'=>$saved, 'control_id' => 0 , 'select_name' => $saved));
        $this->load->view('common/footer');            
    }

    function ajax_add_schedule() {
        $date_schedule  = $this->input->post('date_schedule');
        $member_id      = $this->input->post('member_id');
        $time_type       = $this->input->post('tym_type');
    
        $data = array('success' => false);
        if($date_schedule !='' and count($member_id) > 0) {
            foreach($member_id as $ministry_id => $member) {
                $member = array_unique($member);
                foreach($member as $mem) {
                    $scheduler = new Scheduleing;
                    $scheduler->set_date_schedule($date_schedule);
                    $scheduler->set_company_id($this->user->company_id);
                    $scheduler->set_ministry_id($ministry_id);
                    $scheduler->set_added_date(date('Y-m-d H:i:s')); 
                    $scheduler->set_time_type($time_type);
                    
                    $new_id = $this->orm->save_object($scheduler);
                    
                    $scheduler->set_id($new_id);

                    $relation = new Scheduler_relation;
                    $relation->set_member_id($mem);
                    $relation->set_role_id($ministry_id);
                    $relation->set_schedule_id($new_id);
                    $new_relation_id = $this->orm->save_object($relation);
                    $relation->set_id($new_relation_id);
                    $scheduler->set_relation($relation);

                    $this->orm->save_object($scheduler);
                }
            } 
            $data = array('success' => true);
        }
        echo json_encode($data);
    }
    
    function ajax_edit_schedule() {
        $date_schedule  = ($this->input->post('date_schedule'));
        $member_id      = $this->input->post('member_id');
        $delete_id      = $this->input->post('delete_member_id');
        $time_type       = $this->input->post('tym_type');
        
        $data = array('success' => false);

        if($date_schedule !='' and count($member_id) > 0) {
            foreach($delete_id as $ministry_id => $member) {
                foreach($member as $schedule_id => $mem) {
                    $scheduler = new Scheduleing;
                    $scheduler->set_id($schedule_id);
                    $rs = $this->orm->delete_object($scheduler);
                       
                    $relation = new Scheduler_relation;
                    $relation->set_member_id($mem[0]);
                    $relation->set_role_id($ministry_id);
                    $relation->set_schedule_id($schedule_id);
                    $rs = $this->orm->delete_object($relation);
                }
            } 

            foreach($member_id as $ministry_id => $member) {
                $member = array_unique($member);
                foreach($member as $mem){
                    if(is_array($mem)){
                        continue;
                    }

                    $scheduler = new Scheduleing;
                    $scheduler->set_date_schedule($date_schedule);
                    $scheduler->set_company_id($this->user->company_id);
                    $scheduler->set_ministry_id($ministry_id);
                    $scheduler->set_added_date(date('Y-m-d H:i:s')); 
                    $scheduler->set_time_type($time_type);
                    
                    $new_id = $this->orm->save_object($scheduler);
                    $scheduler->set_id($new_id);

                    $relation = new Scheduler_relation;
                    $relation->set_member_id($mem);
                    $relation->set_role_id($ministry_id);
                    $relation->set_schedule_id($new_id);
                    $new_relation_id = $this->orm->save_object($relation);
                 
                    $relation->set_id($new_relation_id);
                    $scheduler->set_relation($relation);

                    $this->orm->save_object($scheduler);
                }
            } 
            $data = array('success' => true);
        }
        echo json_encode($data);
    }

    function schedule_email() {
        $selected_month = $this->input->post('month');
        $start_date     = strtotime(date('Y-m-d',  strtotime($selected_month)));
        $end_date       = strtotime(date("Y-m-t", strtotime($selected_month)));
        $action         = $this->input->post('action');
        if($action =='email') {
            
        $scheduling = $this->schedule_model->getDateSchedule($start_date,$end_date,$this->user->company_id);
        
        $row = array();
        $to_emails = array();
        
        if($scheduling) {
           
            foreach($scheduling as $schedule) {
                $info = $this->schedule_model->getScheduleInfo($schedule->id);
                
                if($info) {
                   
                   $member  = array();
                   $role    = array();
                   $oldId = 0;
                   foreach($info as $in) {
                       $member[$in->role_id][] = $in->first_name.' '.$in->last_name;
                       $role[$in->role_id] = $in->rol_name;
                       if($in->email !='')
                        $to_emails[] = $in->email;
                   }
                   $title = '';
                   foreach($role as $key => $value) {
                       $title .='<br><b>Role:</b> '.$value.' / ';
                       $title .=' <b>Members:</b> '.implode(',', $member[$key]);                       
                   }
                   
                   $member = array_unique($member);
                   $member = implode(',', $member);
                   $role = array_unique($role);
                   $role = implode(',', $role);
                   $row_indi = array(); 
                   $row_indi['title'] = '<b>Ministry:</b> '.$schedule->ministry_name.$title;                   
                   $row[$schedule->date_schedule] = isset( $row[$schedule->date_schedule]) ? array_merge($row[$schedule->date_schedule], array($row_indi)): array($row_indi);
                }
            }
        }
            if(count($row) > 0) {
                
                $this->load->library('schedule_report');
                $company = $this->user->_company[0];
                $filename = $this->get_local_path().self::PDF_SCHEDULE_FOLDER.'Schedule_'.date('m-d-Y',  $start_date).'__'.date('m-d-Y',  $end_date).'.pdf';
                $this->schedule_report->create_pdf($company,$row ,$start_date , $end_date , $filename);
                $to_emails = array_unique($to_emails);
                
                $this->load->library('email');
                $this->email->from( 'no-reply@visitorplus.net', 'No-Reply'); 
                $this->email->to(implode(',', $to_emails) ); 
                $this->email->subject('"'.date('F',  $start_date).'" Schedule');
                $this->email->message('Below please find the schedule for the month of "'.date('F',  $start_date).'"');
                $this->email->attach( $filename );       
                // $this->email->send();
                echo 'done';
            }
            
        } elseif($action == 'print') {
            echo site_url('schedule/schedule_print?date='.date('Y-m-d',  $start_date));
        }
    }

    function schedule_print() {
        $selectedDate     = (isset($_GET['date']) and $_GET['date'] !='') ? $this->input->get('date') : strtotime(date('Y-m-d'));
        $start_date     = strtotime(date('Y-m-d',  strtotime($selectedDate)));
        $end_date       = strtotime(date("Y-m-t", strtotime($selectedDate)));
        $scheduling = $this->schedule_model->getDateSchedule($start_date,$end_date,$this->user->company_id);
        
        $row = array();
        $to_emails = array();
        
        if($scheduling) {

            foreach($scheduling as $schedule) {
                // echo "<pre>"; print_r($schedule);
                $info = $this->schedule_model->getScheduleInfo($schedule->id);
                if($info) {
                   $member  = array();
                   $role    = array();
                   $oldId = 0;
                   foreach($info as $in) {
                       $member[$in->role_id][] = $in->first_name.' '.$in->last_name;
                       $role[$in->role_id] = $in->rol_name;
                       if($in->email !='')
                        $to_emails[] = $in->email;
                   }
                   $title = '';
                   foreach($role as $key => $value) {

                       $title .= $value . '  :  <b>' . implode(',', $member[$key]) . '</b>';

                   }
                   
                   $member = array_unique($member);
                   $member = implode(',', $member);
                   $role = array_unique($role);
                   $role = implode(',', $role);
                   $row_indi = array(); 
                   $row_indi['time_type'] = $schedule->time_type;
                   $row_indi['title'] = /*'<b>Ministry:</b> '.$schedule->ministry_name.*/$title;                   
                   $row[$schedule->date_schedule] = isset( $row[$schedule->date_schedule]) ? array_merge($row[$schedule->date_schedule], array($row_indi)): array($row_indi);
                }
            }
        }

        if(count($row) > 0) {
            
            $this->load->library('schedule_report');
            $company = $this->user->_company[0];
            $this->schedule_report->create_pdf($company,$row ,$start_date , $end_date , false);
        }
    }
         
}
