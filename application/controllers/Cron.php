<?php
class Cron extends MY_Controller{
    
     public $orm;
    public function __construct(){
   
               
        parent::__construct();
        //$this->authorize_or_redirect();
        $this->load->model('Cron_model','cron_model', TRUE );          
    }    
    function birhtday_check() {
        $companies = $this->cron_model->getAllCompanies();
        
        if($companies) {
            
            $this->load->library('email'); 
            $this->load->library('mailer');        
            $this->mailer->initialize($this->email, $this->get_mail_config());
            foreach($companies as $company) {
               
                $template = new Template(); 
                $template->set_event('bday');
                $template->set_company_id( $company->id );
                $templates = $this->cron_model->get_object( $template);    

                if($templates and count($templates) > 0) {
                    
                    $persons = $this->cron_model->getTodayBirthdayPersons($company->id);
                    if($persons) {
                        foreach($persons as $person) {
                            
                            $filter = new Visitor();
                            $filter->set_id($person->id);
                            $filter->set_person_type( Person::TYPE_VISITOR );
                            $visitor = $this->cron_model->get_object( $filter,true );
                            
                            if($visitor->email !='') {
                                foreach($templates as $temp) {
                                    
                                    $sent = $this->mailer->send_template_individual($temp,$visitor); 
                                    $this->cron_model->system_log(
                                            'Email status',
                                            $this->mailer->get_logs()
                                        );
                                }
                            }                
                        }
                    }

                }
            }
        }
        
        echo 'Birthday Done';
    }
    function phone_text() {     
        
       
        $start_date =  date('Y-m-d',  strtotime('last monday')).' 00:00:00'; 
	$last_date =   date('Y-m-d',  strtotime('last sunday')).' 23:59:59';   
        
        $this->load->library('email');
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $cronTest .= 'Start Date : '.$start_date.'<br>';
        $cronTest .= 'End Date : '.$last_date.'<br>';

        $this->email->from('no-reply@visitorplus.net', 'No-Reply');
        $this->email->to('jamal2u@gmail.com');

        $this->email->subject('Test Email Cron Check');
        $this->email->message($cronTest);

        $this->email->send();
        
        $companies = $this->cron_model->getAllCompaniesWithPrimaryContact();
        $countSent = 0;
        if($companies) {
            
            $helperPath = APPPATH;         
            require_once($helperPath.'/helpers/Services/Twilio.php'); // Loads the library
            // Your Account Sid and Auth Token from twilio.com/user/account
            $sid = $this->config->item('twillo_id');
            $token = $this->config->item('twillo_token');
            $phone = new Services_Twilio($sid, $token);

            $fromNumber =  $this->config->item('twillo_from');
            
            foreach($companies as $company) {
                $primary_contact = $company->primary_contact;
                
                $filter = new Visitor();
                $filter->set_id($primary_contact);
                $filter->set_person_type( Person::TYPE_USER );
                $visitor = $this->cron_model->get_object( $filter );
                $primaryNumber = $visitor[0]->text_contacts;
                $send_emails   = $visitor[0]->send_emails;
                
                if($primaryNumber != '' or $send_emails != '') {
                    $new_visitors = $this->cron_model->get_new_visitors($company->id,$start_date,$last_date);
                    
                    if($new_visitors) {
                        
                        $text = 'New visitors for this week'."\n";
                        $subject = 'Visitors for last 7 days';
                        $message = 'Hi below you’ll find the details of the visitors for the last 7 days.<br>';
                        $message .= ' For more info please visit Visitor Plus at http://www.visitorplus.net/live/login.<br>';
                        
                        foreach($new_visitors as $vistor) {
                            $countSent++;
                            $text .= 'Name: '.$vistor->first_name.' '.$vistor->last_name.' Phone: '.$vistor->phone_1."\n";
                            
                            $filter = new Visitor();
                            $filter->set_id($vistor->id);
                            $visitorInfo = $this->cron_model->get_objects_with_related(
                                            $filter,
                                            array( //Fetch following related objects
                                                'Visits'
                                            )            
                                        );
                           
                            $visitdate = isset($visitorInfo->_visits) && isset($visitorInfo->_visits[0]->datetime) ? $visitorInfo->get_us_date( $visitorInfo->_visits[0]->datetime) : '';
                            $message .= '<br><br><br>Date of visit:'.$visitdate;
                            $message .= '<br>Name:'.$vistor->first_name.' '.$vistor->last_name;
                            $message .= '<br>Phone number:'.$vistor->phone_1;
                            $message .= '<br>Address:'.$vistor->address;
                        }
                        
                        if($send_emails !='') {
                            $this->load->library('email');
                            $config['mailtype'] = 'html';
                            $this->email->initialize($config);

                            $this->email->from('no-reply@visitorplus.net', 'No-Reply');
                            $this->email->to($send_emails);
                            
                            $this->email->subject($subject);
                            $this->email->message($message);
                            
                            $this->email->send();
                        }
                        
                        if($primaryNumber != '')   {          
                            $primaryNumber = explode(',', $primaryNumber);
                            foreach($primaryNumber as $num) {
                                $response = $phone->account->messages->sendMessage($fromNumber, $num, $text);
                            }
                        }
                    }                    
                }
             
                
                
            }
        }
        
        echo $countSent.' -- Done vister';
        
    }
}
