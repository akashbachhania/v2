<?php
class Admin extends MY_Controller{
    
    const AJAX_CLIENT_ORDERS = 'client_orders';

    private $field_list = array(
            'id',
            'status_string::name',
            'term::name',
            'agent::name',
            'inspector::name',
            'inspection::name',
            'meet_with',
            'address',
            'city',
            'active',
            'client::phone_1',
            'total',
            'paid',
            'amount_due'
    );
    
    public function __construct(){  
        parent::__construct(); 
        //This section is only for Admins and Users
        $this->authorize_or_redirect( array(Person::TYPE_ADMIN));
        $this->load->library('html_table');          
        $this->load->library('order_helper');          
        $this->load->library('order_mapper', array('input'=>$this->input));          
    }
    public function index(){
         
        $company_filter = new Company();
        $companies = null;
        $users = array();
        $selected_company = null;
        $new_company = null;
        $new_user = null;
        $saved = null;
        $company_id = null;
        
        if( $this->input->post('new_company') ){
            $new_company = new Company();
        } else if( $this->input->post('save_company')){ 
            
            if(isset($_POST['company_id'])) {
                unset($_POST['company_id']);
            }
            $company = $this->order_mapper->create_object_from_input('Company');
           
            if( $company->validate()){
               
                $id = $this->model->save_object( $company );
                if( $id == $company_id ){
                    $saved = 'update';
                } else {
                    $saved = true;
                }
                
                $company_id = $id;
            } else {
                $saved = false;
            }
        }  
        /**
        * IF company is selected - Load it
        */
        if( $company_id !== null || ($company_id = $this->input->post('company_id')) ){
            
            $person_filter = new Person();
            $person_filter->set_company_id( $company_id );         
            $person_filter->set_person_type( Person::TYPE_USER );
            $users = $this->model->get_object( $person_filter );
            
            $companies = $this->model->get_object( $company_filter );
            foreach( $companies as $company ){
                if( $company->id == $company_id ){
                    $selected_company = $company;
                    break;
                }
            }            
        }
        /**
        * Load all companies ( For the side div select box)
        */
        if( $companies == null ){
            $companies = $this->model->get_object( $company_filter );    
        }
        /**
        * Company select box statuses
        */
        $statuses = array(
            1 => 'Active',
            0 => 'Disabled'        
        );
        $companies =  $companies; 
        /**
        * Load views
        */
        $this->load->view('common/header', array( 'fluid_layout'=>false,'user'=>$this->user, 'companies'=>$this->companies, 'admin_order_menu'=>true ));
        $this->load->view('admin/admin', array(
            'companies' => $companies ,
            'users'     => $users,                                                    
            'statuses'  => $statuses,                                                    
            'saved'     => $saved,
            'states'    => $this->model->get_us_states(),                                                    
            'selected_company' => $selected_company )
        );
        $this->load->view('common/footer');        
    }
    
    public function ajax_control(){ 
        if( ($company_id = $this->input->post('company_id', TRUE))){                 
            $this->session->set_userdata('company_id', $company_id);
        }
    }
    public function add_new(){
         
        /**
        * Type of new object
        */
        $class_name = $this->uri->segment(3,0);
        /**
        * Company id (Only for new USER)
        */
        $company_id = $this->uri->segment(4,0);
        /**
        * Show user -> click on Coordinators table entry
        * 
        * 
        */
        $show_user = $this->uri->segment(5,0);
        
        $date = array();
        if( $this->input->post('save') && in_array( $class_name, array('Person','Company') ) ){
            //object can be one of 2 types (person, company)

            $object = $this->order_mapper->create_object_from_input($class_name);            
            
            if( $object->validate()){
                $old_id = $object->id;
                $object->set_id($this->model->save_object( $object ));
                if( $old_id != $object->id ){
                    $saved = true;    
                } else {
                    $saved = 'update';
                }
            } else {
                $saved = false;
            } 
            $data  = array('object' => $object, 'saved' => $saved, 'action' => site_url('admin/add_new/'.$class_name));                 
            
        }  else {
            /**
            * User clicked ADD NEW 
            */
            if( $show_user ){
              $filter = new Person();
              //company_id is user_id in this instance
              $filter->set_id( $company_id );
              $users = $this->model->get_object( $filter );
              $user = new Person();
              if( $users ){
                  $user = reset($users);
              }
              $data = array( 'object' => $user, 'action' => site_url('admin/add_new/'. $class_name) );
            } else if( in_array( $class_name, array('Person','Company'))){
                $object = new $class_name();
                if( $class_name == 'Person'){
                    $object->set_company_id( $company_id );
                    $object->set_person_type( Person::TYPE_USER );
                }
                $data = array( 'object' => $object, 'action' => site_url('admin/add_new/'. $class_name) );                
            } 
        }
        
        $data['states'] = $this->model->get_us_states();
        $this->load->view('common/header', array('fluid_layout'=>true, 'user'=>$this->user,'dont_display_header'=>true));             
        $this->load->view('common/add_new', $data );             
        $this->load->view('common/footer');           
    }
    public function orders(){    
        $type = $this->uri->segment(4);
        $filter = new Order();
        $filter->set_type( Order::ORDER_POSTCARD );
        if( $company_id = $this->uri->segment(3,0)){
            $filter->set_company_id($company_id);
        }
        $orders = $this->model->get_object($filter);
        
        $this->load->view('common/header', array('user'=>$this->user, 'jquery_ui'=>true,  'companies'=>$this->companies,'admin_order_menu' => true ));
        $this->load->view('admin/orders', array('user'=>$this->user, 'path'=>$this->get_path(), 'postcards'=>$orders, 'status'=>null   ));
        $this->load->view('common/footer');                    
    }
    /**
    * Ajax
    * Fetches recipient addresses
    * 
    */
    public function get_recipients(){  
        $order_id = $this->input->post('id');
 
         
        $visitors = array();
        if($order = $this->model->get_object(new Order($order_id),true)){
            $visitor = is_array( $order->_visitor ) ? $order->_visitor : (is_object($order->_visitor) ? array($order->_visitor) : '');
            foreach( $visitor as $index=>$visitor){
               if( is_object( $visitor )){
                   $_visitor = array();
                   
                   $comps = array();
                   $comp = array('city','state','zip');
                   foreach ($comp as $value) {
                        if($visitor->$value){
                            $comps[] = $visitor->$value;
                        }
                   }
                   $comps_str = ($comps_str = implode(',', $comps)) ? $comps_str .'</br>':'';
                   
                   $phone_str = '';
                   if( $visitor->phone_1 ){
                       $phone_str = "<abbr title=\"Phone\">P:</abbr>$visitor->phone_1</br>";
                   }
                   
                   $address = 
                        "<address>
                          <strong>$visitor->first_name $visitor->last_name</strong></br>
                          ".($visitor->address ? $visitor->address .'</br>': '')."
                          $comps_str
                          $phone_str 
                        </address>
                        
                        ";                  
                   $visitors[$index] = array('address'=>$address);
               }

            }
        }  
        echo json_encode($visitors); 
    }
    public function ajax_change(){
        $return = 'error';
        
        if(($order_id = $this->input->post('id')) && ($status = $this->input->post('status')) && (in_array($status, array(Order::STATUS_PENDING,Order::STATUS_SENT)))){
           if( $order = $this->model->get_object( new Order($order_id), true) ){
               $order->set_status( $status );
               $this->model->save_object( $order );
               switch($order->status){
                   case Order::STATUS_PENDING:
                        $return = 'warning';
                   break;
                   case Order::STATUS_SENT:
                        $return = 'success';
                   break;
               }
           }
        }
        echo json_encode($return);
    }
    public function ajax_change_letter(){  
        $return = array();
        if( ($letter_id = $this->input->post('id'))        
        ){
            $letter = $this->model->get_object(new Letter( $letter_id ), true);
            if( $letter ){
                 
                $letter->set_description( $this->input->post('description'));
                $letter->set_type( $this->input->post('type'));
                $letter->set_price( $this->input->post('price'));
                $letter->set_active( $this->input->post('active'));
                
 
                $letter->set_type_front_side_postcard( (int)$this->input->post('front_postcard'));
                $letter->set_type_back_side_postcard( (int)$this->input->post('back_postcard'));
                
                if( $letter->validate() ){
                    $this->model->save_object( $letter );
                    $return['status'] = 'success';
                } else {
                    $return['status'] = 'fail';
                    foreach( $letter->errors as $field=>$value ){
                        $return['fields'][$field]= $value;
                    }
                }
            }
        }
        echo json_encode($return);
    }
    public function postcards( $status = null , $new_letter = null ){ 
        $postcards = $this->model->get_object(new Letter(),false, array(),Orm::REL_BINDING_OR, 'Order');
        
        $this->load->view('common/header', array('user'=>$this->user, 'jquery_ui'=>true,  'companies'=>$this->companies ));
        $this->load->view('admin/postcards', array('user'=>$this->user, 'path'=>$this->get_path(), 'postcards'=>$postcards, 'status'=>$status, 'new_letter'=>$new_letter ? $new_letter: new Letter()   ));
        $this->load->view('common/footer');         
    }
    public function add_new_postcard(){ 
         
         
        /**
        * 
        * 
        * @var Letter
        */
        $letter = $this->input_mapper->create_object_from_input('Letter',Input_mapper::SOURCE_POST);
        
        $status = 'fail';
        if( isset( $_FILES['file'])){
            if( preg_match('/image/', $_FILES['file']['type'] ) && $_FILES['file']['size'] < 4194304 ){
                
                $id = null;
                if( $letter->validate() ){
                    
                    if( preg_match('/.(?P<type>\w{3,4})$/',$_FILES['file']['name'], $match)){
                        
                        switch( $this->input->post('type')){
                            case '2':
                                $letter->set_type_back_side_postcard(true);
                            break;
                            case '1':
                                $letter->set_type_front_side_postcard(true);
                            break;                            
                        }
                        
                        $id   = $this->model->save_object( $letter );
                        
                        $path = parse_url( site_url()); 
                        $path = str_replace('index.php','',$path['path']) ; 
                        $path = $_SERVER['DOCUMENT_ROOT'] .  $path  . 'application/views/assets/img/covers/';
                        
                        $filename_thumb =  $id.'_thumb.'.$match['type'];
                        $filename_full  =  $id.'_full.'.$match['type'];
                        
                        $letter->set_id($id);
                        $letter->set_path_full( $filename_full );
                        $letter->set_path_thumb( $filename_thumb );
                        
                        $this->model->save_object( $letter );
                        
                        $this->image_resize( $_FILES['file']['tmp_name'], $path . $filename_thumb, $_FILES['file']['type'], 150 );                                                                
                        $this->image_resize( $_FILES['file']['tmp_name'], $path . $filename_full, $_FILES['file']['type'] );
                                                                                        
                        $status = 'success';
                    }
                } else {
                    $status = 'fail';
                }                

                                   
            } else {
                $letter->errors['_file'] = true;
            }
        } else {
             $letter->errors['_file'] = true;
        }
        
        $this->postcards( $status, $letter );
    }
    public function settings(){
         
        $saved = null;
        
        $path = parse_url( site_url());
         

        
        $filename = $this->user->company_id.'_logo';
        
        if( $this->input->post('save') ){
            
            if( $this->input->post('password') == $this->input->post('password_2')){
                $this->user->email = $this->input->post('username');
                $this->user->password = $this->input->post('password');                
                $this->home_model->save_or_update_object( $this->user );                
            }
            /**
            * TODO file checkup
            */
            if( isset( $_FILES['file'])){
                
                
            }
            $saved = true; 
        }
         
        $file = null;
        if( file_exists( $path. $filename)){
             
            $file = str_replace('index.php','',site_url() .'application/views/assets/img/' . $filename);
        }
        
        $this->load->view('common/header', array('fluid_layout'=>false,'dont_display_header' =>true, 'companies'=>$this->companies,'saved'=>$saved));
        $this->load->view('home/settings', array('user' => $this->user, 'file'=>$file));
        $this->load->view('common/footer');
       
    }

    public function transactions(){   
        $transactions = $this->order_model->get_object(new Transaction());
        
        $this->load->view('common/header', array('fluid_layout'=>false, 'user'=>$this->user,'companies'=>$this->companies));             
        $this->load->view('admin/transactions', array('transactions'=>$transactions  ) );             
        $this->load->view('common/footer');         
        
    } 
    
    function image_resize( $f, $new_file, $type, $size = null ){
         
        $imgFunc = '';
        $img_data = getimagesize($f);
        switch($type)
        {
            case 'image/gif':
                $img = ImageCreateFromGIF($f);
                $imgFunc = 'ImageGIF';
                $transparent_index = ImageColorTransparent($img);
                if($transparent_index!=(-1)) $transparent_color = ImageColorsForIndex($img,$transparent_index);
                break;
            case 'image/jpeg':
                $img = ImageCreateFromJPEG($f);
                $imgFunc = 'ImageJPEG';
                break;
            case 'image/png':
                $img = ImageCreateFromPNG($f);
                ImageAlphaBlending($img,true);
                ImageSaveAlpha($img,true);
                $imgFunc = 'ImagePNG';
                break;
        }
 
 
        if( $size ){
            list($w,$h) = GetImageSize($f);
             
            $percent = $size / (($w>$h)?$w:$h);
            if($percent>0 and $percent<1)
            {
                $nw = intval($w*$percent);
                $nh = intval($h*$percent);
                $img_resized = ImageCreateTrueColor($nw,$nh);
                if($type=='image/png')
                {
                    ImageAlphaBlending($img_resized,false);
                    ImageSaveAlpha($img_resized,true);
                }
                if(!empty($transparent_color))
                {
                    $transparent_new = ImageColorAllocate($img_resized,$transparent_color['red'],$transparent_color['green'],$transparent_color['blue']);
                    $transparent_new_index = ImageColorTransparent($img_resized,$transparent_new);
                    ImageFill($img_resized, 0,0, $transparent_new_index);
                }
                if(ImageCopyResized($img_resized,$img, 0,0,0,0, $nw,$nh, $w,$h ))
                {
                    ImageDestroy($img);
                    $img = $img_resized;
                   
                }
            }            
        }

        $imgFunc($img, $new_file);
        ImageDestroy($img);         
    }           
}
