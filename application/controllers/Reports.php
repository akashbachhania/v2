<?php
class Reports extends MY_Controller{
    const O_SAVED = '1';
    const O_UPDATED = '2';
    const O_ERROR = '3';
    
    public function __construct(){
              
        parent::__construct();  
        $this->authorize_or_redirect();
        $this->load->model('dashboard_model','model', TRUE );
        $this->load->library('html_table');               
    }   
    public function index( $status = null  ){
    
        $start_date = gmdate('m-d-Y',time()-7*24*60*60);
        $end_date   = gmdate('m-d-Y');
        
                
        $this->load->view('include/sidebar', array('user'=>$this->user, 'jquery_ui'=>true,'load_charts' => true));
        //$this->load->view('reports/reports', array('user'=>$this->user, 'start_date'=>$start_date, 'end_date'=>$end_date ));
        $this->load->view('reports/reports', array('user'=>$this->user ));
        $this->load->view('include/footer');    
    }
    public function ajax_get_stats(){
         
         
        $start_date = ($start_date = $this->input->post('start_date')) ? $start_date : gmdate('m-d-Y',time()-7*24*60*60);
        $end_date   = ($end_date = $this->input->post('end_date')) ? $end_date: gmdate('m-d-Y');
        
        $filter_start = new Visits();
        $filter_start->set_datetime($start_date);
        $filter_start->set_company_id( $this->user->company_id );
        
        $filter_end = new Visits();
        $filter_end->set_datetime($end_date);
        $filter_end->set_company_id( $this->user->company_id );
        
        if( gmmktime(0,0,0,$filter_start->__visit_month, $filter_start->__visit_day, $filter_start->__visit_year) > ($end_time = gmmktime(0,0,0,$filter_end->__visit_month, $filter_end->__visit_day, $filter_end->__visit_year))){
            $filter_start->set_datetime( gmdate('Y-m-d', $end_time-7*24*60*60));
        }
        
        
        $filter_member_start = new Visits();
        $filter_member_start->set_datetime($start_date);
        $filter_member_start->set_company_id( $this->user->company_id );
        $filter_member_start->set_is_member(true);
        
        $filter_member_end = new Visits();
        $filter_member_end->set_datetime($end_date);
        $filter_member_end->set_company_id( $this->user->company_id );
        $filter_member_end->set_is_member(true);
        
        if( gmmktime(0,0,0,$filter_member_start->__visit_month, $filter_member_start->__visit_day, $filter_member_start->__visit_year) > ($end_time = gmmktime(0,0,0,$filter_member_end->__visit_month, $filter_member_end->__visit_day, $filter_member_end->__visit_year))){
            $filter_member_start->set_datetime( gmdate('Y-m-d', $end_time-7*24*60*60));
        }
        
        
        /**
        * Fetch all visits for Currenct company
        * that are between start date and and date
        */
         
        
        $charts = array();
        
        $visits_per_sex = array(
            'male' =>0,
            'female' =>0
        );
        $attendance_per_sex = array(
            'male' =>0,
            'female' =>0
        );
        $visits_per_married = array(
            'married'=> 0,
            'unmarried' => 0,
        );
        $attendance_per_married = array(
            'married'=> 0,
            'unmarried' => 0,
        );
        $visits_per_visitor = array();
        $visits_per_town = array();
        $visits_per_dow = array(

        );
        $attendance_per_dow = array(

        );
        
        $visits_per_date = array(
            'No data available for selected dates' => 1,
        );
        
        $attendance_per_date = array(
            'No data available for selected dates' => 1,
        );
        
        $visitors_per_age = array(
            'No data available for selected dates' => 1
        );   
        
        $members_per_age = array(
            'No data available for selected dates' => 1
        );   
        
        $giving_amount_by_fund = array();  
        
        $giving_amount_by_member = array();  
        
        $fields = array(
            'Visits per gender' => 'visits_per_sex',
            'Visits per marital status' => 'visits_per_married',
            'Visitors by town' => 'visits_per_town',
            'Visitors by age'  => 'visitors_per_age',
            'Visitors by date' => 'visits_per_date',
            'Visitors by DOW'  => 'visits_per_dow',
            'Number of visits per visitor' => 'visits_per_visitor' ,
            'Attendance per gender' => 'attendance_per_sex', 
            'Attendance per marital status' => 'attendance_per_married',
            'Attendance per DOW' => 'attendance_per_dow',
            'Attendance by date' => 'attendance_per_date',
            'Giving Amount by Fund' => 'giving_amount_by_fund',
            'Giving Amount by Member' => 'giving_amount_by_member',
            'Members by age' => 'members_per_age',
        );
        
        $charts['status'] = 'success';
        $charts['start_date'] = $filter_start->datetime;
        $charts['end_date'] = $filter_end->datetime;
        foreach( $fields as $field=>$var ){
            $charts['charts'][] = array(
                    'title' => $field,
                    'rows'  => &$$var,
                    'id'    => $var
            );
        }
                    
        if( $stats =  $this->model->get_object( array( $filter_start, $filter_end), false, 
                                                      array( 
                                                            '0' => array('datetime' => Orm::REL_GREAT_E), 
                                                            '1' => array('datetime' => Orm::REL_LESS_E )
                                                            ), 
                                                      Orm::REL_BINDING_AND )) {
                                                          
            $year = gmdate('Y');
            
            $visits_per_dow = array();
            $visits_per_date = array();
            
            foreach( $stats as $stat ){
                /**
                * @var Visitor
                */
                $visitor = $stat->_visitor[0];
                if( $visitor->gender == 'male'){
                    $visits_per_sex['male']++;
                } else {
                    $visits_per_sex['female']++;
                }
                
                if( $visitor->married == 1 ){
                    $visits_per_married['married']++;
                } else {    
                    $visits_per_married['unmarried']++;
                }
                $name = $visitor->first_name.' '.$visitor->last_name;
                
                $visits_per_visitor[$name] = isset($visits_per_visitor[$name])?$visits_per_visitor[$name]+1:1;
                
                $city = $visitor->city ? $visitor->city: 'Unknown';
                $visits_per_town[$city] = isset($visits_per_town[$city])?$visits_per_town[$city]+1:1;
                
                if( $visitor->__age < 10 ){
                    $visitors_per_age['0-10']++;
                } else if( $visitor->__age < 20 ){
                    $visitors_per_age['10-20']++;
                } else if( $visitor->__age < 35 ){
                    $visitors_per_age['20-35']++;
                } else if( $visitor->__age < 50 ){
                    $visitors_per_age['35-50']++;
                } else {
                    $visitors_per_age['50+']++;  
                }
                
                $dow = gmdate('l', gmmktime(0,0,0,$stat->__visit_month, $stat->__visit_day, $stat->__visit_year ));
                $visits_per_dow[$dow] = isset( $visits_per_dow[$dow]) ? $visits_per_dow[$dow]+1: 1;
                $visits_per_date[$stat->datetime] = isset( $visits_per_date[$stat->datetime]) ? $visits_per_date[$stat->datetime]+1:1;
     
            }
            
                                                                                                       
        }    
        
        unset($stats);
        if( $stats =  $this->model->get_object( array( $filter_member_start, $filter_member_end), false, 
                                                      array( 
                                                            '0' => array('datetime' => Orm::REL_GREAT_E), 
                                                            '1' => array('datetime' => Orm::REL_LESS_E )
                                                            ), 
                                                      Orm::REL_BINDING_AND )) {
                                                          
            $year = gmdate('Y');
            
            $attendance_per_dow = array();
            $attendance_per_date = array();
            
            foreach( $stats as $stat ){
                /**
                * @var Visitor
                */
                //$visitor = $stat->_visitor[0];
                $visitor = new Member ($stat->visitor_id , $this->user->company_id);
                $visitor = $this->model->get_object( $visitor , true);
                if( $visitor->gender == 'male'){
                    $attendance_per_sex['male']++;
                } else {
                    $attendance_per_sex['female']++;
                }
                
                if( $visitor->married == 1 ){
                    $attendance_per_married['married']++;
                } else {    
                    $attendance_per_married['unmarried']++;
                }
                $name = $visitor->first_name.' '.$visitor->last_name;
                
                $attendance_per_visitor[$name] = isset($attendance_per_visitor[$name])?$attendance_per_visitor[$name]+1:1;
                
                if( $visitor->__age < 10 ){
                    $members_per_age['0-10']++;
                } else if( $visitor->__age < 20 ){
                    $members_per_age['10-20']++;
                } else if( $visitor->__age < 35 ){
                    $members_per_age['20-35']++;
                } else if( $visitor->__age < 50 ){
                    $members_per_age['35-50']++;
                } else {
                    $members_per_age['50+']++;  
                }
                
                
                $dow = gmdate('l', gmmktime(0,0,0,$stat->__visit_month, $stat->__visit_day, $stat->__visit_year ));
                $attendance_per_dow[$dow] = isset( $attendance_per_dow[$dow]) ? $attendance_per_dow[$dow]+1: 1;
                $attendance_per_date[$stat->datetime] = isset( $attendance_per_date[$stat->datetime]) ? $attendance_per_date[$stat->datetime]+1:1;
     
            }
            
                                                                                                       
        }
        
        $this->load->model('reports_model');
        $givings = $this->reports_model->getMembersByMinsitry($start_date,$end_date);
        $grandTotal = 0;
        if($givings) {
            foreach($givings as $giving) {
                 $name = $giving->first_name.' '.$giving->last_name;
                 $fund_name = $giving->fund_name;
                 $grandTotal += $giving->value;
                 $giving_amount_by_fund[$fund_name] = isset( $giving_amount_by_fund[$fund_name]) ? $giving_amount_by_fund[$fund_name]+$giving->value: 0;
                 $giving_amount_by_member[$name] = isset( $giving_amount_by_member[$name]) ? $giving_amount_by_member[$name]+$giving->value: 0;
            }
        }
        $charts['grand_total_funding'] = $grandTotal;
        echo json_encode($charts);    
    }
} 
?>
